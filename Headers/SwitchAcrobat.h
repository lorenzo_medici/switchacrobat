//****************************************************************************
/*! \file     SwitchAcrobat.h
 *  \brief    Header file
 *  \author   Michele Fischer
 *  \date     2009
 ****************************************************************************/

#ifndef __ADFS2LINKACROBAT_H__
#define __ADFS2LINKACROBAT_H__

#ifndef WIN32
	#include "MacPlatform.h"
#else
	#include "windows.h"
	#include "PDBasicExpT.h"
	#include "avexpt.h"
#endif

#include "TextFile.h"
#include "TCPIPSocket.h"
#include "TCPIPIdleTask.h"

#include <string>

//****************************************************************************
/*! \brief  Extern Globals
 ****************************************************************************/
class   SwitchAcrobat;
extern  SwitchAcrobat gApplication;

class SwitchAcrobat
{
  public:
    SwitchAcrobat();

  public:
    int readConfigFile();
    int setUpCommunication();
    void showError(string sError);
    bool openPDFFile();
    void GetDocumentSize (int &iWidth, int &iHeight);
  public:
    void setIPAddress(string sIPAddress){ m_sIPAddress = sIPAddress; }
    void setPort(int iPort)             { m_iPort = iPort; }
    void setPDFPath(string sPDFPath)    { m_sPDFPath = sPDFPath; };
    void setPDDoc(PDDoc myPDDoc)        { m_oPDDoc =  myPDDoc; };
    void setAVDoc(AVDoc myAVDoc)        { m_oAVDoc = myAVDoc; }
    void setFileOpen(bool b)            { m_bFileOpen = b; }

    TCPIPSocket*  getTCPIPSocket() { return &m_oTCPIPSocket; }
    string getIPAddress()          { return m_sIPAddress; }
    string getPDFPath()            { return m_sPDFPath; }
    int getPort()                  { return m_iPort; }
    bool getFileOpen()             { return m_bFileOpen; }
    PDDoc getPDDoc()               { return m_oPDDoc; }
    AVDoc getAVDoc()               { return m_oAVDoc;}

  private:
    string      m_sIPAddress;
    string      m_sPDFPath;
    int         m_iPort;
    bool        m_bReadConfigFile;
    bool        m_bFileOpen;
    PDDoc       m_oPDDoc;
    AVDoc       m_oAVDoc;
    TCPIPSocket m_oTCPIPSocket;    
};
#endif
