//****************************************************************************
/*! \file     TCPIPIdleTask.h
 *  \brief    Header file
 *  \author   Michele Fischer
 *  \date     2009
 ****************************************************************************/

#ifndef __TCPIDLETASK_H__
#define __TCPIDLETASK_H__

#ifdef WIN32
	#include "windows.h"
#endif
	
#include "TCPIPSocket.h"

#include <string>
using namespace std;

class TCPIPIdleTask
{
  public:
    TCPIPIdleTask();

  public:
    void initializeIdle();
};
#endif