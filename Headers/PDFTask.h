//****************************************************************************
/*! \file     PDFTaskt.h
 *  \brief    Header file
 *  \author   Michele Fischer
 *  \date     2009
 ****************************************************************************/

#ifndef __PDFTASK_H__
#define __PDFTASK_H__

#ifndef MAC_PLATFORM
	#include "windows.h"
#else
	#include "MacPlatform.h"
	#include "PDBasicExpT.h"
	//#include "avexpt.h"
#endif

#include <fstream>
#include <string>
using namespace std;

class PDFTask
{
  public:
    PDFTask();
    ~PDFTask();
   
  public:
    bool openPDFFile(string sPDFFile);
    bool closeAndSavePDFFile();

  public:
    void setPDDoc(PDDoc myPDDoc)        { m_oPDDoc =  myPDDoc; };
    void setPDFFilePath(string sPDFPath){ m_PDFFilePath = sPDFPath; }

    PDDoc getPDDoc()        { return m_oPDDoc; }
    string getPDFFilePath() { return m_PDFFilePath; }

  public:
    PDDoc  m_oPDDoc;
    string m_PDFFilePath;
};
#endif
