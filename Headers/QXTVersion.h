//****************************************************************************
/*! \file
 *  \brief    Header file: Version defines
 *  \author   Hans St�ssel
 *  \date     2011
 *  \version  1.0.0
 *  \bug      
 *  \warning  
 *  \todo     
 ****************************************************************************/
#ifndef __QXTVERSION_H_
#define __QXTVERSION_H_

#if defined QXP6 || defined QXP7 || defined QXP8 || defined QXP9 || defined QXP10 || defined QXP2015 || defined QXP2016
  #define QXP_PROJECT
#endif

#if defined QXP6 || defined QXP7 || defined QXP8 || defined QXP9 || defined QXP10 || defined QXP2015
  #define QXP_PROJECT_LT_QXP2016
#else
  #undef QXP_PROJECT_LT_QXP2016
#endif

#if defined QXP6 || defined QXP7 || defined QXP8 || defined QXP9 || defined QXP10
  #define QXP_PROJECT_LT_QXP2015
#else
  #undef QXP_PROJECT_LT_QXP2015
#endif

#if defined QXP6 || defined QXP7 || defined QXP8 || defined QXP9
  #define QXP_PROJECT_LT_QXP10
#else
  #undef QXP_PROJECT_LT_QXP10
#endif

#if defined QXP6 || defined QXP7 || defined QXP8
  #define QXP_PROJECT_LT_QXP9
#else
  #undef QXP_PROJECT_LT_QXP9
#endif

#if defined QXP6 || defined QXP7
  #define QXP_PROJECT_LT_QXP8
#else
  #undef QXP_PROJECT_LT_QXP8
#endif

#ifdef QXP6
  #define QXP_PROJECT_LT_QXP7
#else
  #undef QXP_PROJECT_LT_QXP7
#endif

#if !defined QXP6 && !defined QXP7 && !defined QXP8 && !defined QXP9 && !defined QXP10 && !defined QXP2015
  #define QXP_PROJECT_GT_QXP2015
#else
  #undef QXP_PROJECT_GT_QXP2015
#endif

#if !defined QXP6 && !defined QXP7 && !defined QXP8 && !defined QXP9 && !defined QXP10
  #define QXP_PROJECT_GT_QXP10
#else
  #undef QXP_PROJECT_GT_QXP10
#endif

#if !defined QXP6 && !defined QXP7 && !defined QXP8 && !defined QXP9
  #define QXP_PROJECT_GT_QXP9
#else
  #undef QXP_PROJECT_GT_QXP9
#endif

#if !defined QXP6 && !defined QXP7 && !defined QXP8
  #define QXP_PROJECT_GT_QXP8
#else
  #undef QXP_PROJECT_GT_QXP8
#endif

#if !defined QXP6 && !defined QXP7
  #define QXP_PROJECT_GT_QXP7
#else
  #undef QXP_PROJECT_GT_QXP7
#endif


#if !defined QXP6
  #define QXP_PROJECT_GT_QXP6
#else
  #undef QXP_PROJECT_GT_QXP6
#endif

#endif