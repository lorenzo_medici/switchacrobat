//****************************************************************************
/*! \file     TCPIPSocket.h
 *  \brief    Header file
 *  \author   Michele Fischer
 *  \date     2009
 ****************************************************************************/

#ifndef __TCPIPSOCKET_H__
#define __TCPIPSOCKET_H__

#ifdef WIN32
	#include "windows.h"
#endif

#include <fstream>
#include <string>
#include <vector>

using namespace std;

class TCPIPSocket
{
  public:
    TCPIPSocket();
    ~TCPIPSocket();

  public:
    int TCPIP_Close_Listening(bool bCloseThread);
    int TCPIP_Init_Listening();
    int TCPIP_Rcv_Data(bool* pDataReceived);
    int split(string s, string sDelimiter, vector<string>& vSplit);
    string TCPIP_get_PDFPath(string sData);
    string convertFromSocket(string s);
    void TCPIP_Call_Back(int iDocWidth, int iDocHeight);

  public:
    int getRcvSocketID()    { return m_iRcvSocketID; }
    int getSndSocketID()    { return m_iSndSocketID; }
    int getListenSocketID() { return m_iListenSocketID; }
    int getErrorThread()    { return m_iErrThread; }

    void setRcvSocketID(int iRcvSocketID)       { m_iRcvSocketID = iRcvSocketID; }
    void setSndSocketID(int iSndSocketID)       { m_iSndSocketID = iSndSocketID; }
    void setListenSocketID(int iListenSocketID) { m_iListenSocketID = iListenSocketID; }
    void setErrorThread(int iErrThread)         { m_iErrThread = iErrThread; }

  protected:
    #ifndef WIN32	
	  pthread_t m_hThreadTCPIP;
    #else
      HANDLE m_hThreadTCPIP;
    #endif

    int m_iErrThread;
    int m_iRcvSocketID;
    int m_iSndSocketID;
    int m_iListenSocketID;
};
#endif
