//****************************************************************************
/*! \file     PDFTaskt.cpp
 *  \brief    
 *  \author   Michele Fischer
 *  \date     2009
 ****************************************************************************/

// Acrobat Headers.
#ifndef MAC_PLATFORM
	#include "PIHeaders.h"
	#include "windows.h"
#else
	#include "MacPlatform.h"
	#include "PDBasicExpT.h"
	#include "avexpt.h"
#endif


#include "SwitchAcrobat.h"
#include "PDFTask.h"

#include <fstream>
#include <sstream> 
#include <string>

using namespace std;

/*********************************************************************
  Konstruktor
*********************************************************************/
PDFTask::PDFTask()
{
}

/*********************************************************************
  Destruktor
*********************************************************************/
PDFTask::~PDFTask()
{
}

/****************************************************************************/
/*!	\brief	open PDF file
 *	\param  sPDFPath        the path to the PDF file
 *	\return 
 ****************************************************************************/
bool PDFTask::openPDFFile(string sPDFPath) 
{
  bool success = false;      
  stringstream ss;  
   
  const char* myPath = sPDFPath.c_str();
  
  DURING  
	 
	ASAtom pathType = ASAtomFromString("Cstring");

	//Create an ASText object
	ASText titleText = ASTextNew();
	ASTextSetPDText(titleText, "This PDF was opened by JPSClient");

    //Create an ASPathName object
    ASFileSys fileSys = ASGetDefaultFileSysForPath(pathType, myPath);
    ASPathName pathName = ASFileSysCreatePathName(fileSys, pathType, myPath, NULL);
	

    //Open the PDF file
    AVDoc myDoc = AVDocOpenFromFile(pathName, NULL, titleText);
    gApplication.setAVDoc(myDoc);

    PDDoc myPDDoc = AVDocGetPDDoc(myDoc);
    gApplication.setPDDoc(myPDDoc);

    //Do some clean up
    ASFileSysReleasePath(fileSys, pathName);
    ASTextDestroy(titleText);

    success = true;

  HANDLER
    AVAlertNote ("Cannot open document.");
  END_HANDLER

  return success;
}

/*********************************************************************
  closeAndSavePDFFile
*********************************************************************/
bool PDFTask::closeAndSavePDFFile() 
{
  bool   success    = false;      
  PDDoc  myPDDoc    = gApplication.getPDDoc();
  string sFilePath  = gApplication.getPDFPath();

  if(!myPDDoc)return success; 

  AVDoc myAVDoc = gApplication.getAVDoc();
 
  DURING   
  
    ASPathName fPath = ASFileSysCreatePathName(NULL, ASAtomFromString("Cstring"), sFilePath.c_str(), 0);
	PDDocSave(myPDDoc, PDSaveFull | PDSaveLinearized, fPath, NULL, NULL, NULL);
    AVDocClose(myAVDoc, true);
 
	success = true;

  HANDLER

    AVAlertNote ("Cannot save document.");
    AVDocClose(myAVDoc, true);

  END_HANDLER
	return success;
}