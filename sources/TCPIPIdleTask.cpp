//****************************************************************************
/*! \file
 *  \brief    Sourcedatei: TCP/IP-Kommunikation
 *  \author   Hans St�ssel
 *  \date     2004
 *  \version  1.0.0
 *  \bug      
 *  \warning  
 *  \todo     
 ****************************************************************************/

// Acrobat Headers.
#ifndef MAC_PLATFORM
#include "PIHeaders.h"
#endif

// Project includes
#include "TCPIPIdleTask.h"
#include "SwitchAcrobat.h"
#include "TCPIPSocket.h"

/*********************************************************************
  Konstruktor
*********************************************************************/
TCPIPIdleTask::TCPIPIdleTask()
{
}

/*********************************************************************
  Idle function
*********************************************************************/
ACCB1 void ACCB2 idleProc( void * clientData)
{ 
  TCPIPSocket*  pTCPIPSocket = gApplication.getTCPIPSocket();  
  bool bDataReceived    = false;   

  do {
    if (pTCPIPSocket->getRcvSocketID() >= 0 && !gApplication.getFileOpen())
    {
        pTCPIPSocket->TCPIP_Rcv_Data(&bDataReceived);   
        if (gApplication.getPDFPath().empty())
            pTCPIPSocket->TCPIP_Call_Back(-1, -1); //  When the path is empty recall the host application.
        else
            gApplication.openPDFFile();
        
    }
  } while(false); 
}

/*********************************************************************
  ionitialize idle
*********************************************************************/
void TCPIPIdleTask::initializeIdle()
{
  ASInt32  delay=0; 
  AVAppRegisterIdleProc  (idleProc,NULL, delay);  
}
