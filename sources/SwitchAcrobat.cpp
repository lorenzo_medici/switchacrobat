/*********************************************************************

 ADOBE SYSTEMS INCORPORATED
 Copyright (C) 1998-2006 Adobe Systems Incorporated
 All rights reserved.

 NOTICE: Adobe permits you to use, modify, and distribute this file
 in accordance with the terms of the Adobe license agreement
 accompanying it. If you have received this file from a source other
 than Adobe, then your use, modification, or distribution of it
 requires the prior written permission of Adobe.

 -------------------------------------------------------------------*/
/** 
\file SwitchAcrobat.cpp

  - This file implements the functionality of the BasicPlugin.
*********************************************************************/


// Acrobat Headers.
#ifndef MAC_PLATFORM
	#include "PIHeaders.h"
	#include "MkDir.h"
#else
	#include "MacPlatform.h"
	#include "PDBasicExpT.h"
	#include "avexpt.h"
#endif

#include "stdlib.h"
#include "Plugin.h"
#include "SwitchAcrobat.h"
#include "TCPIPSocket.h"
#include "TCPIPIdleTask.h"
#include "PDFTask.h"
#include "TextFile.h"
#include "QXT4StringUtil.h"
#include "string.h"

#include <sstream>
#include <string>

#define kPointsPerInch 72.0
#define kMMPerInch 25.4

SwitchAcrobat::SwitchAcrobat()
{
  m_bReadConfigFile = true;
  m_bFileOpen       = false;
}

/*-------------------------------------------------------
	Constants/Declarations
-------------------------------------------------------*/
// this plug-in's name, you should specify your own unique name here.
const char* MyPluginExtensionName = "ADBE:SwitchAcrobat";

/* A convenient function to add a menu item for your plugin.
** A policy in Acrobat 6 is to add menu items for developers' plugins under Advanced menu.
** If specify bUnderAcrobatSDKSubMenu as false, you directly add your menu item under Advanced menu.
** If specify bUnderAcrobatSDKSubMenu as true, you add the menu item under Acrobat SDK sub menu.
*/
ACCB1 ASBool ACCB2 PluginMenuItem(char* MyMenuTitle, char* MyMenuName, char* MyMenuItemTitle, char* MyMenuItemName, ASExtension myGExtensionID);

/*-------------------------------------------------------
	Functions
-------------------------------------------------------*/

/* MyPluginSetmenu
** ------------------------------------------------------
**
** Function to set up menu for the plugin.
** It calls a convenient function PluginMenuItem.
** Return true if successful, false if failed.
*/
ACCB1 ASBool ACCB2 MyPluginSetmenu()
{

  // Add a new menu item under Acrobat SDK submenu.
  // The new menu item name is "ADBE:BasicPluginMenu", title is "Basic Plugin".
  // Of course, you can change it to your own.
    char strMenuTitle [] = "JPSClient";
    char strMenuName []= "ADBE:AdFS2LinkAcrobatMenu";
    char strMenuItemTitle [] = "Save and go back...";
    char strMenuItemName [] = "ADBE:AdFS2LinkAcrobatMenuItem";
  return PluginMenuItem(strMenuTitle, strMenuName, strMenuItemTitle, strMenuItemName, gExtensionID);
}
	

/**		BasicPlugin project is an Acrobat plugin sample with the minimum code 
	to provide an environment for plugin developers to get started quickly.
	It can help Acrobat APIs' code testing, too.  
		This file implements the functionality of the BasicPlugin. It adds a 
	new menu item that will show a message of some simple information about 
	the plugin and front PDF document. Users can modify and add code in this 
	file only to make a simple plugin of their own.   
		
		  MyPluginCommand is the function to be called when executing a menu.
	This is the entry point for user's code, just add your code inside.

	@see ASExtensionGetRegisteredName
	@see AVAppGetActiveDoc
	@see PDDocGetNumPages
*/ 
ACCB1 void ACCB2 MyPluginCommand(void *clientData)
{
    PDFTask myPDFTask;
    int iWidth = -1, iHeight = -1;
    gApplication.GetDocumentSize (iWidth, iHeight);
    bool bFileClosed = myPDFTask.closeAndSavePDFFile();

    if(bFileClosed){
        gApplication.setFileOpen(false);
        gApplication.getTCPIPSocket()->TCPIP_Call_Back(iWidth, iHeight);
    }
    return;
}

/* MyPluginIsEnabled
** ------------------------------------------------------
** Function to control if a menu item should be enabled.
** Return true to enable it, false not to enable it.
*/
ACCB1 ASBool ACCB2 MyPluginIsEnabled(void *clientData)
{
  // always enabled.
  return true;
  // this code make it is enabled only if there is a open PDF document. 
  /* return (AVAppGetActiveDoc() != NULL); */
}

/****************************************************************************/
/*!	\brief	calls the open PDF function in class PDFTask
 ****************************************************************************/
bool SwitchAcrobat::openPDFFile()
{
  PDFTask myPDFTask;

  bool bFileOpen = myPDFTask.openPDFFile(gApplication.getPDFPath());

  m_bFileOpen = bFileOpen;
  return bFileOpen;
}

/****************************************************************************/
/*!	\brief	set up the communication
 ****************************************************************************/
int SwitchAcrobat::setUpCommunication()
{ 
  TCPIPSocket*  pTCPIPSocket = gApplication.getTCPIPSocket(); 

  int myError = ERR_NO;
  int iError  = readConfigFile();

  if(iError == ERR_NO){
    pTCPIPSocket->TCPIP_Init_Listening();
  }
 return myError;
}


//****************************************************************************
/*! \brief  Konfigurationsfile lesen
 *  \return Errorcode
 ****************************************************************************/
int SwitchAcrobat::readConfigFile()
{
  int             myerr = ERR_NO; 
  string          s = "";
  string          sParam[] = { "[IPAddress]=", "[Port]=" };
  CQXT4StringUtil oStrUtil;
  string          sConfigFile = "";

  if (!m_bReadConfigFile) return myerr;

  #ifdef MAC_PLATFORM
	CFURLRef fileLocation = CFBundleCopyBundleURL(CFBundleGetMainBundle());
	ASPathName aspn = ASFileSysCreatePathName(NULL,ASAtomFromString("CFURLRef"), fileLocation,NULL); 
	sConfigFile = ASFileSysDIPathFromPath(NULL,aspn,NULL);
	sConfigFile += "Contents/ConfigurationPMM/Switch.cfg";
	int iPos = sConfigFile.find("/Applications");
	sConfigFile = sConfigFile.substr(iPos);
  #endif
  
  #ifdef WIN32
	char dir[_MAX_PATH]; 
	GetModuleFileName(NULL, dir, _MAX_PATH);
	sConfigFile = string(dir);
	int iPos = sConfigFile.find("\\Acrobat.exe");
	sConfigFile = sConfigFile.substr(0, iPos);
	sConfigFile += "\\plug_ins\\PMM\\Switch.cfg";
  #endif
  

  #if MACINTOSH
    //oStrUtil.convertNetworkPathToHDPath(sConfigFile);
    //oStrUtil.convertWinPathToMacPath(sConfigFile);
  #endif  

  CTextFile oTextFile(sConfigFile); 
  myerr = oTextFile.openForReading();
  if (myerr == ERR_NO) {
    do {
      myerr = oTextFile.readLine(&s);
      oStrUtil.trimLeftIgnoreCase(s, ' ');
      oStrUtil.trimRightIgnoreCase(s, ' ');
      if (oStrUtil.firstIndexOfIgnoreCase(s, sParam[0]) == 0) {
        s = s.substr(sParam[0].length(), s.length());
        s = oStrUtil.trimLeftIgnoreCase(s, ' ');
        s = oStrUtil.trimRightIgnoreCase(s, ' ');
        m_sIPAddress = s;
      }
      if (oStrUtil.firstIndexOfIgnoreCase(s, sParam[1]) == 0) {
        s = s.substr(sParam[1].length(), s.length());
        oStrUtil.trimLeftIgnoreCase(s, ' ');
        oStrUtil.trimRightIgnoreCase(s, ' ');
        m_iPort = atoi(s.c_str());
      }
    } while (myerr == ERR_NO);
    if (myerr == ERR_FILE_EOF) myerr = ERR_NO;
    oTextFile.close();
    m_bReadConfigFile = false;
  } else {
    string sError = "Cannot read confic file. [" + sConfigFile + "]";
    const char* cError = sError.c_str();
    AVAlertNote (cError);
  } 
  return myerr;
}

void SwitchAcrobat::GetDocumentSize(int &iWidth, int &iHeight)
{
    PDDoc oPDDoc = gApplication.getPDDoc();
    if(!oPDDoc)
        return;

    PDPage oPage = PDDocAcquirePage (oPDDoc, 0);
    ASFixedRect oMediaBoxP;
    PDPageGetCropBox (oPage, &oMediaBoxP);
    
    float iWidthPPI = fabs(ASFixedToFloat(oMediaBoxP.right) - ASFixedToFloat(oMediaBoxP.left));
    float iHeightPPI = fabs(ASFixedToFloat(oMediaBoxP.bottom) - ASFixedToFloat(oMediaBoxP.top));

    iWidth = round((iWidthPPI/kPointsPerInch) * kMMPerInch); // convert points to millimetre.
    iHeight = round((iHeightPPI/kPointsPerInch) * kMMPerInch); // convert points to millimetre.
}
