
// Acrobat Headers.
#ifndef MAC_PLATFORM
	#include "PIHeaders.h"
	#include <winsock.h>
	#include "windows.h"
#endif

#include "SwitchAcrobat.h"
#include "TCPIPSocket.h"
#include "OS_Sockets.h"
#include "Constants.h"
#include "IParamBlock.h"
#include "QXT4StringUtil.h"

#include <stdio.h>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

#ifndef WIN32
  #include "pthread.h"
#endif

/****************************************************************************/
/*!	\brief	Thread-Funktion, TCPIP-Socket-Listening
 *	\param
 *	\return
 ****************************************************************************/
#ifndef WIN32
static void* TCPID_Listening_Thread(void* p)
#else
DWORD WINAPI TCPID_Listening_Thread(void* p)
#endif
{
  int myerr             = 0;    
  int iListenSocketID   = 0;
  int iDataSocketID     = 0;
  int iPort             = gApplication.getPort();
  string sIPAddress     = gApplication.getIPAddress();
  char *pIPAddress      = new char[sIPAddress.length() + 1];

  strcpy(pIPAddress, sIPAddress.c_str());

  TCPIPSocket*  pTCPIPSocket = static_cast<TCPIPSocket*>(p);  
   
  myerr = tcp_Init_Listening(pIPAddress, iPort, &iListenSocketID);
  pTCPIPSocket->setErrorThread(myerr);
  pTCPIPSocket->setListenSocketID(iListenSocketID);
  if (myerr == noErr) {
    do {
      myerr = tcp_Listen(pTCPIPSocket->getListenSocketID(), &iDataSocketID);
      if (myerr == noErr && !gApplication.getFileOpen()) {
        pTCPIPSocket->setRcvSocketID(iDataSocketID);  
      }
    } while (myerr == noErr && true);
  } else {
    AVAlertNote ("Cannot initialize TCP listening.");
  }
  return 0;
}

/*********************************************************************
  Konstruktor
*********************************************************************/
TCPIPSocket::TCPIPSocket()
{
  #ifdef WIN32
    tcp_Init();
  #endif

  m_iRcvSocketID = -1;
  m_iSndSocketID = -1;
  m_iListenSocketID = -1;
  
  #ifndef WIN32	
	m_hThreadTCPIP = nil;
  #else
    m_hThreadTCPIP = NULL;
  #endif
}

/*********************************************************************
  Destruktor
*********************************************************************/
TCPIPSocket::~TCPIPSocket()
{
  TCPIP_Close_Listening(false);

  #if WINOS	
    tcp_Release();
  #endif	
}

/****************************************************************************/
/*!	\brief	Schliesst das Warten auf einen TCP/IP-Aufruf
 *	\return	Errorcode
 ****************************************************************************/
int TCPIPSocket::TCPIP_Close_Listening(bool bCloseThread)
{
  if (m_iListenSocketID >= 0) {
    tcp_Close(m_iListenSocketID);
    m_iListenSocketID = -1;
  }

  #ifndef WIN32
	if (m_hThreadTCPIP != nil && bCloseThread) {
	  pthread_detach(m_hThreadTCPIP);
      m_hThreadTCPIP = NULL;
	}
  #else
	if (m_hThreadTCPIP != NULL && bCloseThread)	{
      CloseHandle(m_hThreadTCPIP);
      m_hThreadTCPIP = NULL;
	}
  #endif

  return noErr;
}

/****************************************************************************/
/*!	\brief	Initialisiert das Warten auf einen TCP/IP-Aufruf
 *	\return	Errorcode
 ****************************************************************************/
int TCPIPSocket::TCPIP_Init_Listening()
{
  int iErr = 0;

  TCPIP_Close_Listening(true);
  
  #ifdef WIN32
    DWORD iThreadID;
    m_hThreadTCPIP = CreateThread(NULL, 0, TCPID_Listening_Thread, this, 0, &iThreadID);
  #else
    if (m_hThreadTCPIP == nil) {
      iErr = pthread_create(&m_hThreadTCPIP, nil, TCPID_Listening_Thread,  this);
    }
  #endif
 
  return iErr;
}

/****************************************************************************/
/*!	\brief	Empfangen des TCP/IP-Sockets zum Empfangen von Daten
 *	\return	Errorcode
 ****************************************************************************/
int	TCPIPSocket::TCPIP_Rcv_Data(bool* pDataReceived)
{
  CQXT4StringUtil oStrUtil;
  int	          iErr        = noErr;
  int             iPos;
  int             iTerminated = 0;
  string          sData       = "";
  string          sMethod     = "";
  string          sPDFPath    = "";
  char            s[255 + 1];
 
  *pDataReceived = false;

  do {
	if (m_iRcvSocketID < 0) {
      return ERR_SOCKET_NOSOCKET;
    }
    while (iErr == noErr && iTerminated == 0) {
      iErr = tcp_ReceiveString(m_iRcvSocketID, (char*) s, 255, (char*) "\r\n", &iTerminated);
      sData += (char*) s;
    }
    m_iSndSocketID = m_iRcvSocketID;
    m_iRcvSocketID = -1;
    if (iErr == noErr) {
      iPos = sData.find(' ');
      if (iPos > 0) {
        sMethod = sData.substr(0, iPos); 
        if(oStrUtil.equalsIgnoreCase(sMethod, "pmm_CallSwitchAcrobat")){
          sPDFPath = TCPIP_get_PDFPath(sData);
          gApplication.setPDFPath(sPDFPath);
        }
       *pDataReceived = iErr == ERR_NO;
      }
	}
  } while (false);
 return iErr;
}

/****************************************************************************/
/*!	\brief	Empfangt die gesendetetn Daten
 *  \param  Zu konvertierender String
 *	\return	Konvertierter String
 ****************************************************************************/
string TCPIPSocket::TCPIP_get_PDFPath(string sData)
{
  CQXT4StringUtil oStringUtil;

  string sAppVersion      = ""; 
  string sAppName         = "";
  string sProtokollFlag   = "";
  string sPDFPath         = "";  

  int iPos                = -1;
  int iParam              = 0;
  int iLen                = sData.length();
  int iNoOfParam          = 0;

  string sParam;
  vector<string> vParam;

  iPos = sData.find(" \"");
  if (iPos > 0) {
    iParam = 0;
    sData = sData.substr(iPos + 2, sData.length() - iPos - 2);
    oStringUtil.split(sData, "\" \"", vParam);
    iNoOfParam = (int) vParam.size();
    for (iParam = 0; iParam < iNoOfParam; iParam++) {
      sParam = convertFromSocket(vParam[iParam]);
      iLen = sParam.length();
      switch (iParam) {
        case 0:   sAppVersion    = sParam; break;
        case 1:   sAppName       = sParam; break;
        case 2:   sPDFPath       = sParam; break;
        case 3:   sProtokollFlag = sParam; break;
      }
    }
  }
  
  #ifndef WIN32
	oStringUtil.convertWinPathToMacPath(sPDFPath);
  #endif
  
 return sPDFPath;
}

/****************************************************************************/
/*!	\brief	Senden, dass keine Lizenz verf�gbar ist via TCP/IP-Socket
 *	\return	Errorcode
 ****************************************************************************/
void TCPIPSocket::TCPIP_Call_Back(int iDocWidth, int iDocHeight)
{
  char  s[255 + 1];

  do {
    sprintf((char*) s, "pmm_CallHost_AcrobatFileSaved \"%d\" \"%d\"\n", iDocWidth, iDocHeight);
    tcp_Send(m_iSndSocketID, (char*) s);
  } while (false);
}

/****************************************************************************/
/*!	\brief	Konvertiert Spezialzeichen in einem String, der via Socket
 *          empfangen wurde
 *  \param  Zu konvertierender String
 *	\return	Konvertierter String
 ****************************************************************************/
string  TCPIPSocket::convertFromSocket(string s)
{
  CQXT4StringUtil oStrUtil;

  oStrUtil.replaceStr(s, "\\\"", "\"");
  oStrUtil.replaceStr(s, "\\n", "\n");
  oStrUtil.replaceStr(s, "\\r", "\r");
  return s;
}

