#ifndef	__OS_SOCKETS_H__
#define	__OS_SOCKETS_H__

#ifndef __ODFRC__

#ifdef __cplusplus
  extern "C" {
#endif

#ifndef noErr
  #define noErr 0
#endif  

#ifndef NULL
  #define NULL ((void*) 0)
#endif  

#endif

#define	ERR_SOCKET_NOSOCKET		      -10000
#define	ERR_SOCKET_NOCONNECT		    -10001
#define	ERR_SOCKET_ZERODATALENGTH	  -10002
#define ERR_SOCKET_PORTINUSE		    -10003
#define ERR_SOCKET_LISTENING		    -10004
#define ERR_SOCKET_SEND		          -10005
#define ERR_SOCKET_RECEIVE          -10006
#define ERR_SOCKET_BIND             -10007
#define ERR_SOCKET_RECVFROM         -10008
#define ERR_SOCKET_RECVFROMTIMEOUT  -10009
#define ERR_SOCKET_GETSOCKNAME      -10010
#define ERR_SOCKET_SENDTO           -10011
#define ERR_SOCKET_SETSOCKOPT       -10012
#define ERR_SOCKET_NOTINITIALIZED   -10013

#ifndef __ODFRC__

#define _BSD_SOCKLEN_T_ int32_t

#ifndef WIN32
	#if __MACH__
	  #if defined INDESIGN_PROJECT || defined __GNUC__
  	  #include "arpa/inet.h"
      #include "sys/types.h"
      #include "sys/socket.h"
      #include "netinet/in.h"
#ifdef __GNUC__      
      #include "unistd.h"
#endif      
	  #else
      #include "/usr/include/sys/types.h"
      #include "/usr/include/sys/socket.h"
      #include "/usr/include/netinet/in.h"
 	  #endif
  #else  
    #include "OS_SocketsCarbon.h"
  #endif  
#endif

#ifdef WIN32
  int		tcp_Init();
  void	tcp_Release();
#endif
void  tcp_getTCPIDFromLocalHost(char* sTCPIP);
int		tcp_Connect(char *ServerIP,int theport,int *socketID);
int		tcp_Send(int socketID, char* sData);
int		tcp_Receive(int socketID, char* sData, int iLen);
int 	tcp_ReceiveString255(int socketID, char* sData);
int   tcp_ReceiveString(int iSocketID, char* sData, int iLen, char* sTerminate, int* bTerminated);
int 	tcp_ReceiveString32767(int iSocketID, char* sData);
int 	tcp_Init_Listening(char* sRemoteHost, int iPort, int* iListenSocketID);
int 	tcp_Listen(int iListenSocketID, int* iDataSocketID);
void  tcp_Close(int socketID);
int	  tcp_SendABroadcast(int iPort, int* iSocketID, struct sockaddr_in* pSndAdd, int* iLenSndAdd, char* sMessage);
int	  tcp_ListenForABroadcastAnswer(int iSocketID, struct sockaddr_in* pSndAdd, int* iLenSndAdd, char* sBuffer, int iBufferLen);
int   tcp_ListenForABroadcast(int iPort, int* iSocketID, struct sockaddr_in* pSndAdd, int* iLenSndAdd, char* sBuffer, int iBufferLen);
int   tcp_SendBroadcastAnswer(int iSocketID, struct sockaddr_in* pSndAdd, int iLenSndAdd, char* sAnswer, int bCloseSocket);

int   tcp_os_isinitialized();

#ifdef WIN32
  #define tcp_os_accept accept
  #define tcp_os_inet_pton inet_pton
  #define tcp_os_close closesocket
  #define tcp_os_connect connect
  #define tcp_os_socket socket
  #define tcp_os_send send
  #define tcp_os_recv recv
  #define tcp_os_bind bind
  #define tcp_os_listen listen
  #define tcp_os_setsockopt setsockopt
#else	  
  #ifdef __MACH__
    #define tcp_os_accept accept
    #define tcp_os_inet_pton inet_pton
    #define tcp_os_close close
    #define tcp_os_connect connect
    #define tcp_os_socket socket
    #define tcp_os_send send
    #define tcp_os_recv recv
    #define tcp_os_bind bind
    #define tcp_os_listen listen
    #define tcp_os_setsockopt setsockopt
  #else
    #define tcp_os_accept (*acceptProc)
    #define tcp_os_inet_pton (*inet_ptonProc)
    #define tcp_os_close (*closeProc)
    #define tcp_os_connect (*connectProc)
    #define tcp_os_socket (*socketProc)
    #define tcp_os_send (*sendProc)
    #define tcp_os_recv (*recvProc)
    #define tcp_os_bind (*bindProc)
    #define tcp_os_listen (*listenProc)
  #endif
#endif

#ifdef __cplusplus
  }
#endif

#endif

#endif