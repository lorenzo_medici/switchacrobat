#include "OS_Sockets.h"

#ifdef WIN32
  #include "WinSock.h"
#else
  #include "netdb.h"
#endif

//***********************************************************************************************
//	tcp_Connect
//***********************************************************************************************
int	tcp_Connect(char* sServerIP, int iPort, int* iSocketID)
{
	struct sockaddr_in  serverAddress;

  if (tcp_os_isinitialized() != noErr) return ERR_SOCKET_NOTINITIALIZED;

  *iSocketID = (int) tcp_os_socket(AF_INET, SOCK_STREAM, 0);

	if (*iSocketID == (int) INADDR_NONE) {
		return ERR_SOCKET_NOSOCKET;
	}
	else {
#ifndef WIN32
		memset(&serverAddress,0, sizeof(serverAddress));
#endif
	  serverAddress.sin_family = AF_INET;
#ifndef WIN32
	  serverAddress.sin_port = htons( iPort );
		tcp_os_inet_pton(AF_INET, sServerIP, &serverAddress.sin_addr);
#else
	  serverAddress.sin_port = htons( (unsigned short) iPort);
	  serverAddress.sin_addr.s_addr = inet_addr(sServerIP);
#endif
		if (tcp_os_connect(*iSocketID, (struct sockaddr*) &serverAddress, sizeof(serverAddress)) < 0) {
			return ERR_SOCKET_NOCONNECT;
		} else {
			return 0;
		}
	}
}

//***********************************************************************************************
//	tcp_Send
//***********************************************************************************************
int tcp_Send(int iSocketID, char* sData)
{
	int len 	= 0;
	int	iErr = ERR_SOCKET_ZERODATALENGTH;

  if (tcp_os_isinitialized() != noErr) return ERR_SOCKET_NOTINITIALIZED;
	
	len = (int) strlen(sData);
	if (len > 0)
  {
		iErr = tcp_os_send(iSocketID, sData, len + 1, 0) == len + 1 ? 0 : ERR_SOCKET_SEND;
	}
	return iErr;
}

//***********************************************************************************************
//	tcp_Receive
//***********************************************************************************************
int tcp_Receive(int iSocketID, char* sData, int iLen)
{	
	int iErr = 0;
	int rcvLen = 0;
	
  if (tcp_os_isinitialized() != noErr) return ERR_SOCKET_NOTINITIALIZED;

	rcvLen = tcp_os_recv(iSocketID, sData, iLen, 0);
	if (rcvLen >= 0) {
	  sData[rcvLen] = '\0';
	} else {
	  iErr = ERR_SOCKET_RECEIVE;
	}
	return iErr;
}

//***********************************************************************************************
//	tcp_ReceiveString
//***********************************************************************************************
int tcp_ReceiveString255(int iSocketID, char* sData)
{
  return tcp_Receive(iSocketID, sData, 255);
}

//***********************************************************************************************
//	tcp_ReceiveText
//***********************************************************************************************
int tcp_ReceiveString32767(int iSocketID, char* sData)
{
  return tcp_Receive(iSocketID, sData, 32767);
}

//***********************************************************************************************
//	tcp_ReceiveString
//***********************************************************************************************
int tcp_ReceiveString(int iSocketID, char* sData, int iLen, char* sTerminateChars, int* iTerminated)
{	
  char  s[2];
	int   iErr = 0;
  int   i = 0;
  int   iData = 0;
	int   rcvLen = 0;
	
  *iTerminated = 0;
  if (tcp_os_isinitialized() != noErr) return ERR_SOCKET_NOTINITIALIZED;

  sData[0] = '\0';
  while (*iTerminated == 0 && iData < iLen && (rcvLen = tcp_os_recv(iSocketID, s, 1, 0)) >= 0) {
  	if (rcvLen >= 0) {
      s[1] = '\0';
      for (i = 0; *iTerminated == 0 && i < (int) strlen(sTerminateChars); i++) {
        *iTerminated = (sTerminateChars[i] == s[0]) ? 1 : 0;
      }
      if (*iTerminated == 0) {
        sData[iData++] = s[0];
      }
	  } else {
	    iErr = ERR_SOCKET_RECEIVE;
	  }
  }
  sData[iData] = '\0';
  return iErr;
}

//***********************************************************************************************
//	tcp_Init_Listening
//***********************************************************************************************
int tcp_Init_Listening(char* sRemoteHost,
                       int iPort,
                       int* iListenSocketID)
{
	struct sockaddr_in  serverAddress;
	int		              iErr = 0;
	int		              iOption = 0;

  *iListenSocketID = INADDR_NONE;
  if (tcp_os_isinitialized() != noErr) return ERR_SOCKET_NOTINITIALIZED;
	
	*iListenSocketID = (int) tcp_os_socket(AF_INET, SOCK_STREAM, 0);

	if (*iListenSocketID == (int) INADDR_NONE) {
		iErr =  ERR_SOCKET_NOSOCKET;
	}
	else
	{
#ifndef WIN32
	  memset(&serverAddress, 0, sizeof(serverAddress));
#endif
	  serverAddress.sin_family = AF_INET;
#ifndef WIN32
	  serverAddress.sin_port = htons(iPort);
#else
	  serverAddress.sin_port = htons((unsigned short) iPort);
#endif
	  serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);

    iOption = 1;
	  if (tcp_os_setsockopt(*iListenSocketID, SOL_SOCKET, SO_REUSEADDR, (char*) &iOption, sizeof(iOption)) != 0)
	  {
	   	tcp_Close(*iListenSocketID);
	   	iErr = ERR_SOCKET_PORTINUSE;
	  }

    if (tcp_os_bind(*iListenSocketID, (struct sockaddr*) &serverAddress, sizeof(serverAddress)) != 0)
	  {
	   	tcp_Close(*iListenSocketID);
	   	iErr = ERR_SOCKET_PORTINUSE;
	  }
	  if (tcp_os_listen(*iListenSocketID, 1) != 0)
	  {
	   	iErr = ERR_SOCKET_LISTENING;
	  }
	}
	return iErr;
}

//***********************************************************************************************
//	tcp_Listen
//***********************************************************************************************
int tcp_Listen(int iListenSocketID,
               int* iDataSocketID)
{
	int		              iErr = 0;

  *iDataSocketID = 0;
  if (tcp_os_isinitialized() != noErr) return ERR_SOCKET_NOTINITIALIZED;

  *iDataSocketID = (int) tcp_os_accept(iListenSocketID,
                                       NULL,
                                       NULL);
	if (*iDataSocketID == -1)	{
		iErr = -1;
	}	else {
		iErr = 0;
	}
	return iErr;
}

//***********************************************************************************************
//	tcp_close
//***********************************************************************************************
void tcp_Close(int iSocketID)
{
  if (tcp_os_isinitialized() == noErr) {
    tcp_os_close(iSocketID);
  }
}

#ifdef WIN32

//***********************************************************************************************
//	tcp_Init
//***********************************************************************************************
int	tcp_Init()
{
  WSADATA wsaData;

  return WSAStartup(MAKEWORD(2,2), &wsaData);
}

//***********************************************************************************************
//	tcp_Release
//***********************************************************************************************
void tcp_Release()
{
  WSACleanup();
}

//***********************************************************************************************
//	tcp_os_isinitialized
//***********************************************************************************************
int tcp_os_isinitialized()
{
  return noErr;
}

#else

#ifdef __MACH__

//***********************************************************************************************
//	tcp_os_isinitialized
//***********************************************************************************************
int tcp_os_isinitialized()
{
  return noErr;
}

#endif

#endif

//***********************************************************************************************
//	tcp_SendBroadcast
//***********************************************************************************************
int	tcp_SendABroadcast(int iPort, int* iSocketID, struct sockaddr_in* pSndAdd, int* iLenSndAdd, char* sMessage)
{
	int	                iErr		= 0;
  int                 iBroadcast = 1;

  *iSocketID = (int) socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (*iSocketID == (int) INADDR_NONE) {
		return ERR_SOCKET_NOSOCKET;
	}	else {
	  pSndAdd->sin_family = AF_INET;
	  pSndAdd->sin_port = htons(0);
	  pSndAdd->sin_addr.s_addr = htonl(INADDR_ANY);
    iErr = bind(*iSocketID, (struct sockaddr*) pSndAdd, sizeof(*pSndAdd));
		if (iErr < 0) {
      shutdown(*iSocketID, 2);
      tcp_Close(*iSocketID);
			return ERR_SOCKET_BIND;
		}
#ifdef INDESIGN_PROJECT
    iErr = setsockopt(*iSocketID,
                       SOL_SOCKET,
                       SO_BROADCAST,
                       (char*) &iBroadcast,
                       sizeof(int));
#else
    iErr = setsockopt(*iSocketID,
                       SOL_SOCKET,
                       SO_BROADCAST,
                       (void*) &iBroadcast,
                       sizeof(int));
#endif
		if (iErr < 0) {
      shutdown(*iSocketID, 2);
      tcp_Close(*iSocketID);
			return ERR_SOCKET_SETSOCKOPT;
		}

	  pSndAdd->sin_family = AF_INET;
	  pSndAdd->sin_port = htons((unsigned short) iPort);
	  pSndAdd->sin_addr.s_addr = htonl(INADDR_BROADCAST);
    iErr = sendto(*iSocketID, sMessage, (int) strlen(sMessage), 0, (struct sockaddr*) pSndAdd, sizeof(*pSndAdd));
		if (iErr < 0) {
      shutdown(*iSocketID, 2);
      tcp_Close(*iSocketID);
			return ERR_SOCKET_SENDTO;
		}
	}
  return 0;
}

//***********************************************************************************************
//	tcp_ListenForABroadcastAnswer
//***********************************************************************************************
int	tcp_ListenForABroadcastAnswer(int iSocketID, struct sockaddr_in* pSndAdd, int* iLenSndAdd, char* sBuffer, int iBufferLen)
{
	int	iErr		= 0;

  memset(sBuffer, 0, iBufferLen);
#ifdef __GNUC__
  iErr = recvfrom(iSocketID, sBuffer, iBufferLen, 0, (struct sockaddr*) pSndAdd, (socklen_t*) iLenSndAdd);
#else
  iErr = recvfrom(iSocketID, sBuffer, iBufferLen, 0, (struct sockaddr*) pSndAdd, iLenSndAdd);
#endif  
  if (iErr >= 0) {
    sBuffer[iErr] = '\0';
    iErr = 0;
  } else {
#if defined INDESIGN_PROJECT || defined __GNUC__
    iErr = ERR_SOCKET_RECVFROM;
#else
    iErr = WSAGetLastError();
    if (iErr == 10060) {
      iErr = ERR_SOCKET_RECVFROMTIMEOUT;
    } else {
      iErr = ERR_SOCKET_RECVFROM;
    }
#endif    
  }
  return iErr;
}

//***********************************************************************************************
//	tcp_ListenForABroadcast
//***********************************************************************************************
int tcp_ListenForABroadcast(int iPort, int* iSocketID, struct sockaddr_in* pSndAdd, int* iLenSndAdd, char* sBuffer, int iBufferLen)
{
	int		              iErr = 0;
	struct sockaddr_in  rcvAdd;
#ifdef WIN32	
  unsigned int        iLenRcvAdd = sizeof(rcvAdd);
#else  
  int                 iLenRcvAdd = sizeof(rcvAdd);
#endif

	*iSocketID = (int) socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (*iSocketID == (int) INADDR_NONE) {
		iErr =  ERR_SOCKET_NOSOCKET;
	}	else {
	  rcvAdd.sin_family = AF_INET;
	  rcvAdd.sin_port = htons((unsigned short) iPort);
	  rcvAdd.sin_addr.s_addr = htonl(INADDR_ANY);

	  iErr = bind(*iSocketID, (struct sockaddr*) &rcvAdd, iLenRcvAdd);
    if (iErr < 0) {
      shutdown(*iSocketID, 2);
	   	tcp_Close(*iSocketID);
	   	return ERR_SOCKET_BIND;
	  }

#ifdef __GNUC__
    iErr = getsockname(*iSocketID, (struct sockaddr*) &rcvAdd, (socklen_t*) &iLenRcvAdd);
#else
#ifdef INDESIGN_PROJECT
    iErr = getsockname(*iSocketID, (struct sockaddr*) &rcvAdd, (int*) &iLenRcvAdd);
#else
    iErr = getsockname(*iSocketID, (struct sockaddr*) &rcvAdd, &iLenRcvAdd);
#endif
#endif
    if (iErr < 0) {
      shutdown(*iSocketID, 2);
	   	tcp_Close(*iSocketID);
	   	return ERR_SOCKET_GETSOCKNAME;
	  }

    memset(sBuffer, 0, iBufferLen);
#ifdef __GNUC__
    iErr = recvfrom(*iSocketID, sBuffer, iBufferLen, 0, (struct sockaddr*) pSndAdd, (socklen_t*) iLenSndAdd);
#else
    iErr = recvfrom(*iSocketID, sBuffer, iBufferLen, 0, (struct sockaddr*) pSndAdd, iLenSndAdd);
#endif
    if (iErr < 0) {
      shutdown(*iSocketID, 2);
	   	tcp_Close(*iSocketID);
	   	return ERR_SOCKET_RECVFROM;
    } else {
      sBuffer[iErr] = '\0';
      iErr = 0;
    }
  }
	return iErr;
}

//***********************************************************************************************
//	tcp_SendBroadcastAnswer
//***********************************************************************************************
int tcp_SendBroadcastAnswer(int iSocketID, struct sockaddr_in* pSndAdd, int iLenSndAdd, char* sAnswer, int bCloseSocket)
{
  int iErr = 0;

  iErr = sendto(iSocketID, sAnswer, (int) strlen(sAnswer), 0, (struct sockaddr*) pSndAdd, iLenSndAdd);
  if (bCloseSocket != 0) {
    shutdown(iSocketID, 2);
    tcp_Close(iSocketID);
  }
  if (iErr >= 0) {
    iErr = 0;
  } else {
    iErr = ERR_SOCKET_SENDTO;
  }
	return iErr;
}

//***********************************************************************************************
//	tcp_getTCPIDFromLocalHost
//***********************************************************************************************
void tcp_getTCPIDFromLocalHost(char* sTCPIP)
{
  struct hostent* pHost = gethostbyname("");

  strcpy(sTCPIP, "");
  if (pHost == NULL) {
    char sHostName[1024];
    sHostName[1023] = '\0';
    gethostname(sHostName, 1023);
    pHost = gethostbyname(sHostName);
  }
  if (pHost != NULL) {
    strcpy(sTCPIP, inet_ntoa(*((struct in_addr*) pHost->h_addr)));
  }
}
