#include "OS_Sockets.h"

#ifndef WIN32
#ifndef __MACH__

AcceptProc acceptProc;
InetPtonProc inet_ptonProc;
CloseProc closeProc;
ConnectProc connectProc;
SocketProc socketProc;
SendProc sendProc;
RecvProc recvProc;
BindProc bindProc;
ListenProc listenProc;

//***********************************************************************************************
//	LoadFrameworkBundle
//***********************************************************************************************
OSStatus LoadFrameworkBundle(CFStringRef pFrameworkCFStrRef, CFBundleRef *pFrameworkCFBndlRef)
{
  OSStatus  err;
  FSRef     frameworksFolderRef;
  CFURLRef  baseURL;
  CFURLRef  bundleURL;
    
  if (nil == pFrameworkCFBndlRef) return paramErr;
   
  *pFrameworkCFBndlRef = nil;
    
  baseURL = nil;
  bundleURL = nil;
    
  err = FSFindFolder(kOnAppropriateDisk, kFrameworksFolderType, true, &frameworksFolderRef);
  if (err == noErr) {
    baseURL = CFURLCreateFromFSRef(kCFAllocatorSystemDefault, &frameworksFolderRef);
    if (baseURL == nil) {
      err = coreFoundationUnknownErr;
    }
  }
  if (err == noErr) {
    bundleURL = CFURLCreateCopyAppendingPathComponent(kCFAllocatorSystemDefault, baseURL, pFrameworkCFStrRef, false);
    if (bundleURL == nil) {
      err = coreFoundationUnknownErr;
    }
  }
  if (err == noErr) {
    *pFrameworkCFBndlRef = CFBundleCreate(kCFAllocatorSystemDefault, bundleURL);
    if (*pFrameworkCFBndlRef == nil) {
      err = coreFoundationUnknownErr;
    }
  }
  if (err == noErr) {
    if (!CFBundleLoadExecutable(*pFrameworkCFBndlRef)) {
      err = coreFoundationUnknownErr;
    }
  }
  // Clean up.

  if (err != noErr && *pFrameworkCFBndlRef != nil) {
    CFRelease(*pFrameworkCFBndlRef);
    *pFrameworkCFBndlRef = nil;
  }
  if (bundleURL != nil) {
    CFRelease(bundleURL);
  }    
  if (baseURL != nil) {
    CFRelease(baseURL);
  }    
  return err;
}

//***********************************************************************************************
//	tcp_InitFuncPointers
//***********************************************************************************************
OSStatus tcp_InitFuncPointers()
{
  OSStatus err = noErr;
   
  if (tcp_os_isinitialized() != noErr) {
    CFBundleRef sysBundle;
    if (noErr == err) { 
      err = LoadFrameworkBundle(CFSTR("System.framework"), &sysBundle); 
    }
    if (noErr == err) {
      acceptProc = reinterpret_cast_(AcceptProc)(CFBundleGetFunctionPointerForName(sysBundle, CFSTR("accept")));
      sendProc = reinterpret_cast_(SendProc)(CFBundleGetFunctionPointerForName(sysBundle, CFSTR("send")));
      recvProc = reinterpret_cast_(RecvProc)(CFBundleGetFunctionPointerForName(sysBundle, CFSTR("recv")));
      closeProc = reinterpret_cast_(CloseProc)(CFBundleGetFunctionPointerForName(sysBundle, CFSTR("close")));
      connectProc = reinterpret_cast_(ConnectProc)(CFBundleGetFunctionPointerForName(sysBundle, CFSTR("connect")));
      inet_ptonProc = reinterpret_cast_(InetPtonProc)(CFBundleGetFunctionPointerForName(sysBundle, CFSTR("inet_pton")));
      socketProc = reinterpret_cast_(SocketProc)(CFBundleGetFunctionPointerForName(sysBundle, CFSTR("socket")));
      bindProc = reinterpret_cast_(BindProc)(CFBundleGetFunctionPointerForName(sysBundle, CFSTR("bind")));
      listenProc = reinterpret_cast_(ListenProc)(CFBundleGetFunctionPointerForName(sysBundle, CFSTR("listen")));
    }
  }
  return err;
}

//***********************************************************************************************
//	tcp_os_isinitialized
//***********************************************************************************************
int tcp_os_isinitialized()
{
  return connectProc != NULL ? noErr : ERR_SOCKET_NOTINITIALIZED;
}

#endif
#endif