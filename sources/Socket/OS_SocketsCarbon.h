#ifndef	__OS_SOCKETSCARBON_H__
#define	__OS_SOCKETSCARBON_H__

#ifdef __cplusplus
#pragma message ("C++")
extern "C" {
#endif

#ifndef WIN32
#ifndef __MACH__

  #ifndef Length_ 
    #define Length_(s) �((int) (unsigned) ((s)[0])) 
  #endif

  #define reinterpret_cast_(aType) (aType) 

  #define _MAC
  #define __PTHREAD_ATTR_SIZE__ 36
  typedef struct _opaque_pthread_attr_t { long sig; char opaque[__PTHREAD_ATTR_SIZE__]; } pthread_attr_t;
  typedef	unsigned char	u_char;
  typedef	unsigned short u_short;
  typedef	unsigned int u_int;
  typedef	unsigned long	u_long;
  typedef	unsigned short ushort;
  typedef	unsigned int uint;
  typedef	char* caddr_t;
  typedef	int32_t pid_t;
  typedef	u_int32_t	uid_t;
  typedef	u_int32_t	gid_t;
  typedef	int	ssize_t;
  typedef	u_int32_t	in_addr_t;
  #include "/usr/include/ppc/endian.h"
  #include "/usr/include/arpa/inet.h"
  #include "/usr/include/sys/socket.h"
  #include "/usr/include/netinet/in.h"
  
  typedef int	(*InetPtonProc)(int, const char*, void*);
  typedef int	(*AcceptProc)(int, struct sockaddr*, socklen_t*);
  typedef int	(*CloseProc)(int);
  typedef int	(*ConnectProc)(int, const struct sockaddr*, socklen_t);
  typedef int	(*SocketProc)(int, int, int);
  typedef ssize_t (*SendProc)(int, const void*g, size_t, int);
  typedef ssize_t	(*RecvProc)(int, void *, size_t, int);
  typedef int	(*BindProc)(int, const struct sockaddr *, socklen_t);
  typedef int	(*ListenProc)(int, int);

  extern AcceptProc acceptProc;
  extern InetPtonProc inet_ptonProc;
  extern CloseProc closeProc;
  extern ConnectProc connectProc;
  extern SocketProc socketProc;
  extern SendProc sendProc;
  extern RecvProc recvProc;
  extern BindProc bindProc;
  extern ListenProc listenProc;

  OSStatus LoadFrameworkBundle(CFStringRef pFrameworkCFStrRef, CFBundleRef *pFrameworkCFBndlRef);
  OSStatus tcp_InitFuncPointers();

#endif
#endif  

#ifdef __cplusplus
}
#endif

#endif