//****************************************************************************
/*! \file
 *  \brief    Sourcedatei f�r die Datumsklasse
 *  \author   Hans St�ssel
 *  \date     2007
 *  \version  1.0.0
 *  \bug      
 *  \warning  
 *  \todo     
 ****************************************************************************/
#ifdef INDESIGN_PROJECT
  #include "VCPlugInHeaders.h"
#endif

#include "datetime.h"

#ifndef WIN32
  #include <ctype.h>
  #ifndef __MACH__
    #include <DateTimeUtils.h>
  #endif
#endif  

#include <string>
using namespace std;

int DATETIME_DAYPERMONTH[] = {365, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}; /*!< Array mit Anzahl Tagen je Monat */

//****************************************************************************
/*! \brief  Konstruktor
 ****************************************************************************/
CDateTime::CDateTime() :
  m_Day(0)
{
  struct tm timeinfo;

  GetLocalTime(time(NULL), &timeinfo);
  //-------------------------------------------------------------------------
  // Setting
  //-------------------------------------------------------------------------
  m_Day   = timeinfo.tm_mday;
  m_Month = timeinfo.tm_mon + 1;
  m_Year  = timeinfo.tm_year + 1900;
  m_Sec   = timeinfo.tm_sec;
  m_Min   = timeinfo.tm_min;
  m_Hour  = timeinfo.tm_hour;
  //-------------------------------------------------------------------------
  // Test
  //-------------------------------------------------------------------------
  Validate (); 
}

//****************************************************************************
/*! \brief  Konstruktor
 *  \param  Time Datum/Zeit-Objekt (CDateTime) zum Initialisieren der Startwerte
 ****************************************************************************/
CDateTime::CDateTime (const CDateTime& Time)
{
  *this = Time;
  Validate();
}

//****************************************************************************
/*! \brief  Konstruktor
 *  \param  Jahr Startwert f�r das Jahr
 *  \param  Monat Startwert f�r den Monat
 *  \param  Tag Startwert f�r den Tag
 *  \param  Std Startwert f�r die Stunden
 *  \param  Min Startwert f�r die Minuten
 *  \param  Sec Startwert f�r die Sekunden
 ****************************************************************************/
CDateTime::CDateTime (const int Jahr, const int Monat, const int Tag, const int Std, const int Min, const int Sec)
{
  Set(Jahr, Monat, Tag, Std, Min, Sec);
}

//****************************************************************************
/*! \brief  Destruktor
 ****************************************************************************/
CDateTime::~CDateTime ()
{
}

//****************************************************************************
/*! \brief  Datum-/Zeit-Objet zuweisen
 *  \param  Time Datum/Zeit-Objekt (CDateTime) zum Zuweisen
 *  \return Datum/Zeit-Objekt (this)
 ****************************************************************************/
CDateTime& CDateTime::operator = (const CDateTime& Time)
{
  m_Year  = Time.m_Year;
  m_Month = Time.m_Month;
  m_Day   = Time.m_Day;
  m_Hour  = Time.m_Hour;
  m_Min   = Time.m_Min;
  m_Sec   = Time.m_Sec;
  Validate();
  return *this;
}

#if defined TIMESTAMP_STRUCT

//****************************************************************************
/*! \brief  Datum-/Zeit-Objet zuweisen
 *  \param  Time Datum/Zeit-Objekt (TIMESTAMP_STRUCT) zum Zuweisen
 *  \return Datum/Zeit-Objekt (this)
 ****************************************************************************/
CDateTime& CDateTime::operator = (const TIMESTAMP_STRUCT Time)
{
  m_Year  = Time.year;  
  m_Month = Time.month; 
  m_Day   = Time.day;   
  m_Hour  = Time.hour;  
  m_Min   = Time.minute;
  m_Sec   = Time.second;
  Validate();
  return *this;
}

#endif

//****************************************************************************
/*! \brief  Gr�sser-als-Operator
 *
 *  Die Funktion gibt true zur�ck, wenn das Datums-/Zeitobjekt gr�sser ist als
 *  das zu vergleichende Datums-/Zeitobjekt, Gr�sser heisst in diesem Fall
 *  Datum/Zeit des Objekts sind neuer, j�nger.
 *  j�nger
 *  \param Time Datum/Zeit-Objekt (CDateTime) zum Vergleichen
 *  \return Ist das Datums-/Zeitobjekt gr�sser als das zu vergleichende
 *          Datum/Zeit-Objekt?
 *  \retval true -> Das Datums-/Zeitobjekt ist gr�sser (j�nger) als das zu
 *                  vergleichende
 *  \retval false -> Das Datums-/Zeitobjekt ist kleiner (�lter) oder gleich
 *                   wie das zu vergleichende
 ****************************************************************************/
bool  CDateTime::operator > (const CDateTime& Time)
{
  bool  bGreater = false;

  if (m_Year > Time.m_Year) {
    bGreater = true;
  } else if (m_Year == Time.m_Year) {
    if (m_Month > Time.m_Month) {
      bGreater = true;
    } else if (m_Month == Time.m_Month) {
      if (m_Day > Time.m_Day) {
        bGreater = true;
      } else if (m_Day == Time.m_Day) {
        if (m_Hour > Time.m_Hour) {
          bGreater = true;
        } else if (m_Hour == Time.m_Hour) {
          if (m_Min > Time.m_Min) {
            bGreater = true;
          } else if (m_Min == Time.m_Min) {
            bGreater = m_Sec > Time.m_Sec;
          }
        }
      }
    }
  }
  return bGreater;
}

//****************************************************************************
/*! \brief  Kleiner-als-Operator
 *
 *  Die Funktion gibt true zur�ck, wenn das Datums-/Zeitobjekt kleiner ist als
 *  das zu vergleichende Datums-/Zeitobjekt. Kleiner heisst in diesem Fall
 *  Datum/Zeit des Objekts sind �lter.
 *  j�nger
 *  \param  Time Datum/Zeit-Objekt (CDateTime) zum Vergleichen
 *  \return Ist das Datums-/Zeitobjekt gr�sser als das zu vergleichende
 *          Datum/Zeit-Objekt?
 *  \retval true -> Das Datums-/Zeitobjekt ist kleiner (�lter) als das zu
 *                  vergleichende
 *  \retval false -> Das Datums-/Zeitobjekt ist gr�sser (j�nger) oder gleich
 *                   wie das zu vergleichende
 ****************************************************************************/
bool  CDateTime::operator < (const CDateTime& Time)
{
  bool   bSmaller = false;

  if (m_Year < Time.m_Year) {
    bSmaller = true;
  } else if (m_Year == Time.m_Year) {
    if (m_Month < Time.m_Month) {
      bSmaller = true;
    } else if (m_Month == Time.m_Month) {
      if (m_Day < Time.m_Day) {
        bSmaller = true;
      } else if (m_Day == Time.m_Day) {
        if (m_Hour < Time.m_Hour) {
          bSmaller = true;
        } else if (m_Hour == Time.m_Hour) {
          if (m_Min < Time.m_Min) {
            bSmaller = true;
          } else if (m_Min == Time.m_Min) {
            bSmaller = m_Sec < Time.m_Sec;
          }
        }
      }
    }
  }
  return bSmaller;
}

//****************************************************************************
/*! \brief  Gleich-Operator
 *  \param  Time Datum/Zeit-Objekt (CDateTime) zum Vergleichen
 *  \return Ist das Datums-/Zeitobjekt gleich wie das zu vergleichende
 *          Datum/Zeit-Objekt?
 *  \retval true -> Das Datums-/Zeitobjekt ist gleich
 *  \retval false -> Das Datums-/Zeitobjekt ist ungleich
 ****************************************************************************/
bool  CDateTime::operator == (const CDateTime& Time)
{
  if (m_Year == Time.m_Year &&
      m_Month == Time.m_Month &&
      m_Day == Time.m_Day &&
      m_Hour == Time.m_Hour &&
      m_Min == Time.m_Min &&
      m_Sec == Time.m_Sec) {
    return true;
  }
  return false;
}

//****************************************************************************
/*! \brief  Gr�sser-als-Operator
 *
 *  Die Funktion gibt true zur�ck, wenn das Datums-/Zeitobjekt gr�sser oder
 *  gleich wie das zu vergleichende Datums-/Zeitobjekt ist.
 *  \param  Time Datum/Zeit-Objekt (CDateTime) zum Vergleichen
 *  \return Ist das Datums-/Zeitobjekt gr�sser oder gleich wie das zu
 *          vergleichende Datum/Zeit-Objekt?
 *  \retval true -> Das Datums-/Zeitobjekt ist gr�sser (j�nger) oder gleich
 *                  wie das zu vergleichende
 *  \retval false -> Das Datums-/Zeitobjekt ist kleiner (�lter) als das zu
 *                   vergleichende
 ****************************************************************************/
bool  CDateTime::operator >= (const CDateTime& Time)
{
  if (*this > Time ||
      *this == Time) {
    return true;
  }
  return false;
}

//****************************************************************************
/*! \brief  Kleiner-Gleich-Operator
 *
 *  Die Funktion gibt true zur�ck, wenn das Datums-/Zeitobjekt kleiner oder
 *  gleich wie das zu vergleichende Datums-/Zeitobjekt ist.
 *  j�nger
 *  \param Time Datum/Zeit-Objekt (CDateTime) zum Vergleichen
 *  \return Ist das Datums-/Zeitobjekt gr�sser als das zu vergleichende
 *          Datum/Zeit-Objekt?
 *  \retval true -> Das Datums-/Zeitobjekt ist kleiner (�lter) als das zu
 *                  vergleichende
 *  \retval false -> Das Datums-/Zeitobjekt ist gr�sser (j�nger)  oder gleich
 *                   als das zu vergleichende
 ****************************************************************************/
bool  CDateTime::operator <= (const CDateTime& Time)
{
  if (*this < Time ||
      *this == Time) {
    return true;
  }
  return false;
}

//****************************************************************************
/*! \brief  Ungleich-Operator
 *  \param Time Datum/Zeit-Objekt (CDateTime) zum Vergleichen
 *  \return Ist das Datums-/Zeitobjekt ungleich zum zu vergleichendem
 *          Datum/Zeit-Objekt?
 *  \retval true -> Das Datums-/Zeitobjekt ist ungleich
 *  \retval false -> Das Datums-/Zeitobjekt ist gleich
 ****************************************************************************/
bool  CDateTime::operator != (const CDateTime& Time)
{
  if (*this < Time ||
      *this > Time) {
    return true;
  }
  return false;
}

//****************************************************************************
/*! \brief  Aktuelles Datum-/Zeit-Objekt zur�ckgeben
 *  \return Datums-/Zeitobjekt mit den aktuellen Werten
 ****************************************************************************/
CDateTime CDateTime::GetAktTime ()
{
  struct tm timeinfo;

  GetLocalTime(time(NULL), &timeinfo);

  m_Day   = timeinfo.tm_mday;
  m_Month = timeinfo.tm_mon + 1;
  m_Year  = timeinfo.tm_year + 1900;
  m_Sec   = timeinfo.tm_sec;
  m_Min   = timeinfo.tm_min;
  m_Hour  = timeinfo.tm_hour;
  Validate();
	return *this;
}

//****************************************************************************
/*! \brief  Anzahl Tage des Monats unter Ber�cksichtigung des Schaltjahrs zur�ckgeben
 *  \return Anzahl Tage des Monats
 ****************************************************************************/
int CDateTime::GetDayMonth()
{
  if(m_Month == 2 && m_bLeapYear) return DATETIME_DAYPERMONTH[m_Month - 1] + 1;
  return DATETIME_DAYPERMONTH[m_Month - 1];
}

//****************************************************************************
/*! \brief  Datum-/Zeit als String zur�ckgeben
 *  \param  Format  Format f�r den zur�ckzugebenden String
 *                  - d Platzhalter f�r den Tag
 *                  - m Platzhalter f�r den Monat
 *                  - y Platzhalter f�r das Jahr
 *                  - H Platzhalter f�r die Stunde
 *                  - M Platzhalter f�r die Minute
 *                  - S Platzhalter f�r die Sekunde
 *  \return Datum-/Zeit als String
 ****************************************************************************/
string CDateTime::GetString(const string Format)
{
  char    AktChar;
  char    LastChar;
  int     iChar;
  int     Anzahl;
  string  OutStr = "";
  char    TmpStr[255 + 1];
  
  //-------------------------------------------------------------------------
  // Init
  //-------------------------------------------------------------------------
  LastChar = '\0';
  AktChar  = '\0';
  iChar    = 0;       
  Anzahl   = 0;
  if (Format.length() <= 0) return OutStr;
  //-------------------------------------------------------------------------
  // Formatstring interpretieren und konvertieren
  //-------------------------------------------------------------------------
  do {
    if (iChar < (int) Format.length()) AktChar = Format[iChar++];
                                    else AktChar = '\0';
    if (AktChar != LastChar) {
      switch (LastChar) {
        case '\0':  break;
#if _SECURE_ATL
        case 'd':   sprintf_s((char*) TmpStr, 255 + 1, "%0*d", Anzahl, m_Day);
                    OutStr += (char*) TmpStr;
                    break;
        case 'm':   sprintf_s((char*) TmpStr, 255 + 1, "%0*d", Anzahl, m_Month);
                    OutStr += (char*) TmpStr;
                    break;
        case 'y':   sprintf_s((char*) TmpStr, 255 + 1, "%0*d", Anzahl, m_Year);
                    OutStr += (char*) TmpStr;
                    break;
        case 'H':   sprintf_s((char*) TmpStr, 255 + 1, "%0*d", Anzahl, m_Hour);
                    OutStr += (char*) TmpStr;
                    break;
        case 'M':   sprintf_s((char*) TmpStr, 255 + 1, "%0*d", Anzahl, m_Min);
                    OutStr += (char*) TmpStr;
                    break;
        case 'S':   sprintf_s((char*) TmpStr, 255 + 1, "%0*d", Anzahl, m_Sec);
                    OutStr += (char*) TmpStr;
                    break;
#else
        case 'd':   sprintf((char*) TmpStr, "%0*d", Anzahl, m_Day);
                    OutStr += (char*) TmpStr;
                    break;
        case 'm':   sprintf((char*) TmpStr, "%0*d", Anzahl, m_Month);
                    OutStr += (char*) TmpStr;
                    break;
        case 'y':   sprintf((char*) TmpStr, "%0*d", Anzahl, m_Year);
                    OutStr += (char*) TmpStr;
                    break;
        case 'H':   sprintf((char*) TmpStr, "%0*d", Anzahl, m_Hour);
                    OutStr += (char*) TmpStr;
                    break;
        case 'M':   sprintf((char*) TmpStr, "%0*d", Anzahl, m_Min);
                    OutStr += (char*) TmpStr;
                    break;
        case 'S':   sprintf((char*) TmpStr, "%0*d", Anzahl, m_Sec);
                    OutStr += (char*) TmpStr;
                    break;
#endif
        default:    OutStr += LastChar; break;
      }
      Anzahl = 1;
      LastChar = AktChar;
    } else Anzahl++;
  } while (AktChar != '\0');
  //-------------------------------------------------------------------------
  // Ende
  //-------------------------------------------------------------------------
  return OutStr;
}

//****************************************************************************
/*! \brief  Zeit im MS-Format zur�ckgeben
 *  \return Zeit im MS-Format
 ****************************************************************************/
time_t CDateTime::GetMSTime ()
{
  struct tm timeinfo;

  GetLocalTime(time(NULL), &timeinfo);

  timeinfo.tm_mday = m_Day;
  timeinfo.tm_mon = m_Month - 1;
  timeinfo.tm_year = m_Year - 1900;
  timeinfo.tm_sec = m_Sec;
  timeinfo.tm_min = m_Min;
  timeinfo.tm_hour = m_Hour;
  return MakeTime(&timeinfo);
}

//****************************************************************************
/*! \brief  Kontrolle, ob die Werte f�r das Jahr, den Monat, den Tag, die
 *          Stunde, die die Minute und Sekunde g�ltig sind.
 *  \return Sind die Werte g�ltig?
 *  \retval true Ja
 *  \retval false Nein
 ****************************************************************************/
bool  CDateTime::IsValid ()
{
  bool             bOK;

  //-------------------------------------------------------------------------
  // Init
  //-------------------------------------------------------------------------
  bOK  = true;
  //-------------------------------------------------------------------------
  // Tests
  //-------------------------------------------------------------------------
  if (bOK && m_Year < 0)   bOK = false;
  if (bOK && m_Month < 1)  bOK = false;
  if (bOK && m_Month > 12) bOK = false;
  if (bOK && m_Day   < 1)  bOK = false;
  if (bOK) {
    if (m_Month == 2) {
      if (m_bLeapYear && m_Day > DATETIME_DAYPERMONTH[m_Month] + 1)   bOK = false;
      else if (!m_bLeapYear && m_Day > DATETIME_DAYPERMONTH[m_Month]) bOK = false;
    } else if (m_Day > DATETIME_DAYPERMONTH[m_Month]) bOK = false;
  }
  if (bOK && m_Hour < 0)  bOK = false;
  if (bOK && m_Hour > 23) bOK = false;
  if (bOK && m_Min < 0)  bOK = false;
  if (bOK && m_Min > 59) bOK = false;
  if (bOK && m_Sec < 0)  bOK = false;
  if (bOK && m_Sec > 59) bOK = false;
  //-------------------------------------------------------------------------
  // Fkt. Ende
  //-------------------------------------------------------------------------
  return bOK;
}

//****************************************************************************
/*! \brief  Kontrolliert, ob alle Werte (Jahr, Monat, Tag, Stunde, Minute und
 *          Sekunde) 0 sind.
 *  \return Sind die Werte alle 0?
 *  \retval true Ja
 *  \retval false Nein
 ****************************************************************************/
bool  CDateTime::IsNull()
{
  if (m_Year != 0) return false;
  if (m_Month != 0) return false;
  if (m_Day != 0) return false;
  if (m_Hour != 0) return false;
  if (m_Min != 0) return false;
  if (m_Sec != 0) return false;
  //-------------------------------------------------------------------------
  // Fkt. Ende
  //-------------------------------------------------------------------------
  return true;
}

//****************************************************************************
/*! \brief  Datum-/Zeitstruktur abf�llen mit einem Datum/Zeit als String.
 *  \param  TimeStr Datum/Zeit als String
 *  \param  Format  Format den Datum/Zeit-Strings
 *  \return Sind die Werte alle 0?
 *  \retval true  Datum-Zeit-Objekt konnte erfolgreich gesetzt werden
 *  \retval false Datum/Zeit-String enth�lt ung�ltige Daten
 ****************************************************************************/
bool  CDateTime::SetFromString (const string TimeStr, const string Format)
{
  char    aktChar;
  int   iAktValue;
  int   Idx[6];   // Tag / Monat / Jahr / Stunde / Minute / Sekunde
  int   Len[6];   // Tag / Monat / Jahr / Stunde / Minute / Sekunde
  int   Start[6]; // Tag / Monat / Jahr / Stunde / Minute / Sekunde
  int   iValue;
  int   iAktIdx;
  int   iStr;
  int   iPos;
  int   iMin;
  int   iLen;
  string  s;

  //-------------------------------------------------------------------------
  // Init
  //-------------------------------------------------------------------------
  for (iValue = 0; iValue < 6; iValue++) {
    Idx[iValue] = -1;
    Start[iValue] = -1;
    Len[iValue] = 0;
  }
  //-------------------------------------------------------------------------
  // Positionen der Werte suchen
  //-------------------------------------------------------------------------
  for (iAktValue = 0; iAktValue < 6; iAktValue++) {
    switch (iAktValue) {
      case 0:   aktChar = 'd'; break;
      case 1:   aktChar = 'm'; break;
      case 2:   aktChar = 'y'; break;
      case 3:   aktChar = 'H'; break;
      case 4:   aktChar = 'M'; break;
      default:  aktChar = 'S'; break;
    }
    for (iValue = 0; Start[iAktValue] < 0 && iValue < (int) Format.length(); iValue++) {
      if (Format[iValue] == aktChar) {
        Start[iAktValue] = iValue;
        while (Format[iValue++] == aktChar) {
          Len[iAktValue]++;
        }
      }
    }
  }
  iPos = 0;
  for (iAktValue = 0; iAktValue < 6; iAktValue++) {
    iMin = -1;
    for (iValue = 0; iValue < 6; iValue++) {
      if (Idx[iValue] < 0 && Start[iValue] >= 0) {
        if (iMin < 0 || Start[iValue] < Start[iMin]) iMin = iValue;
      }
    }
    if (iMin >= 0) {
      Idx[iMin] = iPos;
      iPos++;
    }
  }
  //-------------------------------------------------------------------------
  // Werte setzen
  //-------------------------------------------------------------------------
  for (iAktValue = 0; iAktValue < 6; iAktValue++) {
    if (Idx[iAktValue] >= 0) {
      iStr = 0;
      for (iValue = 0; iValue < Idx[iAktValue]; iValue++) {
        for (iAktIdx = 0; iAktIdx < 6; iAktIdx++) {
          if (Idx[iAktIdx] == iValue) break;
        }
        if (iValue >= Idx[iAktValue]) iAktIdx = -1;
        if (iAktIdx >= 0 && Len[iAktIdx] > 0) {
          for (; iStr < (int) TimeStr.length() && !isdigit(TimeStr[iStr]); iStr++);
          for (iLen = 0; iStr < (int) TimeStr.length() && iLen < Len[iAktIdx] && isdigit(TimeStr[iStr]); iStr++, iLen++);
        }
      }
      for (; iStr < (int) TimeStr.length() && !isdigit(TimeStr[iStr]); iStr++);
#ifdef WIN32
      s = TimeStr.substr(iStr, __min((long) TimeStr.length() - iStr, (long) Len[iAktValue]));
#else
      s = TimeStr.substr(iStr, min((long)TimeStr.length() - iStr, (long)Len[iAktValue]));
#endif
      switch (iAktValue) {
        case 0  : m_Day   = atoi(s.c_str()); break;
        case 1  : m_Month = atoi(s.c_str()); break;
        case 2  : m_Year  = atoi(s.c_str());
                  if (s.length() > 0 && m_Year < 100) m_Year += 2000;
                  break;
        case 3  : m_Hour  = atoi(s.c_str()); break;
        case 4  : m_Min   = atoi(s.c_str()); break;
        case 5  : m_Sec   = atoi(s.c_str()); break;
      }
    }
  }
  //-------------------------------------------------------------------------
  // Test
  //-------------------------------------------------------------------------
  Validate (); 
  //-------------------------------------------------------------------------
  // Fkt. Ende
  //-------------------------------------------------------------------------
  return true;
}

//****************************************************************************
/*! \brief  Werte aktualisieren, validieren
 *  \return Konnten die Werte aktualisiert werden?
 *  \retval true OK
 *  \retval false Falsche Werte, Aktualisierung misslungen
 ****************************************************************************/
bool  CDateTime::Validate ()
{
  bool  bOK;
  int   AnzTag;
  int   Month;

  //-------------------------------------------------------------------------
  // Init
  //-------------------------------------------------------------------------
  bOK = true;
  //-------------------------------------------------------------------------
  // Variablen setzen
  //-------------------------------------------------------------------------
  bOK = bOK && m_Sec >= 0;
  bOK = bOK && m_Min >= 0;
  bOK = bOK && m_Hour >= 0;
  bOK = bOK && m_Day >= 1;
  bOK = bOK && m_Month >= 1;
  if (!bOK) return false;
  do {
    bOK = true;
    //-------------------------------------------------------------------------
    // Schaltjahr
    //-------------------------------------------------------------------------
    if (m_Year < 100) m_Year += 1900;
    m_bLeapYear = (m_Year % 4 == 0 && m_Year % 100 != 0) || m_Year % 400 == 0;
    //-------------------------------------------------------------------------
    // Sekunde
    //-------------------------------------------------------------------------
    if (bOK) {
      if (m_Sec > 59) {
        bOK = false;
        if (m_Sec == 60) {
          m_Min++;
          m_Sec = 0;
        } else {
          m_Min += m_Sec % 60;
          m_Sec -= (m_Sec % 60) * 60;
        }
      }
    }
    //-------------------------------------------------------------------------
    // Minute
    //-------------------------------------------------------------------------
    if (bOK) {
      if (m_Min > 59) {
        bOK = false;
        if (m_Min == 60) {
          m_Hour++;
          m_Min = 0;
        } else {
          m_Hour += m_Min % 60;
          m_Min -= (m_Min % 60) * 60;
        }
      }
    }
    //-------------------------------------------------------------------------
    // Stunde
    //-------------------------------------------------------------------------
    if (bOK) {
      if (m_Hour > 23) {
        bOK = false;
        if (m_Hour == 24) {
          m_Day++;
          m_Hour = 0;
        } else {
          m_Day += m_Hour % 24;
          m_Hour -= (m_Hour % 24) * 24;
        }
      }
    }
    //-------------------------------------------------------------------------
    // Tag
    //-------------------------------------------------------------------------
    if (bOK) {
      Month = m_Month % 12;
      if (Month == 0) Month = 12;
      if (Month == 2 && m_bLeapYear) AnzTag = DATETIME_DAYPERMONTH[Month] + 1;
                                else AnzTag = DATETIME_DAYPERMONTH[Month];
      while (bOK && m_Day > AnzTag) {
        m_Day -= AnzTag;
        m_Month++;
        if (m_Month > 12) {
          bOK = false;
          m_Year++;
          m_Month -= 12;
        } else {
          Month = m_Month % 12;
          if (Month == 0) Month = 12;
          if (Month == 2 && m_bLeapYear) AnzTag = DATETIME_DAYPERMONTH[Month] + 1;
                                    else AnzTag = DATETIME_DAYPERMONTH[Month];
        }
      }
    }
    //-------------------------------------------------------------------------
    // Monat
    //-------------------------------------------------------------------------
    if (bOK) {
      if (m_Month > 12) {
        bOK = false;
        m_Year += m_Month % 12;
        m_Month -= (m_Month % 12) * 12;
      }
    }
  } while (!bOK);
  //-------------------------------------------------------------------------
  // Tag im Jahr bestimmen
  //-------------------------------------------------------------------------
  m_DayOfYear = m_Day;
  for (int iMonat = 1; iMonat < m_Month; iMonat++) m_DayOfYear += DATETIME_DAYPERMONTH[iMonat];
  if (m_bLeapYear && m_Month >= 3) m_DayOfYear++;
  //-------------------------------------------------------------------------
  // Tag und Monat bestimmen
  //-------------------------------------------------------------------------
  m_Day   = m_DayOfYear;
  m_Month = 1; 
  while (m_Month <= 12) { 
    if (m_Month == 2 && m_bLeapYear) AnzTag = DATETIME_DAYPERMONTH[m_Month] + 1;
                                else AnzTag = DATETIME_DAYPERMONTH[m_Month];
    if (m_Day <= AnzTag) break;
    m_Day -= AnzTag;
    m_Month++; 
  }
  //-------------------------------------------------------------------------
  // Wochentag bestimmen (1.1.0000 -> Montag)
  //-------------------------------------------------------------------------
  long AnzSchaltJahr = int ((m_Year - 1) / 4) - (m_Year - 1) / 100 + (m_Year - 1) / 400;
  long AnzTage = 365L * (long) m_Year + AnzSchaltJahr + ((long) m_DayOfYear - 1L);
  m_DayOfWeek = (int) (AnzTage % 7);
  //-------------------------------------------------------------------------
  // Fkt. Ende
  //-------------------------------------------------------------------------
  return true;
}

//****************************************************************************
/*! \brief  Datum-/Zeit-Objekt setzen
 *  \param  Jahr   Wert f�r das Jahr
 *  \param  Monat  Wert f�r den Monat
 *  \param  Tag    Wert f�r den Tag
 *  \param  Std    Wert f�r die Stunden
 *  \param  Min    Wert f�r die Minuten
 *  \param  Sec    Wert f�r die Sekunden
 *  \return Konnten die Werte korrekt gesetzt werden?
 *  \retval true  OK
 *  \retval false Falsche Werte, Fehler
 ****************************************************************************/
bool CDateTime::Set(const int Jahr, const int Monat, const int Tag, const int Std, const int Min, const int Sec)
{
  m_Year  = Jahr;
  m_Month = Monat;
  m_Day   = Tag;
  m_Hour  = Std;
  m_Min   = Min;
  m_Sec   = Sec;  
  return Validate (); 
}

//****************************************************************************
/*! \brief  Datum-/Zeit-Objekt setzen
 *  \param  Time  Wert f�r das Datum-/Zeit-Objekt in Microsoft-Form (Anzahl
 *                Sekunden seit 1.1.1970)
 *  \return Konnten die Werte korrekt gesetzt werden?
 *  \retval true OK
 *  \retval false Falsche Werte, Fehler
 ****************************************************************************/
bool CDateTime::Set(time_t iTime)
{
  struct tm timeinfo;

  GetLocalTime(iTime, &timeinfo);

  m_Day   = timeinfo.tm_mday;
  m_Month = timeinfo.tm_mon + 1;
  m_Year  = timeinfo.tm_year + 1900;
  m_Sec   = timeinfo.tm_sec;
  m_Min   = timeinfo.tm_min;
  m_Hour  = timeinfo.tm_hour;
  return Validate();
}

//****************************************************************************
/*! \brief  Datum-/Zeit-String aufbereiten, in ein bestimmtes Format bringen
 *  \param  sDate   Aufzubereitender Datum-/Zeit-String
 *  \param  sFormat Gew�nschtes Format f�r den Datum/Zeit-Strings
 *  \return Formatierter Datum-/Zeit-String
 ****************************************************************************/
string CDateTime::Prepare(const string sDate, const string sFormat)
{
  string    theString = sDate;
  char      s[255 + 1];
  int       i;
  int       i1;
  int       i2;
  CDateTime oDate;

#if _SECURE_ATL
  strcpy_s((char*) s, 255 + 1, (char*) theString.c_str());
#else
  strcpy((char*) s, (char*) theString.c_str());
#endif
  for (i = 0; i < (int) strlen((char*) s); i++) {
    if (s[i] >= 'A' && s[i] <= 'Z') {
      s[i] = s[i] - 'A' + 'a';
    }
  }
  theString = (char*) s;
  i = (int) theString.find("heute");
  if (i == 0) {
    i1 = (int) theString.find("+");
    i2 = (int) theString.find("-");
    if (i1 >= 0 && i2 >= 0) {
#ifdef WIN32
      i = __min(i1, i2);
#else
      i = min(i1, i2);
#endif
    } else if (i1 >= 0) {
      i = i1;
    } else {
      i = i2;
    }
    if (i >= 0) {
      if (theString.at(i) == '+') {
        i = atoi(theString.c_str() + i + 1);
      } else {
        i = -atoi(theString.c_str() + i + 1);
      }
      oDate.SetDay(oDate.GetDay() + i, true);
    }
    theString = oDate.GetString(sFormat);
  }
  return theString;
}

//****************************************************************************
/*! \brief  Struktur mit den Datums-/Zeitinformationen zurueckgeben
 *          Korrigiert Fehler in Metrowerks Codewarrior!
 *  \param  time      Zeit in s seit 1.1.1970
 *  \param  pTimeInfo Datum-/Zeitstruktur
 ****************************************************************************/
bool CDateTime::GetLocalTime(time_t time, struct tm* pTimeInfo)
{
#if defined WIN32

#if _SECURE_ATL
  localtime_s(pTimeInfo, &time);
#else
  struct tm*  pTI;
  pTI = localtime(&time);
  
  pTimeInfo->tm_sec   = pTI->tm_sec;
  pTimeInfo->tm_min   = pTI->tm_min;
  pTimeInfo->tm_hour  = pTI->tm_hour;
  pTimeInfo->tm_mday  = pTI->tm_mday;
  pTimeInfo->tm_mon   = pTI->tm_mon;
  pTimeInfo->tm_year  = pTI->tm_year;
  pTimeInfo->tm_wday  = pTI->tm_wday;
  pTimeInfo->tm_yday  = pTI->tm_yday;
  pTimeInfo->tm_isdst = pTI->tm_isdst;
#endif

#else

#ifdef __MACH__

#if defined __MWERKS__ && __MWERKS__ < 0x3204
  CFTimeZoneRef   timeZone = CFTimeZoneCopySystem();
  CFAbsoluteTime  absoluteTime = time - kCFAbsoluteTimeIntervalSince1970;
  CFGregorianDate date = CFAbsoluteTimeGetGregorianDate(absoluteTime, timeZone);

  pTimeInfo->tm_sec   = (int) date.second;
  pTimeInfo->tm_min   = date.minute;
  pTimeInfo->tm_hour  = date.hour;
  pTimeInfo->tm_mday  = date.day;
  pTimeInfo->tm_mon   = date.month - 1;
  pTimeInfo->tm_year  = date.year - 1900;
  pTimeInfo->tm_wday  = CFAbsoluteTimeGetDayOfWeek(absoluteTime, timeZone) % 7;
  pTimeInfo->tm_yday  = CFAbsoluteTimeGetDayOfYear(absoluteTime, timeZone) - 1;
  pTimeInfo->tm_isdst = CFTimeZoneIsDaylightSavingTime(timeZone, absoluteTime);
  
  CFRelease(timeZone);
#else  
  struct tm*  pTI;
  pTI = localtime(&time);
  
  pTimeInfo->tm_sec   = pTI->tm_sec;
  pTimeInfo->tm_min   = pTI->tm_min;
  pTimeInfo->tm_hour  = pTI->tm_hour;
  pTimeInfo->tm_mday  = pTI->tm_mday;
  pTimeInfo->tm_mon   = pTI->tm_mon;
  pTimeInfo->tm_year  = pTI->tm_year;
  pTimeInfo->tm_wday  = pTI->tm_wday;
  pTimeInfo->tm_yday  = pTI->tm_yday;
  pTimeInfo->tm_isdst = pTI->tm_isdst;
#endif

#else

  DateTimeRec   oDateTime1904;
  DateTimeRec   oDateTime1970;
  DateTimeRec   oDateTime;
  unsigned long lTime1904;
  unsigned long lTime1970;
  unsigned long lTimeDiff;
  unsigned long lTime;
  
  oDateTime1904.year = 1904;
  oDateTime1904.month = 1;
  oDateTime1904.day = 1;
  oDateTime1904.hour = 0;
  oDateTime1904.minute = 0;
  oDateTime1904.second = 0;
  oDateTime1970.year = 1970;
  oDateTime1970.month = 1;
  oDateTime1970.day = 1;
  oDateTime1970.hour = 0;
  oDateTime1970.minute = 0;
  oDateTime1970.second = 0;
  
  DateToSeconds(&oDateTime1904, &lTime1904);
  DateToSeconds(&oDateTime1970, &lTime1970);
  lTimeDiff = lTime1970 - lTime1904;
  
  lTime = time + lTimeDiff;
  
  SecondsToDate(lTime, &oDateTime);
  
  pTimeInfo->tm_sec   = oDateTime.second;
  pTimeInfo->tm_min   = oDateTime.minute;
  pTimeInfo->tm_hour  = oDateTime.hour;
  pTimeInfo->tm_mday  = oDateTime.day;
  pTimeInfo->tm_mon   = oDateTime.month - 1;
  pTimeInfo->tm_year  = oDateTime.year - 1900;
  pTimeInfo->tm_wday  = oDateTime.dayOfWeek;
  pTimeInfo->tm_yday  = 0;
  pTimeInfo->tm_isdst = 0;
  
#endif

#endif  
  return true;
}

//****************************************************************************
/*! \brief  Zeit in s seit 1.1.1970 aus einer Zeitstruktur generieren
 *  \return Zeit in s seit 1.1.1970
 ****************************************************************************/
time_t CDateTime::MakeTime(struct tm* tm)
{
#if defined WIN32

  time_t lTime;
  lTime = mktime(tm);
  
#else

#ifdef __MACH__
#if defined __MWERKS__ && __MWERKS__ < 0x3204
  time_t lTime;

  CFTimeZoneRef   timeZone = ::CFTimeZoneCopySystem();
  CFGregorianDate date;
  CFAbsoluteTime  absTime;
  
  date.second = tm->tm_sec;
  date.minute = tm->tm_min;
  date.hour = tm->tm_hour;
  date.day = tm->tm_mday;
  date.month = tm->tm_mon + 1;
  date.year = tm->tm_year + 1900;
    
  absTime = ::CFGregorianDateGetAbsoluteTime(date, timeZone);
  lTime = (time_t) (absTime + kCFAbsoluteTimeIntervalSince1970);
  ::CFRelease(timeZone);
#else
  time_t lTime;
  lTime = mktime(tm);
#endif
  
#else

  DateTimeRec   oDateTime1904;
  DateTimeRec   oDateTime1970;
  DateTimeRec   oDateTime;
  unsigned long lTime1904;
  unsigned long lTime1970;
  unsigned long lTimeDiff;
  unsigned long lTime;
  
  oDateTime1904.year = 1904;
  oDateTime1904.month = 1;
  oDateTime1904.day = 1;
  oDateTime1904.hour = 0;
  oDateTime1904.minute = 0;
  oDateTime1904.second = 0;
  oDateTime1970.year = 1970;
  oDateTime1970.month = 1;
  oDateTime1970.day = 1;
  oDateTime1970.hour = 0;
  oDateTime1970.minute = 0;
  oDateTime1970.second = 0;
  oDateTime.year = tm->tm_year + 1900;
  oDateTime.month = tm->tm_mon + 1;
  oDateTime.day = tm->tm_mday;
  oDateTime.hour = tm->tm_hour;
  oDateTime.minute = tm->tm_min;
  oDateTime.second = tm->tm_sec;
  
  DateToSeconds(&oDateTime1904, &lTime1904);
  DateToSeconds(&oDateTime1970, &lTime1970);
  lTimeDiff = lTime1970 - lTime1904;
  
  DateToSeconds(&oDateTime, &lTime);
  lTime -= lTimeDiff;
  
#endif

#endif
  return (time_t) lTime;
}