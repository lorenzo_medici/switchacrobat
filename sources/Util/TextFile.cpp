//****************************************************************************
/*! \file
 *  \brief    Header file: Text file reading and writing
 *  \author   Hans Stössel
 *  \date     2007 - 2009
 *  \version  1.0.0 Erste Version
 *  \version  1.0.1 HS 20091006 -> Korrektur in createDirectory
 *  \bug      
 *  \warning  
 *  \todo     
 ****************************************************************************/
#ifdef INDESIGN_PROJECT
  #include "VCPlugInHeaders.h"

  #include "IOpenFileDialog.h"
  #include "FileTypeRegistry.h"

  #include "SDKFileHelper.h"
  #include "SDKUtilities.h"
  #include "CoreFileUtils.h"

  #include "Errorcodes.h"
  #include "SysFileList.h"

  #include "IndesignUtilsVersion.h"
#endif

#if defined QXP6 || defined QXP7 || defined QXP8 || defined QXP9 || defined QXP10
  #include "QXTVersion.h"
#endif

#ifdef WIN32
  #include  <shlobj.h>
#endif

#include "DateTime.h"
#include "LogFile.h"
#include "TextFile.h"
#include "QXT4StringUtil.h"
#include <sys/stat.h>

#ifdef WIN32
  #include "Errno.h"
  #include "Direct.h"
#else
  #include "MkDir.h"
#endif

//****************************************************************************
/*! \brief    Textmarker-Vektor sortieren nach uidRefFrame und Textposition
 *  \param    oTextMarker1  1. Textmarker
 *  \param    oTextMarker2  2. Textmarker
 *  \return   Flag: Ist die UIDRef kleiner und die Textposition gr�sser?
 *  \retval   true -> UIDRef kleiner und die Textposition gr�sser
 *  \retval   false -> UIDRef gr�sser/gleich oder die Textposition kleiner
 ****************************************************************************/
bool sortTextSAndR(stSAndRText oTextSAndR1, stSAndRText oTextSAndR2)
{
  if (oTextSAndR1.iPosStart < oTextSAndR2.iPosStart) {
    return true;
  }
  return false;
}

//****************************************************************************
/*! \brief  Constructor
 *  \param  sFilename       Filename
 ****************************************************************************/
CTextFile::CTextFile(string sFilename, int iWriteLogFile /*= 0*/)
{
  m_pFile = NULL;
  init(sFilename, iWriteLogFile);
}

//****************************************************************************
/*! \brief  Destructor
 ****************************************************************************/
CTextFile::~CTextFile() 
{
  close();
}

//****************************************************************************
/*! \brief  Initializing
 *  \param  sFilename       Filename
 ****************************************************************************/
void CTextFile::init(string sFilename, int iWriteLogFile /*= 0*/)
{
#ifndef WIN32
  CQXT4StringUtil oStrUtil;

  oStrUtil.convertNetworkPathToHDPath(sFilename);
	oStrUtil.convertWinPathToMacPath(sFilename);
  oStrUtil.convertMacPathToUNIXPath(sFilename);
#endif
  close();
  m_sFilename = sFilename;
  m_iWriteLogFile = iWriteLogFile;
}

//****************************************************************************
/*! \brief  Closes files
 ****************************************************************************/
void CTextFile::close() 
{
  if (m_pFile != NULL) {
    fclose(m_pFile);
    m_pFile = NULL;
  }
  m_iCurPos = 0;
}

//****************************************************************************
/*! \brief  Opens files for writing
 *  \return Error code
 ****************************************************************************/
int CTextFile::openForWriting() 
{
  close();
  if (createDirectory(m_sFilename) != ERR_NO) {
#ifdef DEBUG
    CLogfile::consoleOutFormat("ERROR CTextFile::openForWriting -> createDirectory: %s - Error %d / %s", m_sFilename.c_str(), errno, strerror(errno));
#endif
    return ERR_FILE_OPENFORWRITING;
  }
  m_pFile = fileOpen(m_sFilename.c_str(), "wb");
  if (m_pFile == NULL) {
#ifdef DEBUG
    CLogfile::consoleOutFormat("ERROR CTextFile::openForWriting -> fileOpen: %s - Error %d / %s", m_sFilename.c_str(), errno, strerror(errno));
#endif
    return ERR_FILE_OPENFORWRITING;
  }
  return ERR_NO;
}

//****************************************************************************
/*! \brief  Opens files for reading
 *  \return Error code
 ****************************************************************************/
int CTextFile::openForReading() 
{
  close();
  m_pFile = fileOpen(m_sFilename.c_str(), "rb");
  if (m_pFile == NULL) {
#ifdef DEBUG
    CLogfile::consoleOutFormat("ERROR CTextFile::openForReading -> fileOpen: %s - Error %d / %s", m_sFilename.c_str(), errno, strerror(errno));
#endif
    return ERR_FILE_OPENFORREADING;
  }  
  return ERR_NO;
}

//****************************************************************************
/*! \brief  Opens files for appending
 *  \return Error code
 ****************************************************************************/
int CTextFile::openForAppending() 
{
  close();
  m_pFile = fileOpen(m_sFilename.c_str(), "ab");
  if (m_pFile == NULL) {
#ifdef DEBUG
    CLogfile::consoleOutFormat("ERROR CTextFile::openForAppending -> fileOpen: %s - Error %d / %s", m_sFilename.c_str(), errno, strerror(errno));
#endif
    return ERR_FILE_OPENFORWRITING;
  }
  return ERR_NO;
}

//****************************************************************************
/*! \brief  Writes a line into a file
 *  \param  sFrm  Format string
 *  \param  ...   Variable argument list
 *  \return Error code
 ****************************************************************************/
int CTextFile::writeLine(char* sFrm, ...)
{
  char*     p = NULL;
  int       iLen = 0;
  va_list   ArgLst;
#ifndef WIN32
  Str255    s;
#endif
  string    sBuffer = "";
  
  if (m_pFile == NULL) {
    return ERR_FILE_NOTOPEN;
  }
  va_start(ArgLst, sFrm);
#ifdef WIN32  
  iLen = _vscprintf(sFrm, ArgLst);
#else
  iLen = vsnprintf((char*) s, 255, sFrm, ArgLst);
#endif
  va_end(ArgLst);
  if (iLen > 0) {
    p = new char[iLen + 1];
    if (p == NULL) {
      return -1;
    }
    va_start(ArgLst, sFrm);
    vsprintf(p, sFrm, ArgLst);
    va_end(ArgLst);
    sBuffer = p;
    delete [] p;
  }
#ifdef WIN32
  sBuffer += "\r\n";
#else
  sBuffer += "\r";
#endif
  return write((char*) sBuffer.c_str());
}

//****************************************************************************
/*! \brief  Writes into a file
 *  \param  sFrm  Format string
 *  \param  ...   Variable argument list
 *  \return Error code
 ****************************************************************************/
int CTextFile::write(char* sFrm, ...)
{
  char*     p = NULL;
  int       iLen = 0;
  va_list   ArgLst;
#ifndef WIN32
  Str255    s;
#endif
  string    sBuffer = "";
  
  if (m_pFile == NULL) {
    return ERR_FILE_NOTOPEN;
  }
  va_start(ArgLst, sFrm);
#ifdef WIN32  
  iLen = _vscprintf(sFrm, ArgLst);
#else
  iLen = vsnprintf((char*) s, 255, sFrm, ArgLst);
#endif
  va_end(ArgLst);
  if (iLen > 0) {
    p = new char[iLen + 1];
    if (p == NULL) {
      return -1;
    }
    va_start(ArgLst, sFrm);
    vsprintf(p, sFrm, ArgLst);
    va_end(ArgLst);
    sBuffer = p;
    delete [] p;
  }
  fwrite(sBuffer.c_str(), sizeof(char), sBuffer.length(), m_pFile);
  return ERR_NO;
}

//****************************************************************************
/*! \brief  Writes a binary stream into a file
 *  \param  pBuffer     Format string
 *  \param  iLenBuffer  Length of the buffer
 *  \return Error code
 ****************************************************************************/
int CTextFile::writeBinary(unsigned char* pBuffer, unsigned int iLenBuffer)
{
  fwrite(pBuffer, sizeof(unsigned char), iLenBuffer, m_pFile);
  return ERR_NO;
}

//****************************************************************************
/*! \brief  Read a binary stream from a file
 *  \param  pBuffer     Format string
 *  \param  iLenBuffer  Length of the buffer
 *  \return Lenght of readed buffer
 ****************************************************************************/
int CTextFile::readBinary(unsigned char* pBuffer, unsigned int iLenBuffer)
{
  return (int) fread(pBuffer, sizeof(unsigned char), iLenBuffer, m_pFile);
}

//****************************************************************************
/*! \brief  Writes a line into a file
 *  \param  sFrm  Format string
 *  \param  ...   Variable argument list
 *  \return Error code
 ****************************************************************************/
int CTextFile::writeLine(string s)
{
#ifdef WIN32
  s += "\r\n";
#else
  s += "\r";
#endif
  return write(s);
}

//****************************************************************************
/*! \brief  Writes into a file
 *  \param  sFrm  Format string
 *  \param  ...   Variable argument list
 *  \return Error code
 ****************************************************************************/
int CTextFile::write(string s)
{
  fwrite(s.c_str(), sizeof(char), s.length(), m_pFile);
  return ERR_NO;
}

//****************************************************************************
/*! \brief  Reads a line from a file
 *  \param  sLine       Readed line
 *  \param  sDelimiters Delimiter for a line
 *  \return Error code
 ****************************************************************************/
int CTextFile::readLine(string* sLine)
{
  int             iNoOfRead;
  int             iNoOfString;
  char            sBuffer[1024 + 1];
  string          sDelimiter = "";
  string          s = "";
  CQXT4StringUtil oStrUtil;
  vector<string>  vString;
  
  sBuffer[0] = '\0';
  *sLine = "";
  if (m_pFile == NULL) {
    return ERR_FILE_NOTOPEN;
  }
  iNoOfRead = (int) fread(sBuffer, sizeof(char), 1024, m_pFile);
  if (iNoOfRead > 0) {
    sBuffer[iNoOfRead] = '\0';
    for (int iDelimiter = 0; iDelimiter < 4; iDelimiter++) {
      s = sBuffer;
      switch (iDelimiter) {
        case 0:   sDelimiter = "\r\n"; break;
        case 1:   sDelimiter = "\n\r"; break;
        case 2:   sDelimiter = "\r"; break;
        case 3:   sDelimiter = "\n"; break;
        default:  sDelimiter = ""; break;
      }
      if (sDelimiter.length() > 0) {
        iNoOfString = oStrUtil.split(sBuffer, sDelimiter, vString);
        if (iNoOfString > 0) *sLine = vString[0];
        if (iNoOfString > 1) break;
      }
    }
    if (iNoOfString > 0) {
      m_iCurPos += (*sLine).length() + sDelimiter.length();
#ifdef WIN32
      if (m_iCurPos >= 0) fseek(m_pFile, (long) m_iCurPos, SEEK_SET);
#else
      if (m_iCurPos >= 0) fseek(m_pFile, m_iCurPos, SEEK_SET);
#endif
    }  
  } else {
    return ERR_FILE_EOF;
  }
  return ERR_NO;
}

//****************************************************************************
/*! \brief  Deletes a file
 *  \return Error code
 ****************************************************************************/
bool CTextFile::deleteFile()
{
	int iErr = ERR_NO;

  if (m_sFilename.length() > 0) {
    if ((m_iWriteLogFile & 0x1000) == 0x1000) {
      writeLogFile("Loesche File (CTextFile::deleteFile): %s", m_sFilename.c_str());
    }
    iErr = remove(m_sFilename.c_str());
  }
  return iErr == ERR_NO;
}

//****************************************************************************
/*! \brief  Verzeichnis erstellen.
 *  \param  sFile     Name der Datei oder des Verzeichnisses (wenn \ am
 *                    Schluss). Ist der Name ein Dateiname (d.h. Pfad und
 *                    Filename) wird der Pfad extrahiert.
 *  \return Fehlercode
 ****************************************************************************/
int CTextFile::createDirectory(string sFile)
{
  int             iErr = ERR_NO;
  int             iPos;
  string          sPath = "";
  CQXT4StringUtil oStrUtil;
#ifdef WIN32
  WIN32_FIND_DATA stFileInfo;
  HANDLE          hFile;
  string          sMakePath = "";
  char            cSeparator = PATH_DELIMITER_CHAR;
#else
  int             iPath;
  char            cSeparator = ':';
  string          sSeparator = "";
  sSeparator += cSeparator;
  vector<string>  vPath;
#endif

#ifndef WIN32
  oStrUtil.convertNetworkPathToHDPath(sFile);
	oStrUtil.convertWinPathToMacPath(sFile);
#endif
  while ((iPos = (int) sFile.find('/')) >= 0) {
    sFile.replace(iPos, 1, PATH_DELIMITER_WIN);
  }
  if (sFile.length() > 0) {
    if (sFile.at(sFile.length() - 1) != cSeparator) {
      // Ist ein Filename
      iPos = (int) sFile.rfind(cSeparator);
      if (iPos >= 0) {
        sPath = sFile.substr(0, iPos);
      } else {
        sPath = "";
      }
    } else {
      sPath = sFile;
    }
    if (sPath.length() > 0) {
#ifdef WIN32
      iPos = (int) sPath.find(':');
      if (iPos == 1) {
        // Windowspfad mit Laufwerk
        iPos += 2;
#ifdef INDESIGN_PROJECT_GT_CC2015
        while (sPath.at(iPos) == (char) PATH_DELIMITER_WIN_CHAR || sPath.at(iPos) == (char) PATH_DELIMITER_UNIX_CHAR) {
#else
        while (sPath.at(iPos) == (int) PATH_DELIMITER_WIN_CHAR || sPath.at(iPos) == (int) PATH_DELIMITER_UNIX_CHAR) {
#endif
          iPos++;
        }
      } else {
        iPos = (int) sPath.find("\\\\");
        if (iPos == 0) {  // Netzwerkpfad -> UNC-Pfad
          iPos = (int) sPath.find(PATH_DELIMITER_WIN_CHAR, 2);
          iPos++;
          iPos = (int) sPath.find(PATH_DELIMITER_WIN_CHAR, iPos); // Der erste Teil nach der Netzwerkadresse
                                                                  // (Servername oder IP) ist der Freigabename
                                                                  // -> Eigentlicher Pfad beginnt erst danach!
          iPos++;
        } else {
          iPos = 0;
        }
      }
      if (sPath.at(sPath.length() - 1) != cSeparator) sPath += cSeparator;
      while (iErr == ERR_NO && (iPos = (int) sPath.find(cSeparator, iPos)) >= 0) {
        sMakePath = sPath.substr(0, iPos);
        if (sMakePath.length() > 0) {
#ifdef INDESIGN_PROJECT
		      PMString swPath = oStrUtil.getPMString(sMakePath);
          hFile = ::FindFirstFile(swPath.GrabTString(), &stFileInfo);
#else
          hFile = ::FindFirstFile(sPath.c_str(), &stFileInfo);
#endif
          if (hFile == INVALID_HANDLE_VALUE) {  // Verzeichnis existiert nicht -> Erstellen
            if (_mkdir(sMakePath.c_str()) != 0) {
              if (errno != EEXIST) {
#ifdef DEBUG
                CLogfile::consoleOutFormat("ERROR CTextFile::createDirectory -> _mkdir: %s - Error %d / %s", m_sFilename.c_str(), errno, strerror(errno));
#endif
                iErr = ERR_FILE_MKDIR;
              }
            }
          } else {
            ::FindClose(hFile);
          }
        }
        iPos++;
      }
#else
      #define DEFAULT_MODE  S_IRWXU | S_IRWXG | S_IRWXO
      if (sPath.at(sPath.length() - 1) == cSeparator) sPath = oStrUtil.trimRight(sPath, cSeparator);
      oStrUtil.split(sPath, sSeparator, vPath);
      if (vPath.size() > 1) {
        iPath = 1;
        sPath = vPath[0] + cSeparator + vPath[1];
        do {
          int iResult = access(sPath.c_str(), F_OK);
          if (iResult < 0) {
            if (errno == ENOENT) {
              // Directory does not exist
              if (mkdir(sPath.c_str(), DEFAULT_MODE) != 0) {
                iErr = ERR_FILE_MKDIR;
              }
            } else if (errno == EACCES) {
              // Direcotry exists but is not readable
            } else {
              // Error
              iErr = ERR_FILE_MKDIR;
            }
          }
          if (iErr != ERR_NO) break;
          iPath++;
          if (iPath >= vPath.size()) break;
          sPath += cSeparator;
          sPath += vPath[iPath];
        } while (true);
      }
#endif        
    }
  }
  return iErr;
}

#if defined INDESIGN_PROJECT && !defined INDESIGN_SERVERVERSION

//****************************************************************************
/*! \brief  Dialog for file choosing to open a text file
 *  \param  oFile   Choosed file
 *  \param  sTitle  Title
 *  \return Flag: File choosed?
 ****************************************************************************/
bool CTextFile::dialogOpenFile(IDFile& oFile, PMString sTitle)
{
  vector<stInddFileFilter>  vFileFilter;
  stInddFileFilter          oFileFilter;

  do {
    oFileFilter.iSysOSType = '*';
    oFileFilter.sFileExt = PMString("*");
    oFileFilter.sFilterName = PMString("Alle Dateien");
    vFileFilter.push_back(oFileFilter);
    oFileFilter.iSysOSType = 'TEXT';
    oFileFilter.sFileExt = PMString("txt");
    oFileFilter.sFilterName = PMString("Textdatei (*.txt)");
    vFileFilter.push_back(oFileFilter);
    return dialogOpenFile(oFile, sTitle, vFileFilter);
  } while (kFalse);
  return kTrue;
}

//****************************************************************************
/*! \brief  Dialog for file choosing to open a file
 *  \param  oFile       Choosed file
 *  \param  sTitle      Title
 *  \param  vFileFilter Vector with the filters
 *  \return Flag: File choosed?
 ****************************************************************************/
bool CTextFile::dialogOpenFile(IDFile& oFile, PMString sTitle, vector<stInddFileFilter> vFileFilter)
{
  PMString            sFileExt;
	PMString            sFilterName;
  SDKFileOpenChooser  oDlgOpenChooser;

  do {
    if (sTitle.IsTranslatable()) sTitle.Translate();
    for (int iFile = 0; iFile < vFileFilter.size(); iFile++) {
      sFileExt = vFileFilter[iFile].sFileExt;
      sFilterName = vFileFilter[iFile].sFilterName;
      if (sFileExt.IsTranslatable()) sFileExt.Translate();
      if (sFilterName.IsTranslatable()) sFilterName.Translate();
      oDlgOpenChooser.AddFilter(vFileFilter[iFile].iSysOSType, sFileExt, sFilterName);
    }
    oDlgOpenChooser.SetTitle(sTitle);
		oDlgOpenChooser.ShowDialog();
    if (!oDlgOpenChooser.IsChosen()) {
      return kFalse;
    }
    oFile = oDlgOpenChooser.GetIDFile();
  } while (kFalse);
  return kTrue;
}

//****************************************************************************
/*! \brief  Dialog for file choosing to save a text file
 *  \param  oFile   Choosed file
 *  \param  sTitle  Title
 *  \return Flag: File choosed?
 ****************************************************************************/
bool CTextFile::dialogSaveFile(IDFile& oFile, PMString sTitle)
{
  vector<stInddFileFilter>  vFileFilter;
  stInddFileFilter          oFileFilter;

  do {
    oFileFilter.iSysOSType = '*';
    oFileFilter.sFileExt = PMString("*");
    oFileFilter.sFilterName = PMString("Alle Dateien");
    vFileFilter.push_back(oFileFilter);
    oFileFilter.iSysOSType = 'TEXT';
    oFileFilter.sFileExt = PMString("txt");
    oFileFilter.sFilterName = PMString("Textdatei (*.txt)");
    vFileFilter.push_back(oFileFilter);
    return dialogSaveFile(oFile, sTitle, vFileFilter);
  } while (kFalse);
  return kTrue;
}

//****************************************************************************
/*! \brief  Dialog for file choosing to save a file
 *  \param  oFile       Choosed file
 *  \param  sTitle      Title
 *  \param  vFileFilter Vector with the filters
 *  \return Flag: File choosed?
 ****************************************************************************/
bool CTextFile::dialogSaveFile(IDFile& oFile, PMString sTitle, vector<stInddFileFilter> vFileFilter)
{
  PMString            sFileExt;
	PMString            sFilterName;
  SDKFileSaveChooser  oDlgSaveChooser;
  SysOSType           macFileCreator = FileTypeRegistry::GetCurrentFileCreator(kPublicationFileTypeInfoID);

  do {
    if (sTitle.IsTranslatable()) sTitle.Translate();
    for (int iFile = 0; iFile < vFileFilter.size(); iFile++) {
      sFileExt = vFileFilter[iFile].sFileExt;
      sFilterName = vFileFilter[iFile].sFilterName;
      if (sFileExt.IsTranslatable()) sFileExt.Translate();
      if (sFilterName.IsTranslatable()) sFilterName.Translate();
      oDlgSaveChooser.AddFilter(macFileCreator, vFileFilter[iFile].iSysOSType, sFileExt, sFilterName);
    }
    oDlgSaveChooser.SetTitle(sTitle);
    oDlgSaveChooser.ShowDialog();
    if (!oDlgSaveChooser.IsChosen()) {
      return kFalse;
    }
    oFile = oDlgSaveChooser.GetIDFile();
  } while (kFalse);
  return kTrue;
}

#endif

//****************************************************************************
/*! \brief  Rename the file
 *  \param  sNewFilename        New file name (Target)
 *  \param  bDeleteExistingFile Flag: Allf�llig bereits existierendes File
 *                                    (Target) l�schen
 *  \return Flag: Renaming OK?
 ****************************************************************************/
bool CTextFile::renameFile(string sNewFilename, bool bDeleteExistingFile /*= true*/)
{
  bool bOK = true;
  
  bOK = copyFile(sNewFilename, bDeleteExistingFile);
  if (bOK) bOK = deleteFile();
  return bOK;
}

//****************************************************************************
/*! \brief  Copy the file
 *  \param  sNewFilename              New file name (Target)
 *  \param  bDeleteExistingFile Flag: Allf�llig bereits existierendes File
 *                                    (Target) l�schen
 *  \return Flag: Copying OK?
 ****************************************************************************/
bool CTextFile::copyFile(string sNewFilename, bool bDeleteExistingFile /*= true*/)
{
  int                 iErr  = ERR_NO;
  unsigned char       Buffer [1024 + 1];
  int                 Anz;
  FILE*               pSourceFile;
  FILE*               pTargetFile;
  CQXT4StringUtil     oStrUtil;

#ifndef WIN32
  oStrUtil.convertNetworkPathToHDPath(sNewFilename);
  oStrUtil.convertWinPathToMacPath(sNewFilename);
  oStrUtil.convertMacPathToUNIXPath(sNewFilename);
#endif  
  if (bDeleteExistingFile) {
    CTextFile oFileToDelete(sNewFilename);
    oFileToDelete.deleteFile();
  }
  pSourceFile = fopen(m_sFilename.c_str(), "r+b");
  if (pSourceFile != NULL) {
    pTargetFile = fopen(sNewFilename.c_str(), "w+b");
    if (pTargetFile != NULL) {
      while (iErr == ERR_NO) {
        Anz = (int) fread((void*) Buffer, sizeof(unsigned char), 1024, pSourceFile);
        if (Anz >= 0) {
          if (Anz == 0) break;
          Anz = (int) fwrite((void*) Buffer, sizeof(unsigned char), Anz, pTargetFile);
          if (Anz < 0) iErr = ERR_FILE_WRITING;
        } else iErr = ERR_FILE_READING;
      }
      fclose(pTargetFile);
    } else iErr = ERR_FILE_OPENFORWRITING;
    fclose(pSourceFile);
  } else iErr = ERR_FILE_OPENFORREADING;
  return iErr == ERR_NO;
}

//****************************************************************************
/*! \brief  Open a file
 *  \param  sFilename Filename
 *  \param  sMode     Opening mode
 *  \return Flag: Renaming OK?
 ****************************************************************************/
FILE* CTextFile::fileOpen(string sFilename, const char* sMode) 
{ 
 FILE* pFile = NULL;

#ifdef WIN32
  pFile = fopen(sFilename.c_str(), sMode);
#else
  CQXT4StringUtil oStrUtil;
  	
  oStrUtil.convertNetworkPathToHDPath(sFilename);
  oStrUtil.convertWinPathToMacPath(sFilename);
  oStrUtil.convertMacPathToUNIXPath(sFilename);
  sFilename = oStrUtil.convertPlatformToUTF8(sFilename);
  pFile = fopen(sFilename.c_str(), sMode);
#endif
 return pFile; 
}

//****************************************************************************
/*! \brief  Systemverzeichnis ermitteln
 *  \param  iFolderType Art des Verzeichnisses
 *  \return Systemverzeichnis
 ****************************************************************************/
string CTextFile::getSystemFolder(int iFolderType)
{
  string          sFolder = "";
  CQXT4StringUtil oStrUtil;
#ifdef WIN32
  TCHAR           sSystemFolder[MAX_PATH];
  LPITEMIDLIST    pIdlList;
  IMalloc*        pMalloc = NULL;

  SHGetMalloc(&pMalloc);
  SHGetSpecialFolderLocation(NULL, iFolderType, &pIdlList);
  SHGetPathFromIDList(pIdlList, sSystemFolder);
  pMalloc->Free(pIdlList);
#ifdef INDESIGN_PROJECT
  sFolder = oStrUtil.convertFromSQLWChar(sSystemFolder);
#else
  sFolder = sSystemFolder;
#endif
#else
#if __MAC_OS_X_VERSION_MAX_ALLOWED > 1070
  FSRef fsRef;
  int   iErrCode = FSFindFolder(kOnSystemDisk, iFolderType, kDontCreateFolder, &fsRef);
  if (noErr == iErrCode) {
    // Get the URL
    CFURLRef urlRef = CFURLCreateFromFSRef(0, &fsRef);
    if (urlRef) {
      char arBuffer[1024];
       if (CFURLGetFileSystemRepresentation(urlRef, true, (UInt8*) arBuffer, 1024)) {
        sFolder = string(arBuffer);
        oStrUtil.convertUNIXPathToMacPath(sFolder);
      }
      CFRelease(urlRef);
    }
  }
#else
  FSRef           fsRef;
  unsigned char   sPath[255 + 1];
  FSVolumeRefNum  volRefNum;
  SInt32          dirID;
  
  if (FindFolder(kOnSystemDisk,
                 iFolderType,
                 false,
                 &volRefNum,
                 &dirID) == 0) {
    FSRefParam refParm = { 0 };
    
    refParm.ioVRefNum = volRefNum;
    refParm.ioDirID = dirID;
    refParm.newRef = &fsRef;
    if (PBMakeFSRefSync(&refParm) == 0) {
      if (FSRefMakePath(&fsRef, sPath, (unsigned long) 255) == 0) {
        sFolder = (char*) sPath;
        oStrUtil.convertUNIXPathToMacPath(sFolder);
      }
    }
  }
#endif
#endif
  sFolder = oStrUtil.trimRightIgnoreCase(sFolder, PATH_DELIMITER);
  return sFolder;
}

//****************************************************************************
/*! \brief  Desktopverzeichnis ermitteln
 *  \return Desktopverzeichnis
 ****************************************************************************/
string CTextFile::getDesktopFolder()
{
#ifdef WIN32
  return getSystemFolder(CSIDL_DESKTOP);
#else
  return getSystemFolder(kDesktopFolderType);
#endif
}

//****************************************************************************
/*! \brief  PMM-Log-Verzeichnis ermitteln
 *  \return PMM-Log-Verzeichnis
 ****************************************************************************/
string CTextFile::getPMMLogFolder()
{
  CQXT4StringUtil oStrUtil;
  string          sLogFolder = getDesktopFolder();

  oStrUtil.appendPartToPath(sLogFolder, "PMM Logs", PATH_DELIMITER);
  return sLogFolder;
}

//****************************************************************************
/*! \brief  Temporäres Verzeichnis ermitteln
 *  \return Temporäres Verzeichnis
 ****************************************************************************/
string CTextFile::getTempFolder()
{
#ifdef WIN32
  string  sFolder = "";
  sFolder = getenv("TEMP");
  if (sFolder.length() <= 0) sFolder = getenv("TMP");
  if (sFolder.length() <= 0) sFolder = getCurrentUserFolder();
  return sFolder;
#else
  return getSystemFolder(kTemporaryFolderType);
#endif
}

//****************************************************************************
/*! \brief  Userverzeichnis des aktuellen Users ermitteln
 *  \return Userverzeichnis
 ****************************************************************************/
string CTextFile::getCurrentUserFolder()
{
#ifdef WIN32
  return getSystemFolder(CSIDL_PERSONAL);
#else
  return getSystemFolder(kCurrentUserFolderType);
#endif
}

//****************************************************************************
/*! \brief  Preferenz-Verzeichnis ermitteln
 *  \param  bAllUser      Preferenz-Verzeichnis aller User? Sonst aktueller
 *                        User
 *  \param  sApplication  Bezeichnung der Applikation
 *  \return Preferenz-Verzeichnis
 ****************************************************************************/
string CTextFile::getPreferenceFolder(bool bAllUser, string sApplication)
{
  string          sFolder = "";
  CQXT4StringUtil oStrUtil;

#ifdef WIN32
  if (bAllUser) sFolder = getSystemFolder(CSIDL_COMMON_APPDATA);
           else sFolder = getSystemFolder(CSIDL_APPDATA);
#else
  if (bAllUser) sFolder = getSystemFolder(kSystemPreferencesFolderType);
           else sFolder = getSystemFolder(kPreferencesFolderType);
#endif
  if (sApplication.length() > 0) {
    oStrUtil.appendPartToPath(sFolder, sApplication, PATH_DELIMITER);
  }
  return sFolder;
}

//****************************************************************************
/*! \brief  Konfigurationsfile auslesen
 *  \param  sCfgFilename  Dateiname des Konfigurationsfile
 *  \param  vValue        Vector mit Daten zum Auslesen
 *  \return Fehlercode
 ****************************************************************************/
int CTextFile::readConfigFile(string sCfgFilename, vector<stCfgFileParameter>& vValue)
{
  int             iErr = ERR_NO;
  int             iValue;
  int             iPos;
  int             iLen;
  char*           pStr = NULL;
  string          s = "";
  CQXT4StringUtil oStrUtil;

  do {
    for (iValue = 0; iValue < (int) vValue.size(); iValue++) {
      vValue[iValue].reset(false);
      vValue[iValue].sParam = oStrUtil.trimLeft(vValue[iValue].sParam, '[');
      vValue[iValue].sParam = oStrUtil.trimRight(vValue[iValue].sParam, '=');
      vValue[iValue].sParam = oStrUtil.trimRight(vValue[iValue].sParam, ']');
      vValue[iValue].sParam = '[' + vValue[iValue].sParam;
      vValue[iValue].sParam += "]=";
    }
    init(sCfgFilename);
    if (!existFile()) break;
    iErr = openForReading();
    if (iErr == ERR_NO) {
      do {
        iErr = readLine(&s);
        s = oStrUtil.trimLeftIgnoreCase(s, ' ');
        s = oStrUtil.trimRightIgnoreCase(s, ' ');
        for (iValue = 0; iValue < (int) vValue.size(); iValue++) {
          if (oStrUtil.firstIndexOfIgnoreCase(s, vValue[iValue].sParam) == 0) {
            break;
          }
        }
        if (iValue < (int) vValue.size()) {
          vValue[iValue].bSet = true;
          s = s.substr(vValue[iValue].sParam.length(), s.length());
          string sComment[] = { " /*", "\t/*", " //", "\t//" };
          for (int iComment = 0; iComment < 4; iComment++) {
            iPos = oStrUtil.lastIndexOfIgnoreCase(s, sComment[iComment]);
            if (iPos >= 0) {
              s = s.substr(0, iPos);
              break;
            }
          }
          do {
            iLen = (int) s.length();
            s = oStrUtil.trimLeftIgnoreCase(s, ' ');
            s = oStrUtil.trimRightIgnoreCase(s, ' ');
            s = oStrUtil.trimLeftIgnoreCase(s, '\t');
            s = oStrUtil.trimRightIgnoreCase(s, '\t');
          } while (iLen != s.length());
          vValue[iValue].sValue = s;
          if (oStrUtil.equalsIgnoreCase(vValue[iValue].sValue, "true") ||
              oStrUtil.equalsIgnoreCase(vValue[iValue].sValue, "yes") ||
              oStrUtil.equalsIgnoreCase(vValue[iValue].sValue, "wahr") ||
              oStrUtil.equalsIgnoreCase(vValue[iValue].sValue, "ja")) {
            vValue[iValue].iValue = 1;
            vValue[iValue].dValue = 1.0;
            vValue[iValue].bValue = true;
          } else if (oStrUtil.equalsIgnoreCase(vValue[iValue].sValue, "false") ||
              oStrUtil.equalsIgnoreCase(vValue[iValue].sValue, "no") ||
              oStrUtil.equalsIgnoreCase(vValue[iValue].sValue, "falsch") ||
              oStrUtil.equalsIgnoreCase(vValue[iValue].sValue, "nein")) {
            vValue[iValue].iValue = 0;
            vValue[iValue].dValue = 0.0;
            vValue[iValue].bValue = false;
          } else {
            iPos = oStrUtil.firstIndexOfIgnoreCase(s, "0x");
            if (iPos == 0) {
              s = oStrUtil.trimLeftIgnoreCase(s, "0x");
              vValue[iValue].iValue = strtol(s.c_str(), &pStr, 16);
              vValue[iValue].dValue = (double) vValue[iValue].iValue;
              vValue[iValue].bValue = vValue[iValue].iValue != 0;
            } else {
              vValue[iValue].iValue = oStrUtil.getInt(vValue[iValue].sValue);
              vValue[iValue].dValue = oStrUtil.getDouble(vValue[iValue].sValue);
              vValue[iValue].bValue = vValue[iValue].iValue != 0;
            }
          }
        }
      } while (iErr == ERR_NO);
      if (iErr == ERR_FILE_EOF) iErr = ERR_NO;
      close();
    }
  } while (false);
  return iErr;
}

//****************************************************************************
/*! \brief  String in einem File ersetzen; Es kann auch ein bin�res File sein.
 *  \param  sFilename Dateiname
 *  \param  sSearch   Zu suchender String
 *  \param  sReplace  String der ersetzt
 *  \return Fehlercode
 ****************************************************************************/
int CTextFile::binarySearchAndReplace(string sFilename, vector<string> vTextSearch, vector<string> vTextReplace)
{
  bool                bSearchTextFound = false;
  int                 iErr = ERR_NO;
  int                 iStartPos = 0;
  int                 iPos = 0;
  int                 iSearchText = 0;
  int                 iSearchTextPos = 0;
  int                 iLenRead = 0;
  int                 iTotalLenRead = 0;
  int                 iBufferStart = 0;
  int                 iBufferEnd = 0;
  CDateTime           oDateTime;
  string              sDestFileName = getDesktopFolder() + PATH_DELIMITER + oDateTime.GetString("yyyymmddHHMMSS") + ".bin";
  CTextFile           oSourceFile(sFilename);
  CTextFile           oDestFile(sDestFileName);
  CQXT4StringUtil     oStrUtil;
  unsigned char       vBuffer[10240 + 1];
  unsigned char       vBufferReplace[10240 + 1];
  const int           iBufferLen = sizeof(vBuffer) - 1;
  int                 iBufferReplaceLen = 0;
  int                 iMaxLen = 0;
  string              sReplaceBuffer;
  vector<stSAndRText> vSearchText;
  stSAndRText         stSAndRText;
  fpos_t              iCurFilePos = -1;
  fpos_t              iNewFilePos = -1;

  iErr = oSourceFile.openForReading();
  if (iErr == ERR_NO) oDestFile.openForWriting();
  for (iSearchText = 0; iSearchText < (int) vTextSearch.size(); iSearchText++) {
    iMaxLen = max((int) iMaxLen, (int) vTextSearch[iSearchText].size());
  }
  while (iErr == ERR_NO) {
    if (feof(oSourceFile.getFile())) {
      break;
    }
    fgetpos(oSourceFile.getFile(), &iCurFilePos);
    iLenRead = oSourceFile.readBinary(vBuffer, iBufferLen);
    iTotalLenRead += iLenRead;
    iNewFilePos = -1;
    if (iLenRead > 0) {
      vSearchText.clear();
      for (iSearchText = 0; iSearchText < (int) vTextSearch.size(); iSearchText++) {
        // Buffer durchsuchen
        if (vTextSearch[iSearchText].size() > 0) {
          for (iPos = 0; iPos < iLenRead; iPos++) {
            if (vBuffer[iPos] != vTextSearch[iSearchText][0]) {
              continue;
            }
            stSAndRText.reset();
            stSAndRText.iPosStart = iPos;
            iSearchTextPos = 0;
            while (iSearchTextPos < (int) vTextSearch[iSearchText].size() && iPos < iLenRead && vBuffer[iPos] == vTextSearch[iSearchText][iSearchTextPos]) {
              iPos++;
              iSearchTextPos++;
            }
            if (iSearchTextPos == vTextSearch[iSearchText].size()) {
              // String gefunden -> ersetzen
              stSAndRText.iPosEnd = iPos;
              stSAndRText.iSearchText = iSearchText;
              vSearchText.push_back(stSAndRText);
            } else if (iNewFilePos < 0 && iPos >= iLenRead && iLenRead == iBufferLen) {
              // Teile des Suchtextes wurden gefunden, aber das Bufferende wurde erreicht.
              // --> Filepointer zur�ckstellen vor dem n�chsten Lesen des Buffers
              iNewFilePos = iCurFilePos + iLenRead - iMaxLen;
  #ifdef WIN32
              if (iNewFilePos >= 0) fseek(oSourceFile.getFile(), (long) iNewFilePos, SEEK_SET);
  #else
              if (iNewFilePos >= 0) fseek(oSourceFile.getFile(), iNewFilePos, SEEK_SET);
  #endif
              iLenRead -= iMaxLen;
            }
          }
        }
      }
      // Buffer schreiben
      if (vSearchText.size() > 0) {
        std::sort(vSearchText.begin(), vSearchText.end(), sortTextSAndR);
        iBufferStart = 0;
        iBufferEnd = 0;
        for (iSearchText = 0; iSearchText < (int) vSearchText.size(); iSearchText++) {
          if (vSearchText[iSearchText].iSearchText < (int) vTextReplace.size()) {
            sReplaceBuffer = vTextReplace[vSearchText[iSearchText].iSearchText];
          } else {
            sReplaceBuffer = "";
          }
          if (vSearchText[iSearchText].iPosStart == 0) {
            // Searchtext steht am Anfang
            if (sReplaceBuffer.size() > 0) {
              oDestFile.writeBinary((unsigned char*) sReplaceBuffer.c_str(), (int) sReplaceBuffer.size());
            }
            iBufferStart = vSearchText[iSearchText].iPosEnd;
          } else {
            iBufferEnd = vSearchText[iSearchText].iPosStart;
            iBufferReplaceLen = iBufferEnd - iBufferStart;
            if (iBufferReplaceLen > 0) {
              memcpy(&vBufferReplace, vBuffer + iBufferStart, iBufferReplaceLen);
              oDestFile.writeBinary(vBufferReplace, iBufferReplaceLen);
              if (sReplaceBuffer.size() > 0) {
                oDestFile.writeBinary((unsigned char*) sReplaceBuffer.c_str(), (int) sReplaceBuffer.size());
              }
            }
            iBufferStart = vSearchText[iSearchText].iPosEnd;
            // Letzter Text
            if (iSearchText == vSearchText.size() - 1) {
              iBufferEnd = iLenRead;
              iBufferReplaceLen = iBufferEnd - iBufferStart;
              if (iBufferReplaceLen > 0) {
                memcpy(&vBufferReplace, vBuffer + iBufferStart, iBufferReplaceLen);
                oDestFile.writeBinary(vBufferReplace, iBufferReplaceLen);
              }
            }
          }
        }
      } else {
        oDestFile.writeBinary(vBuffer, iLenRead);
      }
    } else {
      break;  // Dateiende erreicht
    }
  }
  oSourceFile.close();
  oDestFile.close();
  if (iErr == ERR_NO) {
    iErr = ERR_NO;
    if (oSourceFile.deleteFile()) {
      if (!oDestFile.renameFile(sFilename, true)) {
        iErr = ERR_FILE_COPY;
      }
    } else {
      iErr = ERR_FILE_DELETE;
    }
  }
  return iErr;
}

//****************************************************************************
/*! \brief  Exists a file
 *  \return Error code
 ****************************************************************************/
bool CTextFile::existFile(string sFile /* = ""*/)
{
	int iErr = ERR_NO;

  if (sFile.length() <= 0) {
    sFile = m_sFilename;
  }
  if (sFile.length() <= 0) {
#ifdef DEBUG
    CLogfile::consoleOut("ERROR CTextFile::existFile -> Filename is missing!");
#endif
    return false;
  }
  FILE* pFile = fileOpen(sFile.c_str(), "rb");
  if (pFile == NULL) {
#ifdef DEBUG
    CLogfile::consoleOutFormat("ERROR CTextFile::existFile -> File %s doesn't exist!", sFile.c_str());
#endif
    return false;
  }
  fclose(pFile);
#ifdef DEBUG
  CLogfile::consoleOutFormat("ERROR CTextFile::existFile -> File %s exists!", sFile.c_str());
#endif
  return true;
}

/*****************************************************************
 writeLogFile
******************************************************************/
void CTextFile::writeLogFile(const char* sFrm, ...)
{
  bool            bOK = true;
  int             iLen = 0;
  char*           p = NULL;
#ifndef WIN32
  char            sTmp[255 + 1];
#endif  
  va_list         Args;       // Optionale Argumente
  CQXT4StringUtil oStrUtil;
  CLogfile        oLogfile;
  string          sLog = "";
  string          sFilename = CTextFile::getPMMLogFolder();

  //-----------------------------------------------------------------------
  // Logfile
  //-----------------------------------------------------------------------
  oStrUtil.appendPartToPath(sFilename, "TextFile.log", PATH_DELIMITER);
  //-----------------------------------------------------------------------
  // Text setzen
  //-----------------------------------------------------------------------
  va_start(Args, sFrm);
#ifdef WIN32
  iLen = _vscprintf(sFrm, Args);
#else
  iLen = vsnprintf((char*) sTmp, 255, sFrm, Args);
#endif
  va_end(Args);
  if (iLen > 0) {
    p = (char*) malloc(iLen + 1);
    if (p == NULL) {
      return;
    }
    va_start(Args, sFrm);
#if _SECURE_ATL
    vsprintf_s(p, iLen + 1, sFrm, Args);
#else
    vsprintf(p, sFrm, Args);
#endif
    va_end(Args);
    oLogfile.Init(sFilename, false);
    sLog = p;
    oStrUtil.replaceStr(sLog, "\r\n", "\n");
    oStrUtil.replaceStr(sLog, "\n\r", "\n");
    oStrUtil.replaceStr(sLog, "\n", "\r\n");
    bOK = oLogfile.TestOut(false, true, sLog);
    free(p);
  }
}

//****************************************************************************
/*! \brief    Testet einen Dateinamen auf eine bestimmte Extension
 *  \param    sFilename   Filename
 *  \param    sExtension  Erweiterung
 *  \return   Flag: Hat Erweiterung?
 ****************************************************************************/
bool CTextFile::hasExtension(string sFilename, string sExtension)
{
  bool            bHasExtension = false;
  int             iPos;
  CQXT4StringUtil oStrUtil;

  sExtension = oStrUtil.trimLeftIgnoreCase(sExtension, '.');
  sExtension = "." + sExtension;
  iPos = oStrUtil.firstIndexOfIgnoreCase(sFilename, sExtension);
  if (iPos >= 0 && iPos == sFilename.length() - sExtension.length()) {
    bHasExtension = true;
  }
  return bHasExtension;
}
