//****************************************************************************
/*! \file
 *  \brief    Sourcedatei f�r die Funktion mkdir (aus stat.mac.c uebernommen
 *            und angepasst)
 *  \author   Hans St�ssel
 *  \date     2003
 *  \version  1.0.0
 *  \bug      
 *  \warning  
 *  \todo     
 ****************************************************************************/
#include "MkDir.h"
#include "QXT4StringUtil.h"
#include "TextFile.h"

/*
 *	int mkdir(const char *path, int mode)
 *
 *		Creates a directory. (NB: mode is ignored on the mac)
 */
int mkdir(string sPath)
{
#if __MAC_OS_X_VERSION_MAX_ALLOWED > 1050
  CTextFile oTextFile;
  return oTextFile.createDirectory(sPath);
#else
	OSErr err = -1;
		
	HFileParam	    fpb;
	Str255		      ppath;
	CQXT4StringUtil oStrUtil;

	if (sPath.length() > 0) {
		/* convert the c string into a pascal string */
		oStrUtil.convertCToPIfMac(sPath, (unsigned char*) ppath, 255);

		fpb.ioNamePtr = (unsigned char*) ppath;
		fpb.ioVRefNum = 0;
		fpb.ioDirID = 0L;
		err = PBDirCreateSync((HParmBlkPtr) &fpb);
	}
	if (err == dupFNErr) {
	  err = noErr;
	}
	return (err == noErr ? 0 : -1);
#endif
}
