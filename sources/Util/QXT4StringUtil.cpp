//****************************************************************************
/*! \file
 *  \brief    Sourcedatei f¸r Funktionen zum Anzeigen einer Fehlermeldung
 *  \author   Hans Stˆssel
 *  \date     2003 - 2012
 *  \version  1.0.0
 *  \version  1.0.1 HS 20091006 -> Änderung in convertUNIXPathToMacPath
 *  \bug      
 *  \warning  
 *  \todo     
 ****************************************************************************/

#include "QXT4StringUtil.h"

//****************************************************************************
/*! \brief  Konstruktor
 ****************************************************************************/
CQXT4StringUtil::CQXT4StringUtil()
{

}

//****************************************************************************
/*! \brief  Destruktor
 ****************************************************************************/
CQXT4StringUtil::~CQXT4StringUtil()
{

}

//****************************************************************************
/*! \brief  String in einem String ersetzen
 *  \param  s           String, in dem ein String ersetzt werden soll
 *  \param  iLen        Maximale L‰nge des Strings
 *  \param  sToReplace  String der ersetzt werden soll
 *  \param  sReplace    String der den String ersetzt
 ****************************************************************************/
void CQXT4StringUtil::replaceStr(char* s, int iLen, string sToReplace, string sReplace)
{
    string  sStr = s;
    
    replaceStr(sStr, sToReplace, sReplace);
#if _SECURE_ATL
    strcpy_s(s, iLen + 1, sStr.c_str());
#else
#ifdef WIN32
    int iStrLen = __min((int) sStr.length(), iLen);
#else
    int iStrLen = min((int) sStr.length(), iLen);
#endif
    strncpy(s, sStr.c_str(), iStrLen);
    s[iStrLen] = '\0';
#endif
}

//****************************************************************************
/*! \brief  Strings vergleichen, unabh‰ngig von Gross- und Kleinschreibung
 *  \param  s1  1. String, der vergleichen werden soll
 *  \param  s2  2. String, der vergleichen werden soll
 *  \return Sind die beiden Strings gleich?
 *  \retval true -> Sie sind gleich
 *  \retval false -> Sie sind nicht gleich
 ****************************************************************************/
bool CQXT4StringUtil::equalsIgnoreCase(string s1, string s2)
{
    s1 = makeLower(s1);
    s2 = makeLower(s2);
    return s1 == s2;
}

//****************************************************************************
/*! \brief    Windows- oder Macintosh- in UTF8-String wandeln
 *  \param    sStr    Zu wandelnder String
 *  \return   UTF-String
 ****************************************************************************/
string CQXT4StringUtil::convertPlatformToUTF8(string sStr)
{
    string s = sStr;
    sStr = "";
#ifndef WIN32
#ifdef __MACH__
    sStr = transcodeStringEncoding(s,
                                   kCFStringEncodingMacRoman,
                                   kCFStringEncodingUTF8);
#else
    sStr = "CQXT4StringUtil::convertPlatformToUTF8 -> Not supported!";
#endif
#else
    sStr = transcodeStringEncoding(s,
                                   CP_ACP,
                                   CP_UTF8);
#endif
    return sStr;
}


//****************************************************************************
/*! \brief  Converts a network path into a HD path -> Removes name of the
 *          server (e.g. \\PM_SRV_001\) at the beginning of the path.
 *  \param  sPath             Pfad
 *  \param  sHDPath           Konvertierter Pfad
 *  \param  iHDPathLen        L‰nge des HD-Pfads
 *  \param  bConvertToPString Mac-FileName in einen P-String konvertieren?
 ****************************************************************************/
void CQXT4StringUtil::convertNetworkPathToHDPath(string sPath, unsigned char* sHDPath, int iHDPathLen, bool bConvertToPString)
{
    convertNetworkPathToHDPath(sPath);
    if (bConvertToPString) {
        convertCToP(sPath, sHDPath, iHDPathLen);
#if _SECURE_ATL
    } else {
        strcpy_s((char*) sHDPath, iHDPathLen + 1, sPath.c_str());
    }
#else
} else {
#ifdef WIN32
    int iStrLen = __min((int) sPath.length(), iHDPathLen);
#else
    int iStrLen = min((int) sPath.length(), iHDPathLen);
#endif
    strncpy((char*) sHDPath, sPath.c_str(), iStrLen);
    sHDPath[iStrLen] = '\0';
}
#endif
}

//****************************************************************************
/*! \brief  Filename und Pfad vom Macintosh-(HFS)- ins UNIX-(POSIX)-Format
 *          konvertieren
 *  \param  sFileName   Pfad + Filename
 *  \return Konvertierter Pfad + Filename
 ****************************************************************************/
void CQXT4StringUtil::convertMacPathToUNIXPath(string& sFileName)
{
    CFStringRef   sRef1 = NULL;
    CFStringRef   sRef2 = NULL;
    CFURLRef      cfurl = NULL;
    unsigned char s[255 + 1];
    int           iPos;
    int           iPosMac;
    int           iPosUnix;
    string        sDelimiterUNIX = PATH_DELIMITER_UNIX;
    
    if (!isUNIXPath(sFileName)) {
        iPos = sFileName.find(PATH_DELIMITER_MAC);
        while ((iPos = sFileName.find(PATH_DELIMITER_WIN)) >= 0) {
            sFileName.replace(iPos, 1, sDelimiterUNIX);
        }
        iPosUnix = sFileName.find(PATH_DELIMITER_UNIX);
        iPosMac = sFileName.find(PATH_DELIMITER_MAC);
        if (iPosUnix >= 0 || iPosMac >= 0) {
            sRef1= CFStringCreateWithCString(kCFAllocatorDefault,
                                             sFileName.c_str(),
                                             kCFStringEncodingMacHFS);
            cfurl = CFURLCreateWithFileSystemPath(kCFAllocatorDefault,
                                                  sRef1,
                                                  kCFURLHFSPathStyle,
                                                  true);
            if (cfurl != NULL) {
                sRef2 = CFURLCopyFileSystemPath(cfurl, kCFURLPOSIXPathStyle);
                if (sRef2 != NULL) {
                    CFStringGetCString(sRef2, (char*) s, 255, kCFStringEncodingMacHFS);
                }
            }
            if (cfurl != NULL) CFRelease(cfurl);
            if (sRef1 != NULL) CFRelease(sRef1);
            if (sRef2 != NULL) CFRelease(sRef2);
            sFileName = (char*) s;
        }
    }
}

//****************************************************************************
/*! \brief  Filename und Pfad ins Macintosh-Format konvertieren.
 *          Backslash suchen und den davorliegenden Teil lˆschen.
 *  \param  sFileName         Pfad + Filename
 *  \param  sMacFileName      Konvertierter Pfad + Filename als P-String
 *  \param  iMacFileNameLen   Maximale L‰nge des Filenamens
 *  \param  bConvertToPString Mac-FileName in einen P-String konvertieren?
 ****************************************************************************/
void CQXT4StringUtil::convertWinPathToMacPath(string sFileName, unsigned char* sMacFileName, int iMacFileNameLen, bool bConvertToPString)
{
    convertWinPathToMacPath(sFileName);
    if (bConvertToPString) {
        convertCToP(sFileName, sMacFileName, iMacFileNameLen);
#if _SECURE_ATL
    } else {
        strcpy_s((char*) sMacFileName, iMacFileNameLen + 1, sFileName.c_str());
    }
#else
} else {
#ifdef WIN32
    int iStrLen = __min((int) sFileName.length(), iMacFileNameLen);
#else
    int iStrLen = min((int) sFileName.length(), iMacFileNameLen);
#endif
    strncpy((char*) sMacFileName, sFileName.c_str(), iStrLen);
    sMacFileName[iStrLen] = '\0';
}
#endif
}

//****************************************************************************
/*! \brief  Erste Position eines Substrings suchen, unabh‰ngig von Gross- und
 *          Kleinschreibung
 *  \param  s         String, in dem gesucht werden soll
 *  \param  sSub      Gesuchter Subsring
 *  \param  iStartPos Startposition zum Suchen
 *  \return Position des Substrings
 ****************************************************************************/
int CQXT4StringUtil::firstIndexOfIgnoreCase(string s, string sSub, int iStartPos /*= 0*/)
{
    string sLower = makeLower(s);
    string sSubLower = makeLower(sSub);
    
    return (int) sLower.find(sSubLower, iStartPos);
}

//****************************************************************************
/*! \brief  Substring links entfernen, unabh‰ngig von Gross- und Klein-
 *          schreibung
 *  \param  s       String, in dem gesucht werden soll
 *  \param  sToTrim Zu entfernender String
 *  \return Neuer String
 ****************************************************************************/
string CQXT4StringUtil::trimLeftIgnoreCase(string s, string sToTrim)
{
    string  sNew = s;
    
    while (firstIndexOfIgnoreCase(sNew, sToTrim) == 0) {
        sNew = sNew.substr(sToTrim.length());
    }
    return sNew;
}

//****************************************************************************
/*! \brief  Split a string
 *  \param  s           String
 *  \param  sDelimiter  Delimiter string
 *  \param  vSplit      Vector with splitted strings
 *  \return Number of splitted strings
 ****************************************************************************/
int CQXT4StringUtil::split(string s, string sDelimiter, vector<string>& vSplit)
{
    int     iPos;
    string  sParam;
    
    vSplit.clear();
    while ((iPos = (int) s.find(sDelimiter)) >= 0) {
        sParam = s.substr(0, iPos);
        vSplit.push_back(sParam);
        s = s.substr(iPos + sDelimiter.length(), s.length() - iPos - sDelimiter.length());
    }
    vSplit.push_back(s);
    return (int) vSplit.size();
}

//****************************************************************************
/*! \brief  Zeichen links entfernen, unabh‰ngig von Gross- und Klein-
 *          schreibung
 *  \param  s       String, in dem gesucht werden soll
 *  \param  cToTrim Zu entfernendes Zeichen
 *  \return Neuer String
 ****************************************************************************/
string CQXT4StringUtil::trimLeftIgnoreCase(string s, char cToTrim)
{
    string  sToTrim;
    
    sprintfString(sToTrim, (char*) "%c", cToTrim);
    return trimLeftIgnoreCase(s, sToTrim);
}


//****************************************************************************
/*! \brief  Zeichen rechts entfernen, unabh‰ngig von Gross- und Klein-
 *          schreibung
 *  \param  s       String, in dem gesucht werden soll
 *  \param  cToTrim Zu entfernendes Zeichen
 *  \return Neuer String
 ****************************************************************************/
string CQXT4StringUtil::trimRightIgnoreCase(string s, char cToTrim)
{
    string  sToTrim;
    
    sprintfString(sToTrim, (char*) "%c", cToTrim);
    return trimRightIgnoreCase(s, sToTrim);
}

//****************************************************************************
/*! \brief  String in Kleinbuchstaben wandeln
 *  \param  s   String, der gewandelt werden soll
 *  \return String in Kleinbuchstaben
 ****************************************************************************/
string CQXT4StringUtil::makeLower(string s)
{
    string  sLower = s;
    
#if __GNUC__
    for (size_t i(0); i < sLower.size(); ++i) {
        sLower[i] = char(std::tolower(sLower[i]) );
    }
#else
    std::transform(sLower.begin(), sLower.end(), sLower.begin(), tolower);
#endif
    return sLower;
}

//****************************************************************************
/*! \brief  Converts a network path into a HD path -> Removes name of the
 *          server (e.g. \\PM_SRV_001\) at the beginning of the path.
 *  \param  sPath Path of the file/directory
 *  \return ERR_NO
 ****************************************************************************/
void CQXT4StringUtil::convertNetworkPathToHDPath(string& sPath)
{
    bool    bUNIXPath = false;
    string  s = "";
    int     iPos = -1;
    string  sNewPath;
    
#ifndef WIN32
    bUNIXPath = isUNIXPath(sPath);
#endif
    if (bUNIXPath) {
#ifndef WIN32
        convertUNIXPathToMacPath(sPath);
#endif
    } else {
        s =  PATH_DELIMITER_WIN;
        s += PATH_DELIMITER_WIN;
        replaceChar(sPath, PATH_DELIMITER_UNIX_CHAR, PATH_DELIMITER_WIN_CHAR);
        if (sPath.find(s) == 0) {
            iPos = (int) sPath.find(PATH_DELIMITER_WIN, 2);
            if (iPos > 0)  {
                sNewPath = sPath.substr(iPos + 1, sPath.length() - iPos - 1);
                iPos = (int) sNewPath.find(PATH_DELIMITER_WIN, 0);
                if (iPos >= 0)  {
                    s =  PATH_DELIMITER_MAC;
                    s += PATH_DELIMITER_WIN;
                    sNewPath.replace(iPos, 1, s);
                }
                sPath = sNewPath;
            }
        }
    }
}

//****************************************************************************
/*! \brief  Convert a C into a Pascal string
 *  \param  sCString  C string
 *  \param  sPString  Pascel string, converted from sCString
 ****************************************************************************/
void CQXT4StringUtil::convertCToP(string sCString, unsigned char* sPString, int iPStringLen)
{
    unsigned char* s;
    // Init
#ifdef WIN32
    int iStrLen = __min((int) iPStringLen, (int) sCString.length());
#else
    int iStrLen = min((int) iPStringLen, (int) sCString.length());
#endif
    s = new unsigned char[iStrLen + 1];
#if _SECURE_ATL
    strncpy_s((char*) s, iStrLen + 1, sCString.c_str(), iStrLen);
    s[iStrLen] = '\0';
    strncpy_s((char*) sPString + 1, iStrLen, (char*) s, iStrLen);
#else
    strncpy((char*) s, sCString.c_str(), iStrLen);
    s[iStrLen] = '\0';
    strncpy((char*) sPString + 1, (char*) s, iStrLen);
#endif
    sPString[0] = iStrLen;
    delete [] s;
}

//****************************************************************************
/*! \brief  Handelt es sich um einen UNIX-Pfad
 *  \param  sFileName   Pfad + Filename
 *  \return Flag: UNIX-Pfad
 ****************************************************************************/
bool CQXT4StringUtil::isUNIXPath(string sFileName)
{
    if (sFileName.length() == 1) {
        return sFileName.at(0) == PATH_DELIMITER_UNIX_CHAR;
    }
    if (sFileName.length() > 1) {
        return sFileName.at(0) == PATH_DELIMITER_UNIX_CHAR && sFileName.at(1) != PATH_DELIMITER_UNIX_CHAR;
    }
    return false;
}



//****************************************************************************
/*! \brief  Filename und Pfad ins Macintosh-Format konvertieren.
 *  \param  sFileName     Pfad + Filename; muss ein Windowspfad sein!
 *  \param  sMacFileName  Konvertierter Pfad + Filename
 ****************************************************************************/
void CQXT4StringUtil::convertWinPathToMacPath(string& sFileName)
{
#ifndef WIN32
    bool    bUnixPath = isUNIXPath(sFileName);
#else
    bool    bUnixPath = false;
#endif
    int 	  iPos = 0;
    string  s = "";
    
    if (sFileName.length() > 0) {
        if (bUnixPath) {
#ifndef WIN32
            convertUNIXPathToMacPath(sFileName);
#endif
        } else {
            s = DRIVE_DELIMITER;
            s += PATH_DELIMITER_UNIX;
            iPos = (int) sFileName.find(s, 0);
            s = PATH_DELIMITER_WIN;
            if (iPos >= 0) sFileName.replace(iPos, 2, s);
            
            s = DRIVE_DELIMITER;
            s += PATH_DELIMITER_WIN;
            iPos = (int) sFileName.find(s, 0);
            s = PATH_DELIMITER_WIN;
            if (iPos >= 0) sFileName.replace(iPos, 2, s);
            
            s = DRIVE_DELIMITER;
            iPos = (int) sFileName.find(s, 0);
            if (iPos == 0) sFileName.erase(iPos, 1);
            
            replaceChar(sFileName, PATH_DELIMITER_UNIX_CHAR, PATH_DELIMITER_MAC_CHAR);
            replaceChar(sFileName, PATH_DELIMITER_WIN_CHAR, PATH_DELIMITER_MAC_CHAR);
        }
    }
}	

//****************************************************************************
/*! \brief  Substring rechts entfernen, unabh‰ngig von Gross- und Klein-
 *          schreibung
 *  \param  s       String, in dem gesucht werden soll
 *  \param  sToTrim Zu entfernender String
 *  \return Neuer String
 ****************************************************************************/
string CQXT4StringUtil::trimRightIgnoreCase(string s, string sToTrim)
{
    int     iPos = 0;
    string  sNew = s;
    
    while (iPos >= 0 && (iPos = lastIndexOfIgnoreCase(sNew, sToTrim)) == ((int) sNew.length() - (int) sToTrim.length())) {
        sNew = sNew.substr(0, iPos);
    }
    return sNew;
}

//****************************************************************************
/*! \brief  Zeichen in einem String ersetzen
 *  \param  s           String, in dem ein Zeichen ersetzt werden soll
 *  \param  cToReplace  Zeichen das ersetzt werden soll
 *  \param  cReplace    Zeichen, das das Zeichen ersetzt
 ****************************************************************************/
void CQXT4StringUtil::replaceChar(string& s, char cToReplace, char cReplace)
{
    string  sToReplace = "";
    string  sReplace = "";
    sToReplace += cToReplace;
    sReplace += cReplace;
    replaceStr(s, sToReplace, sReplace);
}

//****************************************************************************
/*! \brief  Letzte Position eines Substrings suchen, unabh‰ngig von Gross- und
 *          Kleinschreibung
 *  \param  s     String, in dem gesucht werden soll
 *  \param  sSub  Gesuchter Subsring
 *  \return Position des Substrings
 ****************************************************************************/
int CQXT4StringUtil::lastIndexOfIgnoreCase(string s, string sSub)
{
    string sLower = makeLower(s);
    string sSubLower = makeLower(sSub);
    
    return (int) sLower.rfind(sSubLower);
}

//****************************************************************************
/*! \brief  Substring rechts entfernen
 *  \param  s       String, in dem gesucht werden soll
 *  \param  sToTrim Zu entfernender String
 *  \return Neuer String
 ****************************************************************************/
string CQXT4StringUtil::trimRight(string s, string sToTrim)
{
    int     iPos = 0;
    string  sNew = s;
    
    while (iPos >= 0 && (iPos = (int) sNew.rfind(sToTrim)) == ((int) sNew.length() - (int) sToTrim.length())) {
        sNew = sNew.substr(0, iPos);
    }
    return sNew;
}

//****************************************************************************
/*! \brief  Zeichen rechts entfernen
 *  \param  s       String, in dem gesucht werden soll
 *  \param  cToTrim Zu entfernendes Zeichen
 *  \return Neuer String
 ****************************************************************************/
string CQXT4StringUtil::trimRight(string s, char cToTrim)
{
    string  sToTrim;
    
    sprintfString(sToTrim, (char*) "%c", cToTrim);
    return trimRight(s, sToTrim);
}

//****************************************************************************
/*! \brief  Appends a part, delimited by a delimiter, to a file path
 *  \param  sPath       Path
 *  \param  sPart       Part
 *  \param  sDelimiter  Delimiter
 ****************************************************************************/
void CQXT4StringUtil::appendPartToPath(string& sPath, string sPart, string sDelimiter /*= ""*/)
{
    appendDelimiterToPath(sPath, sDelimiter);
    sPath += sPart;
}

//****************************************************************************
/*! \brief  Substring links entfernen
 *  \param  s       String, in dem gesucht werden soll
 *  \param  sToTrim Zu entfernender String
 *  \return Neuer String
 ****************************************************************************/
string CQXT4StringUtil::trimLeft(string s, string sToTrim)
{
    string  sNew = s;
    
    while (sNew.find(sToTrim) == 0) {
        sNew = sNew.substr(sToTrim.length());
    }
    return sNew;
}

//****************************************************************************
/*! \brief  Zeichen links entfernen
 *  \param  s       String, in dem gesucht werden soll
 *  \param  cToTrim Zu entfernendes Zeichen
 *  \return Neuer String
 ****************************************************************************/
string CQXT4StringUtil::trimLeft(string s, char cToTrim)
{
    string  sToTrim;
    
    sprintfString(sToTrim, (char*) "%c", cToTrim);
    return trimLeft(s, sToTrim);
}

//****************************************************************************
/*! \brief  Appends a delimiter to a file path
 *  \param  sPath       Path
 *  \param  sDelimiter  Delimiter
 ****************************************************************************/
void CQXT4StringUtil::appendDelimiterToPath(string& sPath, string sDelimiter /*= ""*/)
{
    if (sDelimiter.length() == 0) {
        sDelimiter = PATH_DELIMITER;
    }
    if (sPath.length() > 0) {
        sPath = trimRight(sPath, sDelimiter);
        sPath += sDelimiter;
    }
}

//****************************************************************************
/*! \brief  Convert a string into a int
 *  \param  s   String to convert
 *  \return double-Value of the string
 ****************************************************************************/
int CQXT4StringUtil::getInt(string s)
{
    return atoi((char*) s.c_str());
}

//****************************************************************************
/*! \brief  Convert a string into a double
 *  \param  s   String to convert
 *  \return double-Value of the string
 ****************************************************************************/
double CQXT4StringUtil::getDouble(string s)
{
    return atof((char*) s.c_str());
}

//****************************************************************************
/*! \brief  Zwei Strings zusammenf¸gen, getrennt durch ein Trennstring.
 *            Vor dem Zusammenf¸gen werden beim ersten String allf‰llig
 *            vorhandene TrennString's am Ende entfernt!
 *  \param  sStr1       Erster String
 *  \param  sStr2       Zweiter String
 *  \param  sSeparator  Trennstring
 *  \return Zusammengef¸gter String
 ****************************************************************************/
string CQXT4StringUtil::compoundString(string sStr1, string sStr2, string sSeparator)
{
    string sStr = "";
    
    sStr1 = trimRight(sStr1, sSeparator);
    sStr2 = trimLeft(sStr2, sSeparator);
    if (sStr1.length() > 0) {
        sStr = sStr1;
        if (sStr2.length() > 0) {
            sStr += sSeparator;
            sStr += sStr2;
        }
    } else sStr = sStr2;
    return sStr;
}

//****************************************************************************
/*! \brief  String in einem String ersetzen
 *  \param  s           String, in dem ein String ersetzt werden soll
 *  \param  sToReplace  String der ersetzt werden soll
 *  \param  sReplace    String der den String ersetzt
 ****************************************************************************/
void CQXT4StringUtil::replaceStr(string& s, string sToReplace, string sReplace)
{
    int     iPos = 0;
    int     iLen;
    string  sNew;
    
    iLen = (int) sToReplace.length();
    if (iLen > 0) {
        while ((iPos = (int) s.find(sToReplace, iPos)) >= 0) {
            sNew = "";
            if (iPos > 0) {
                sNew += s.substr(0, iPos);
            }
            sNew += sReplace;
            if (iPos < (int) s.length() - iLen) {
                sNew += s.substr(iPos + iLen, s.length() - iPos - iLen);
            }
            s = sNew;
            iPos += (int) sReplace.length();
        }
    }
}

//****************************************************************************
/*! \brief  Filename und Pfad vom UNIX-(POSIX)- ins Macintosh-(HFS)-Format
 *          konvertieren
 *  \param  sFileName   Pfad + Filename
 *  \return Konvertierter Pfad + Filename
 ****************************************************************************/
void CQXT4StringUtil::convertUNIXPathToMacPath(string& sFileName)
{
    bool          bLastCharIsSeparator = false;
    CFStringRef   sRef1 = NULL;
    CFStringRef   sRef2 = NULL;
    CFURLRef      cfurl = NULL;
    unsigned char s[255 + 1];
    
    if (sFileName.length() > 0 && isUNIXPath(sFileName)) {
        bLastCharIsSeparator = sFileName.at(sFileName.length() - 1) == PATH_DELIMITER_UNIX_CHAR;
        replaceChar(sFileName, PATH_DELIMITER_WIN_CHAR, PATH_DELIMITER_UNIX_CHAR);
        sRef1= CFStringCreateWithCString(kCFAllocatorDefault,
                                         sFileName.c_str(),
                                         kCFStringEncodingMacHFS);
        cfurl = CFURLCreateWithFileSystemPath(kCFAllocatorDefault,
                                              sRef1,
                                              kCFURLPOSIXPathStyle,
                                              true);
        if (cfurl != NULL) {
            sRef2 = CFURLCopyFileSystemPath(cfurl, kCFURLHFSPathStyle);
            if (sRef2 != NULL) {
                CFStringGetCString(sRef2, (char*) s, 255, kCFStringEncodingMacHFS);
            }
        }
        if (cfurl != NULL) CFRelease(cfurl);
        if (sRef1 != NULL) CFRelease(sRef1);
        if (sRef2 != NULL) CFRelease(sRef2);
        sFileName = (char*) s;
        if (sFileName.length() > 0 && bLastCharIsSeparator && sFileName.at(sFileName.length() - 1) != PATH_DELIMITER_MAC_CHAR) {
            sFileName += PATH_DELIMITER_MAC_CHAR;
        }
    }
}

//****************************************************************************
/*! \brief  Sprintf for strings
 *  \param  s     String
 *  \param  sFrm  Format string
 *  \param  ...   Variable argument list
 *  \return Error code
 ****************************************************************************/
int CQXT4StringUtil::sprintfString(string& s, string sFrm, ...)
{
    va_list   ArgLst;
    
    va_start(ArgLst, sFrm);
    sprintfStringArgList(s, sFrm, ArgLst);
    va_end(ArgLst);
    return 0;
}

//****************************************************************************
/*! \brief  Sprintf for strings
 *  \param  s     String
 *  \param  sFrm  Format string
 *  \param  ...   Variable argument list
 *  \return Error code
 ****************************************************************************/
int CQXT4StringUtil::sprintfString(string& s, char* sFrm, ...)
{
    va_list   ArgLst;
    
    va_start(ArgLst, sFrm);
    sprintfStringArgList(s, sFrm, ArgLst);
    va_end(ArgLst);
    return 0;
}

//****************************************************************************
/*! \brief  Sprintf for strings
 *  \param  s       String
 *  \param  sFrm    Format string
 *  \param  ArgList Variable argument list
 *  \return Error code
 ****************************************************************************/
int CQXT4StringUtil::sprintfStringArgList(string& s, string sFrm, va_list ArgLst)
{
    return sprintfStringArgList(s, (char*) sFrm.c_str(), ArgLst);
}

//****************************************************************************
/*! \brief  Sprintf for strings
 *  \param  s       String
 *  \param  sFrm    Format string
 *  \param  ArgList Variable argument list
 *  \return Error code
 ****************************************************************************/
int CQXT4StringUtil::sprintfStringArgList(string& s, char* sFrm, va_list ArgLst)
{
    char*     p = NULL;
    int       iLen = 0;
#ifndef WIN32
    char      sTmp[255 + 1];
#endif
    va_list   ArgLstCopy;
    
#ifdef _MSC_VER
#if _MSC_VER < 1800 // Visual Studio 2013: _MSC_VER == 1800
    ArgLstCopy = ArgLst;
#else
    va_copy(ArgLstCopy, ArgLst);
#endif
#else
    va_copy(ArgLstCopy, ArgLst);
#endif
    s = "";
#ifdef WIN32
    iLen = _vscprintf(sFrm, ArgLst);
#else
    iLen = vsnprintf((char*) sTmp, 255, sFrm, ArgLst);
#endif
    if (iLen > 0)
    {
        p = new char[iLen + 1];
        if (p == NULL)
        {
            return -1;
        }
#if _SECURE_ATL
        vsprintf_s(p, iLen + 1, sFrm, ArgLstCopy);
#else
        vsprintf(p, sFrm, ArgLstCopy);
#endif
#ifdef _MSC_VER
#if _MSC_VER >= 1800 // Visual Studio 2013: _MSC_VER == 1800
        va_end(ArgLstCopy);
#endif
#else
        va_end(ArgLstCopy);
#endif
        s = p;
        delete [] p;
    }
    return 0;
}

#ifndef WIN32

#ifdef __MACH__

//****************************************************************************
/*! \brief  Encoding eines Strings wechseln
 *  \param  sToTranscode    String, dessen Encoding gewechselt werden soll
 *  \param  iEncodingFrom   Aktuelles Encoding des Strings
 *  \param  iEncodingFrom   Neues Encoding des Strings
 *  \return Transformierter String
 ****************************************************************************/
string CQXT4StringUtil::transcodeStringEncoding(string sToTranscode,
                                                CFStringEncoding iEncodingFrom,
                                                CFStringEncoding iEncodingTo)
{
    CFStringRef str;
    CFRange     rangeToProcess;
    UInt8*      pLocalBuffer = NULL;
    
    str = CFStringCreateWithBytes(NULL,
                                  (unsigned char*) sToTranscode.c_str(),
                                  sToTranscode.length(),
                                  iEncodingFrom,
                                  false);
    if (str == NULL) {
        return "ERROR_STRING_TRANSCODING";
    }
    sToTranscode = "ERROR_STRING_TRANSCODING";
    rangeToProcess = CFRangeMake(0, CFStringGetLength(str));
    
    // Länge des benötigten Buffers suchen
    CFIndex usedBufferLength = 0;
    CFIndex numChars = CFStringGetBytes(str,
                                        rangeToProcess,
                                        iEncodingTo,
                                        '?',
                                        FALSE,
                                        NULL,
                                        0,
                                        &usedBufferLength);
    
    pLocalBuffer = new UInt8[usedBufferLength + 1];
    if (pLocalBuffer != NULL) {
        numChars = CFStringGetBytes(str,
                                    rangeToProcess,
                                    iEncodingTo,
                                    '?',
                                    FALSE,
                                    pLocalBuffer,
                                    usedBufferLength,
                                    &usedBufferLength);
        sToTranscode = "";
        if (numChars > 0) {
            pLocalBuffer[usedBufferLength] = '\0';
            sToTranscode = (char*) pLocalBuffer;
        }
    }
    if (pLocalBuffer != NULL) delete[] pLocalBuffer;
    CFRelease(str);
    return sToTranscode;
}

//****************************************************************************
/*! \brief  Encoding eines Strings wechseln
 *  \param  sToTranscode    String, dessen Encoding gewechselt werden soll
 *  \param  iEncodingFrom   Aktuelles Encoding des Strings
 *  \param  iEncodingFrom   Neues Encoding des Strings
 *  \return Transformierter String
 ****************************************************************************/
string CQXT4StringUtil::transcodeStringEncodingOLD(string sToTranscode,
                                                   CFStringEncoding iEncodingFrom,
                                                   CFStringEncoding iEncodingTo)
{
    CFStringRef str;
    CFRange     rangeToProcess;
    
    str = CFStringCreateWithBytes(NULL,
                                  (unsigned char*) sToTranscode.c_str(),
                                  sToTranscode.length(),
                                  iEncodingFrom,
                                  false);
    if (str == NULL) {
        return "ERROR_STRING_TRANSCODING";
    }
    sToTranscode = "";
    rangeToProcess = CFRangeMake(0, CFStringGetLength(str));
    while (rangeToProcess.length > 0) {
        UInt8 localBuffer[100 + 1];
        CFIndex usedBufferLength = 0;
        CFIndex numChars = CFStringGetBytes(str,
                                            rangeToProcess,
                                            iEncodingTo,
                                            '?',
                                            FALSE,
                                            (UInt8*) localBuffer,
                                            100,
                                            &usedBufferLength);
        if (numChars == 0) break;   // Failed to convert anything...
        localBuffer[usedBufferLength] = '\0';
        rangeToProcess.location += numChars;
        rangeToProcess.length -= numChars;
        sToTranscode += (char*) localBuffer;
    }
    CFRelease(str);
    return sToTranscode;
}

#ifdef INDESIGN_PROJECT_GT_CS2

//****************************************************************************
/*! \brief  Encoding eines Strings wechseln
 *  \param  sToTranscode    String, dessen Encoding gewechselt werden soll
 *  \param  iEncodingFrom   Aktuelles Encoding des Strings
 *  \param  iEncodingFrom   Neues Encoding des Strings
 *  \return Transformierter String
 ****************************************************************************/
string CQXT4StringUtil::transcodeStringEncoding(WideString sToTranscode,
                                                CFStringEncoding iEncodingFrom,
                                                CFStringEncoding iEncodingTo)
{
    PMString    pmToTranscode(sToTranscode);
    CFStringRef str;
    CFRange     rangeToProcess;
    string      sTranscoded = "";
    UInt8*      pLocalBuffer = NULL;
    
    str = pmToTranscode.CreateCFString();
    if (str == NULL) {
        return "ERROR_STRING_TRANSCODING";
    }
    sTranscoded = "ERROR_STRING_TRANSCODING";
    
    // Länge des benötigten Buffers suchen
    CFIndex usedBufferLength = 0;
    CFIndex numChars = CFStringGetBytes(str,
                                        rangeToProcess,
                                        iEncodingTo,
                                        '?',
                                        FALSE,
                                        NULL,
                                        0,
                                        &usedBufferLength);
    
    pLocalBuffer = new UInt8[usedBufferLength + 1];
    if (pLocalBuffer != NULL) {
        numChars = CFStringGetBytes(str,
                                    rangeToProcess,
                                    iEncodingTo,
                                    '?',
                                    FALSE,
                                    pLocalBuffer,
                                    usedBufferLength,
                                    &usedBufferLength);
        sTranscoded = "";
        if (numChars > 0) {
            pLocalBuffer[usedBufferLength] = '\0';
            sTranscoded = (char*) pLocalBuffer;
        }
    }
    if (pLocalBuffer != NULL) delete[] pLocalBuffer;
    CFRelease(str);
    return sTranscoded;
}

//****************************************************************************
/*! \brief  Encoding eines Strings wechseln
 *  \param  sToTranscode    String, dessen Encoding gewechselt werden soll
 *  \param  iEncodingFrom   Aktuelles Encoding des Strings
 *  \param  iEncodingFrom   Neues Encoding des Strings
 *  \return Transformierter String
 ****************************************************************************/
string CQXT4StringUtil::transcodeStringEncodingOLD(WideString sToTranscode,
                                                   CFStringEncoding iEncodingFrom,
                                                   CFStringEncoding iEncodingTo)
{
    PMString    pmToTranscode(sToTranscode);
    CFStringRef str;
    CFRange     rangeToProcess;
    string      sTranscoded = "";
    
    str = pmToTranscode.CreateCFString();
    if (str == NULL) {
        return "ERROR_STRING_TRANSCODING";
    }
    rangeToProcess = CFRangeMake(0, CFStringGetLength(str));
    while (rangeToProcess.length > 0) {
        UInt8 localBuffer[100 + 1];
        CFIndex usedBufferLength = 0;
        CFIndex numChars = CFStringGetBytes(str,
                                            rangeToProcess,
                                            iEncodingTo,
                                            '?',
                                            FALSE,
                                            (UInt8*) localBuffer,
                                            100,
                                            &usedBufferLength);
        if (numChars == 0) break;   // Failed to convert anything...
        localBuffer[usedBufferLength] = '\0';
        rangeToProcess.location += numChars;
        rangeToProcess.length -= numChars;
        sTranscoded += (char*) localBuffer;
    }
    CFRelease(str);
    return sTranscoded;
}

#endif

#else

//****************************************************************************
/*! \brief  Encoding eines Strings wechseln
 *  \param  sToTranscode    String, dessen Encoding gewechselt werden soll
 *  \param  bWinToMac     Konvertierung von Win zu Mac?
 *  \return Transformierter String
 ****************************************************************************/
string  CQXT4StringUtil::transcodeStringEncoding(string sToTranscode, bool bWinToMac)
{
    int     i;
    int     cMac[] = { 128, 133, 134, 138, 154, 159, 142, 143, 136, 144, 137, 148, 141, 203, 158, 153, 152, 157, 229, 243, 239, 153, 230 };
    int     cWin[] = { 196, 214, 220, 228, 246, 252, 233, 232, 224, 234, 226, 238, 231, 192, 251, 244, 242, 249, 194, 219, 212, 244, 202 };
    
    if (bWinToMac) {
        for (i = 0; i < sizeof(cWin) / sizeof(int); i++) {
            replaceChar(sToTranscode, cWin[i], cMac[i]);
        }
    } else {
        for (i = 0; i < sizeof(cMac) / sizeof(int); i++) {
            replaceChar(sToTranscode, cMac[i], cWin[i]);
        }
    }
    return sToTranscode;
}

#endif

#else

//****************************************************************************
/*! \brief  Encoding eines Strings wechseln
 *  \param  sToTranscode    String, dessen Encoding gewechselt werden soll
 *  \param  iEncodingFrom   Aktuelles Encoding des Strings
 *  \param  iEncodingFrom   Neues Encoding des Strings
 *  \return Transformierter String
 ****************************************************************************/
string  CQXT4StringUtil::transcodeStringEncodingOLD(string sToTranscode,
                                                    int iEncodingFrom,
                                                    int iEncodingTo)
{
    int     iLen = -1;
    char    cToTranscode[(100 + 1) * sizeof(WCHAR)];
    WCHAR   wsToTranscode[(100 + 1) * sizeof(WCHAR)];
    string  s = "";
    
    while (sToTranscode.length() > 0) {
#if _SECURE_ATL
        strcpy_s(cToTranscode, 100 + 1, sToTranscode.substr(0, __min(100, sToTranscode.length())).c_str());
#else
        strcpy(cToTranscode, sToTranscode.substr(0, __min(100, sToTranscode.length())).c_str());
#endif
        MultiByteToWideChar(iEncodingFrom,
                            0,
                            cToTranscode,
                            (int) sToTranscode.length() + 1,
                            wsToTranscode,
                            sizeof(wsToTranscode) / sizeof(WCHAR));
        iLen = WideCharToMultiByte(iEncodingTo,
                                   0,
                                   wsToTranscode,
                                   -1,
                                   cToTranscode,
                                   sizeof(cToTranscode) / sizeof(char),
                                   NULL,
                                   NULL);
        s += cToTranscode;
        sToTranscode = sToTranscode.substr(__min(100, sToTranscode.length()), sToTranscode.length());
    }
    return s;
}

//****************************************************************************
/*! \brief  Encoding eines Strings wechseln
 *  \param  sToTranscode    String, dessen Encoding gewechselt werden soll
 *  \param  iEncodingFrom   Aktuelles Encoding des Strings
 *  \param  iEncodingFrom   Neues Encoding des Strings
 *  \return Transformierter String
 ****************************************************************************/
string  CQXT4StringUtil::transcodeStringEncoding(string sToTranscode,
                                                 int iEncodingFrom,
                                                 int iEncodingTo)
{
    int     iLenWChar = -1;
    int     iLenChar = -1;
    WCHAR*  wsToTranscode = NULL;
    char*   cToTranscode = NULL;
    string  s = "";
    
    if (sToTranscode.length() == 0) return "";
    cToTranscode = new char[sToTranscode.length() + 1];
#if _SECURE_ATL
    strcpy_s(cToTranscode, 100 + 1, sToTranscode.substr(0, __min(100, sToTranscode.length())).c_str());
#else
    strcpy(cToTranscode, sToTranscode.c_str());
#endif
    
    iLenWChar = MultiByteToWideChar(iEncodingFrom,
                                    0,
                                    cToTranscode,
                                    -1,
                                    NULL,
                                    0);
    wsToTranscode = new WCHAR[iLenWChar];
    if (wsToTranscode != NULL) {
        MultiByteToWideChar(iEncodingFrom,
                            0,
                            cToTranscode,
                            -1,
                            wsToTranscode,
                            iLenWChar);
        wsToTranscode[iLenWChar - 1] = '\0';
        
        iLenChar = WideCharToMultiByte(iEncodingTo,
                                       0,
                                       wsToTranscode,
                                       -1,
                                       NULL,
                                       0,
                                       NULL,
                                       NULL);
        cToTranscode = new char[iLenChar]; 
        if (cToTranscode != NULL) {
            WideCharToMultiByte(iEncodingTo,
                                0,
                                wsToTranscode,
                                -1,
                                cToTranscode,
                                iLenChar,
                                NULL,
                                NULL);
            cToTranscode[iLenChar - 1] = '\0';
            s = cToTranscode;
        } else {
            s = "CQXT4StringUtil::transcodeStringEncoding: MEMORY ERROR";
        }
    } else {
        s = "CQXT4StringUtil::transcodeStringEncoding: MEMORY ERROR";
    }
    if (cToTranscode != NULL) delete[] cToTranscode;
    if (wsToTranscode != NULL) delete[] wsToTranscode;
    return s;
}

#endif

