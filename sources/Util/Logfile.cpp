#ifdef INDESIGN_PROJECT
  #include "VCPlugInHeaders.h"
#endif

#ifndef __MWERKS__
  #include  <iostream>
#endif
#include  "logfile.h"
#include  "datetime.h"
#include  "textfile.h"
#include  <stdarg.h>

#ifdef WIN32
  #include  <fcntl.h>
  #include  <time.h>
  #include  <sys/stat.h>
  #include  <shlobj.h>
#else
#ifdef __MWERKS__
  #include  <files.h>
  #include  <Folders.h>
#endif
#ifdef QXP6
  #include  <script.h>
#endif  
#endif
#include  "QXT4StringUtil.h"

//----------------------------------------------------------------------------
// Konstruktor
//----------------------------------------------------------------------------
CLogfile::CLogfile()
{
  m_bError = false;
  m_bInit = false;
  m_sFilename = "";
}

CLogfile::CLogfile(string sFilename, bool bDate)
{
  m_bInit = false;
  Init(sFilename, bDate);
}

//----------------------------------------------------------------------------
// Destruktor
//----------------------------------------------------------------------------
CLogfile::~CLogfile () 
{
}

//----------------------------------------------------------------------------
// Filename setzen und File allenfalls kreieren
//----------------------------------------------------------------------------
void CLogfile::Init(string sFilename, bool bDate)
{
  string      sDate;
  int         pos;
  string      ext;
  CDateTime   oDateTime;
  CTextFile   oTextFile;

  m_bError = false;
  m_sFilename = sFilename;
  if (bDate) {
    sDate = oDateTime.GetString("_yyyymmdd_HHMMSS");
    pos = (int) m_sFilename.find_last_of(".");
    if (pos > 0) {
      ext = m_sFilename.substr(pos, m_sFilename.length() - pos);
      m_sFilename = m_sFilename.substr(0, pos);
      m_sFilename += sDate;
      m_sFilename += ext;
    } else {
      m_sFilename += sDate;
    }
  }
  m_bInit = oTextFile.createDirectory(m_sFilename) == ERR_NO;
}

bool CLogfile::TestOutFormat(bool bError, bool bDate, const char* sFrm, ...)
{
  bool    bOK = true;
  int     iLen = 0;
  char*   p = NULL;
  string  s = "";
#ifndef WIN32
  char    sTmp[255 + 1];
#endif  
  va_list Args;       // Optionale Argumente

  //-------------------------------------------------------------------------
  // Text setzen
  //-------------------------------------------------------------------------
  va_start(Args, sFrm);
#ifdef WIN32
  iLen = _vscprintf(sFrm, Args);
#else
  iLen = vsnprintf((char*) sTmp, 255, sFrm, Args);
#endif
  va_end(Args);
  if (iLen > 0) {
    p = new char[iLen + 1];
    if (p == NULL) {
      return false;
    }
    va_start(Args, sFrm);
#if _SECURE_ATL
    vsprintf_s(p, iLen + 1, sFrm, Args);
#else
    vsprintf(p, sFrm, Args);
#endif
    va_end(Args);
    s = p;
    delete [] p;
    bOK = TestOut(bError, bDate, s);
  }
  return bOK;
}

//----------------------------------------------------------------------------
// Eintrag ins Logfile
//----------------------------------------------------------------------------
bool CLogfile::TestOut(bool bError, bool bDate, string s)
{
  int           iErr = ERR_NO;
  string        sDate;
  CDateTime     oDateTime;

  if (!m_bError && bError) m_bError = true;
  close();
  m_oTextFile.init(m_sFilename);
  iErr = m_oTextFile.openForAppending();
  if (iErr == ERR_NO) {
    if (bDate) {
      sDate = oDateTime.GetString("yyyy-mm-dd HH:MM:SS ");
      m_oTextFile.write((char*) sDate.c_str());
    }
    m_oTextFile.writeLine(s);
    m_oTextFile.close();
  } else {
    return false;
  }
  return true;
}

bool CLogfile::TestOutWithErrClass(int iErrClass, int iErrClassToWrite, bool bError, bool bDate, string s)
{
  bool        bWrite = false;

  bWrite = (iErrClassToWrite & iErrClass) == iErrClass;
  if (bWrite) return TestOut(bError, bDate, s);
  return true;
}

bool CLogfile::TestOutWithErrClassFormat(int ErrClass, int iErrClassToWrite, bool bError, bool bDate, const char* sFrm, ...)
{
  bool    bOK = true;
  int     iLen = 0;
  char*   p = NULL;
#ifndef WIN32
  char    sTmp[255 + 1];
#endif  
  va_list Args;       // Optionale Argumente
  string  s = "";

  //-------------------------------------------------------------------------
  // Text setzen
  //-------------------------------------------------------------------------
  va_start(Args, sFrm);
#ifdef WIN32
  iLen = _vscprintf(sFrm, Args);
#else
  iLen = vsnprintf((char*) sTmp, 255, sFrm, Args);
#endif
  va_end (Args);
  if (iLen > 0) {
    p = new char[iLen + 1];
    if (p == NULL) {
      return false;
    }
    va_start(Args, sFrm);
#if _SECURE_ATL
    vsprintf_s(p, iLen + 1, sFrm, Args);
#else
    vsprintf(p, sFrm, Args);
#endif
    va_end (Args);
    s = p;
    delete [] p;
    bOK = TestOutWithErrClass(ErrClass, iErrClassToWrite, bError, bDate, s);
  }
  return bOK;
}

//----------------------------------------------------------------------------
// File �ffnen
//----------------------------------------------------------------------------
FILE* CLogfile::openForAppending()
{
  int iErr = ERR_NO;

  close();
  m_oTextFile.init(m_sFilename);
  iErr = m_oTextFile.openForAppending();
  if (iErr == ERR_NO) return m_oTextFile.getFile();
  return NULL;
}

//----------------------------------------------------------------------------
// File schliessen
//----------------------------------------------------------------------------
void CLogfile::close()
{
  m_oTextFile.close();
}

#ifndef __MWERKS__

/*****************************************************************
 consoleOut
******************************************************************/
void CLogfile::consoleOutFormat(const char* sFrm, ...)
{
  int       iLen = 0;
  char*     p = NULL;
#ifndef WIN32
  char      sTmp[255 + 1];
#endif  
  string    sOut = "";
  va_list   Args;       // Optionale Argumente
  CDateTime oDateTime;

  //-----------------------------------------------------------------------
  // Text setzen
  //-----------------------------------------------------------------------
  va_start(Args, sFrm);
#ifdef WIN32
  iLen = _vscprintf(sFrm, Args);
#else
  iLen = vsnprintf((char*) sTmp, 255, sFrm, Args);
#endif
  va_end (Args);
  if (iLen > 0) {
    p = new char[iLen + 1];
    if (p == NULL) {
      return;
    }
    va_start(Args, sFrm);
#if _SECURE_ATL
    vsprintf_s(p, iLen + 1, sFrm, Args);
#else
    vsprintf(p, sFrm, Args);
#endif
    va_end (Args);
    sOut = p;
    delete [] p;
    consoleOut(sOut);
  }
}

/*****************************************************************
 consoleOut
******************************************************************/
void CLogfile::consoleOutAppFormat(string sApp, string sVersion, bool bDate, const char* sFrm, ...)
{
  int       iLen = 0;
  char*     p = NULL;
#ifndef WIN32
  char      sTmp[255 + 1];
#endif  
  string    sOut = "";
  va_list   Args;       // Optionale Argumente
  CDateTime oDateTime;

  //-----------------------------------------------------------------------
  // Text setzen
  //-----------------------------------------------------------------------
  va_start(Args, sFrm);
#ifdef WIN32
  iLen = _vscprintf(sFrm, Args);
#else
  iLen = vsnprintf((char*) sTmp, 255, sFrm, Args);
#endif
  va_end (Args);
  if (iLen > 0) {
    p = new char[iLen + 1];
    if (p == NULL) {
      return;
    }
    va_start(Args, sFrm);
#if _SECURE_ATL
    vsprintf_s(p, iLen + 1, sFrm, Args);
#else
    vsprintf(p, sFrm, Args);
#endif
    va_end (Args);
    sOut = p;
    delete [] p;
    consoleOutApp(sApp, sVersion, bDate, sOut);
  }
}

/*****************************************************************
 consoleOut
******************************************************************/
void CLogfile::consoleOut(string sOut)
{
  CDateTime oDateTime;

#ifdef DEBUG
  cout << "* (DEBUG) " << sOut << " *" << endl;
#else
  cout << "* " << sOut << " *" << endl;
#endif
}

/*****************************************************************
 consoleOut
******************************************************************/
void CLogfile::consoleOutApp(string sApp, string sVersion, bool bDate, string sOut)
{
  string          sApplication = "";
  CQXT4StringUtil oStrUtil;
  CDateTime       oDateTime;

  sApplication = oStrUtil.compoundString(sApplication, sApp, " ");
  sApplication = oStrUtil.compoundString(sApplication, sVersion, " ");
#ifdef DEBUG
  sApplication = oStrUtil.compoundString(sApplication, "(DEBUG)", " ");
#endif
  sApplication = oStrUtil.compoundString(sApplication, "-", " ");
  if (bDate) {
    sApplication = oStrUtil.compoundString(sApplication, oDateTime.GetString("dd.mm.yyyy HH:MM:SS"), " ");
  }
  if (sApplication.length() > 0) sApplication += ": ";
  if (sApplication.length() > 0) {
    cout << "* " << sApplication << sOut << " *" << endl;
  } else {
    cout << "* " << sOut << " *" << endl;
  }
}

#endif

#if defined WIN32 && defined INDESIGN_PROJECT

/*****************************************************************
 Ausgabe auf Konsolenfenster; Das Fenster wird ge�ffnet, falls noch
 keines offen ist
******************************************************************/
void CLogfile::writeToPMMConsole(const char* sFrm, ...)
{
  int       iLen = 0;
  char*     p = NULL;
  string    sOut = "";
  va_list   Args;       // Optionale Argumente
  HANDLE    __hStdOut = NULL;
	DWORD     cCharsWritten;

  //-----------------------------------------------------------------------
  // Text setzen
  //-----------------------------------------------------------------------
  va_start(Args, sFrm);
  iLen = _vscprintf(sFrm, Args);
  va_end (Args);
  if (iLen > 0) {
    p = new char[iLen + 1];
    if (p == NULL) {
      return;
    }
    va_start(Args, sFrm);
#if _SECURE_ATL
    vsprintf_s(p, iLen + 1, sFrm, Args);
#else
    vsprintf(p, sFrm, Args);
#endif
    va_end (Args);
    sOut = p;
    delete [] p;
    // Konsole
    if (GetConsoleWindow() == NULL) {
      AllocConsole();
	    SetConsoleTitle(L"PMM Debug Window");
	    __hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
      if (__hStdOut != NULL) {
        COORD co = { 200, 100 };
        SetConsoleScreenBufferSize(__hStdOut, co);
      }
    } else {
	    __hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
    }
    if (__hStdOut != NULL) {
      CQXT4StringUtil oStrUtil;
      SQLWCHAR*       pConsoleMsg = nil;
      oStrUtil.convertToSQLWChar(sOut, &pConsoleMsg);
      if (pConsoleMsg != nil) {
#ifdef _WIN64
		    WriteConsole(__hStdOut, pConsoleMsg, (DWORD) sOut.length(), &cCharsWritten, NULL);
#else
		    WriteConsole(__hStdOut, pConsoleMsg, sOut.length(), &cCharsWritten, NULL);
#endif
        delete pConsoleMsg;
      }
    }
  }
}

/*****************************************************************
 Konsolenfenster schliessen
******************************************************************/
void CLogfile::closePMMConsole()
{
  if (GetConsoleWindow() != NULL) {
    FreeConsole();
  }
}

#endif
