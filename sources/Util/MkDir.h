//****************************************************************************
/*! \file
 *  \brief    Headerdatei f�r die Funktion mkdir (aus stat.mac.c)
 *  \author   Hans St�ssel
 *  \date     2003
 *  \version  1.0.0
 *  \bug      
 *  \warning  
 *  \todo     
 ****************************************************************************/
#ifndef MKDIR_H
#define MKDIR_H

#include <string>
using namespace std;

int mkdir(const string sPath);

#endif