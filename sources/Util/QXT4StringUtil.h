//****************************************************************************
/*! \file
 *  \brief    Headerdatei f�r Stringfunktionen
 *  \author   Hans St�ssel
 *  \date     2012
 *  \version  1.0.0 *  \bug      
 *  \warning  
 *  \todo     
 ****************************************************************************/
#if !defined __CQXT4STRINGUTIL
#define __CQXT4STRINGUTIL

#ifdef INDESIGN_PROJECT
  #include "atypes.h"
#ifndef WIN32
  #include "iodbcunix.h"
#endif
  #include "sqltypes.h"
#ifdef INDESIGN_ICU_SUPPORT
  #include "DecImFmt.h"
#endif  
  #include "StringUtils.h"
#endif

#ifdef  __MWERKS__
  typedef unsigned long CFStringEncoding;
#endif

#ifdef WIN32
  #include <wtypes.h>
#endif

#include <string>
#include <vector>
using namespace std;

#if defined XERCES_2_1 || defined XERCES_2_7 || defined XERCES_2_8 || defined XERCES_3_1_2
  #include <xercesc/util/XMLString.hpp>
  #include <xercesc/framework/XMLFormatter.hpp>
  #include <xercesc/util/Base64.hpp>

#if defined XERCES_2_7 || defined XERCES_2_8 || defined XERCES_3_1_2
  XERCES_CPP_NAMESPACE_USE
#endif
#endif

#if !defined INDESIGN_PROJECT && !defined EXCLUDE_QXP
  #include "QXTVersion.h"
#endif

#include <algorithm>
#ifndef PATH_DELIMITER_WIN
  #define PATH_DELIMITER_WIN        "\\"  /*!< \brief Trennzeichen f�r einen Dateipfad (Windows) */
#endif
#ifndef PATH_DELIMITER_WIN_CHAR
  #define PATH_DELIMITER_WIN_CHAR   PATH_DELIMITER_WIN[0]
#endif
#ifndef DRIVE_DELIMITER
  #define DRIVE_DELIMITER           ":"   /*!< \brief Trennzeichen f�r den Laufwerksbuchstaben */
#endif
#ifndef DRIVE_DELIMITER_CHAR
  #define DRIVE_DELIMITER_CHAR      DRIVE_DELIMITER[0]
#endif
#ifndef PATH_DELIMITER_MAC
  #define PATH_DELIMITER_MAC        ":"   /*!< \brief Trennzeichen f�r einen Dateipfad (Macintosh) */
#endif
#ifndef PATH_DELIMITER_MAC_CHAR
  #define PATH_DELIMITER_MAC_CHAR   PATH_DELIMITER_MAC[0]
#endif
#ifndef PATH_DELIMITER_UNIX
  #define PATH_DELIMITER_UNIX       "/"   /*!< \brief Trennzeichen f�r einen Dateipfad (UNIX) */
#endif
#ifndef PATH_DELIMITER_UNIX_CHAR
  #define PATH_DELIMITER_UNIX_CHAR  PATH_DELIMITER_UNIX[0]
#endif

#ifndef PATH_DELIMITER
  #ifdef WIN32
	  #define PATH_DELIMITER          PATH_DELIMITER_WIN  /*!< \brief Trennzeichen f�r einen Dateipfad (Windows) */
  #else
	  #define PATH_DELIMITER          PATH_DELIMITER_MAC  /*!< \brief Trennzeichen f�r einen Dateipfad (Macintosh) */
  #endif
#endif
#ifndef PATH_DELIMITER_CHAR
  #define PATH_DELIMITER_CHAR       PATH_DELIMITER[0]
#endif

#ifndef ERR_NO
  #define ERR_NO  0
#endif

//****************************************************************************
/*! \brief    4D-Konstanten f�r SQL
 ****************************************************************************/
#define FOURD_SQL_QUOTE       "<pmm22>"
#define FOURD_SQL_UNDERLINE   "<pmm5F>"
#define FOURD_SQL_BACKQUOTE   "<pmm60>"
#define FOURD_SQL_APOSTROPHE  "<pmm27>"

//****************************************************************************
/*! \brief    Klasse f�r Stringfunktionen
 ****************************************************************************/
class CQXT4StringUtil {

public:
  CQXT4StringUtil();
  ~CQXT4StringUtil();
    void    replaceStr(char* s, int iLen, string sToReplace, string sReplace);
    void    replaceStr(string& s, string sToReplace, string sReplace);
    string  convertPlatformToUTF8(string sStr);
    void    convertNetworkPathToHDPath(string sFileName, unsigned char*  sHDPath, int iHDPathLen, bool bConvertToPString);\
    void    convertNetworkPathToHDPath(string& sFilename);
    void    convertMacPathToUNIXPath(string& sFileName);
    void    convertWinPathToMacPath(string sFileName, unsigned char*  sMacFileName, int iMacFilenameLen, bool bConvertToPString);
    int     firstIndexOfIgnoreCase(string s, string sSub, int iStartPos = 0);
    int     split(string s, string sDelimiter, vector<string>& vSplit);
    string  trimLeftIgnoreCase(string s, char cToTrim);
    string  trimLeftIgnoreCase(string s, string sToTrim);
    string  trimRightIgnoreCase(string s, char cToTrim);
    string  trimRightIgnoreCase(string s, string sToTrim);
    string  makeLower(string s);
    bool    equalsIgnoreCase(string s1, string s2);
    
    void    convertCToP(string sCString, unsigned char* sPString, int iPStringLen);
    bool    isUNIXPath(string sFilename);
    
    void convertWinPathToMacPath(string& sFileName);
    
    void    replaceChar(string& s, char cToReplace, char cReplace);
     void   convertUNIXPathToMacPath(string& sFileName);
    int     lastIndexOfIgnoreCase(string s, string sSub);
    string  trimRight(string s, string sToTrim);
    string  trimRight(string s, char cToTrim);
    void appendPartToPath(string& sPath, string sPart, string sDelimiter = "");
    
    string  trimLeft(string s, string sToTrim);
    string  trimLeft(string s, char cToTrim);
    
    void appendDelimiterToPath(string& sPath, string sDelimiter = "");
    int     getInt(string s);
    double  getDouble(string s);
    string  compoundString(string sStr1, string sStr2, string sSeparator);
    
    
    int     sprintfString(string& s, char* sFrm, ...);
    int     sprintfStringArgList(string& s, char* sFrm, va_list ArgLst);
    int     sprintfString(string& s, string sFrm, ...);
    int     sprintfStringArgList(string& s, string sFrm, va_list ArgLst);
    
    
#ifdef WIN32
    string  transcodeStringEncoding(string sToTranscode,
                                    int iEncodingFrom,
                                    int iEncodingTo);
    string  transcodeStringEncodingOLD(string sToTranscode,
                                       int iEncodingFrom,
                                       int iEncodingTo);
#else
#ifdef __MACH__
    string  transcodeStringEncoding(string sToTranscode,
                                    CFStringEncoding iEncodingFrom,
                                    CFStringEncoding iEncodingTo);
    string  transcodeStringEncodingOLD(string sToTranscode,
                                       CFStringEncoding iEncodingFrom,
                                       CFStringEncoding iEncodingTo);
#ifdef INDESIGN_PROJECT_GT_CS2
    string  transcodeStringEncoding(WideString sToTranscode,
                                    CFStringEncoding iEncodingFrom,
                                    CFStringEncoding iEncodingTo);
    string  transcodeStringEncodingOLD(WideString sToTranscode,
                                       CFStringEncoding iEncodingFrom,
                                       CFStringEncoding iEncodingTo);
#endif
#else
    string  transcodeStringEncoding(string sToTranscode,
                                    bool bWinToMac);
#endif
#endif
    
};

#endif
