//****************************************************************************
/*! \file
 *  \brief    Header file: Text file reading and writing
 *  \author   Hans St�ssel
 *  \date     2007
 *  \version  1.0.0
 *  \bug      
 *  \warning  
 *  \todo     
 ****************************************************************************/
#ifndef __TEXTFILE_H__
#define __TEXTFILE_H__
#include <string>
#include <vector>
using namespace std;

#ifdef INDESIGN_PROJECT
  #include "PMTypes.h"
class IDFile;
  class PMString;

  struct structInddFileFilter
  {
    SysOSType     iSysOSType;       /*!< \brief File type, e.g. 'TEXT'  */
    PMString      sFileExt;         /*!< \brief File extension */
    PMString      sFilterName;      /*!< \brief Name of the filter */

    structInddFileFilter()
    {
      reset();
    }

    void reset()
    {
      iSysOSType = kAnyCreator;
      sFileExt = PMString("");
      sFilterName = PMString("");
    }
  };

  typedef structInddFileFilter  stInddFileFilter;
#endif

#ifdef WIN32
  #include <vector>
#endif

#ifndef ERR_NO
  #define	ERR_NO                            0       /*!< No error */
#endif
#ifndef ERR_FILE_OPENFORWRITING
  #define	ERR_FILE_OPENFORWRITING           -2000   /*!< Error open file for writing */
#endif
#ifndef ERR_FILE_OPENFORREADING
  #define	ERR_FILE_OPENFORREADING           -2001   /*!< Error open file for reading */
#endif
#ifndef ERR_FILE_NOTOPEN
  #define	ERR_FILE_NOTOPEN                  -2002   /*!< File not open */
#endif
#ifndef ERR_FILE_EOF
  #define	ERR_FILE_EOF                      -2003   /*!< Reached end of file */
#endif
#ifndef ERR_FILE_MKDIR
  #define	ERR_FILE_MKDIR                    -2004   /*!< Error creating directory */
#endif
#ifndef ERR_FILE_NOTEXIST
  #define	ERR_FILE_NOTEXIST                 -2005   /*!< File doesn't exist */
#endif
#ifndef ERR_FILE_WRITING
  #define	ERR_FILE_WRITING                  -2006   /*!< Error writing */
#endif
#ifndef ERR_FILE_READING
  #define	ERR_FILE_READING                  -2007   /*!< Error reading */
#endif
#ifndef ERR_FILE_COPY
  #define	ERR_FILE_COPY                     -2008   /*!< Error copy a file */
#endif
#ifndef ERR_FILE_DELETE
  #define	ERR_FILE_DELETE                   -2009   /*!< Error delete a file */
#endif

#ifndef __ODFRC__

//****************************************************************************
/*! \brief Struktur zum Auslesen eines Konfiurationsfiles
 ****************************************************************************/
struct structCfgFileParameter
{
  bool        bSet;
	std::string sParam;
	bool        bValue;
	int         iValue;
	double      dValue;
	std::string sValue;

  structCfgFileParameter()
  {
    reset(false);
  }

  void reset(bool bResetParam)
  {
    bSet = false;
    if (bResetParam) sParam = "";
	  iValue = 0;
	  dValue = 0.0;
	  sValue = "";
	  bValue = false;
  }
};
typedef struct structCfgFileParameter stCfgFileParameter;

struct structSAndRText
{
  int iSearchText;
  int iPosStart;
  int iPosEnd;

  structSAndRText()
  {
    reset();
  }

  void reset()
  {
    iSearchText = -1;
    iPosStart = -1;
    iPosEnd = -1;
  }
};
typedef struct structSAndRText stSAndRText;

bool sortTextSAndR(stSAndRText oTextSAndR1, stSAndRText oTextSAndR2);

class CTextFile
{
  public:
    CTextFile(std::string sFilename = "", int iWriteLogFile = 0);
    ~CTextFile();

    void close();
    int  createDirectory(std::string sFile);

    bool existFile(std::string sFile = "");
    bool deleteFile();
    bool renameFile(std::string sNewFilename, bool bDeleteExistingFile = true);
    bool copyFile(std::string sNewFilename, bool bDeleteExistingFile = true);

    std::string getFilePath() { return m_sFilename; }

    FILE* getFile() { return m_pFile; }

    int openForWriting();
    int openForReading();
    int openForAppending();
    int write(char* sFrm, ...);
    int writeLine(char* sFrm, ...);
    int write(std::string s);
    int writeLine(std::string s);
    int writeBinary(unsigned char* pBuffer, unsigned int iLenBuffer);
    int readBinary(unsigned char* pBuffer, unsigned int iLenBuffer);
    int readLine(std::string* s);

    bool hasExtension(std::string sFilename, std::string sExtension);

    void init(std::string sFilename, int iWriteLogFile = 0);

    int binarySearchAndReplace(std::string sFilename, std::vector<std::string> vTextSearch, std::vector<std::string> vTextReplace);

    static std::string getSystemFolder(int iFolderType);
    static std::string getPreferenceFolder(bool bAllUser, std::string sApplication = "");
    static std::string getDesktopFolder();
    static std::string getPMMLogFolder();
    static std::string getTempFolder();
    static std::string getCurrentUserFolder();
#if defined INDESIGN_PROJECT && !defined INDESIGN_SERVERVERSION
    bool  dialogOpenFile(IDFile& oFile, PMString sTitle);
    bool  dialogOpenFile(IDFile& oFile, PMString sTitle, std::vector<stInddFileFilter> vFilter);
    bool  dialogSaveFile(IDFile& oFile, PMString sTitle);
    bool  dialogSaveFile(IDFile& oFile, PMString sTitle, std::vector<stInddFileFilter> vFilter);
#endif

    int   readConfigFile(std::string sCfgFilename, std::vector<stCfgFileParameter>& vValue);

    void  writeLogFile(const char* sFrm, ...);
  private:
    FILE* fileOpen(std::string sFilename, const char* sMode);
  private:
    FILE*   m_pFile;
    std::string  m_sFilename;
    fpos_t  m_iCurPos;
    int     m_iWriteLogFile;
};

#endif

#endif
