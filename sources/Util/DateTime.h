//****************************************************************************
/*! \file
 *  \brief    Headerdatei f�r die Datumsklasse
 *  \author   Hans St�ssel
 *  \date     2007
 *  \version  1.0.0
 *  \bug      
 *  \warning  
 *  \todo     
 ****************************************************************************/
#ifndef DATETIME_H
#define DATETIME_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef WIN32
  #define _MSL_USING_MW_C_HEADERS 1
#endif  

#include  <time.h>

#include <string>
using namespace std;

//****************************************************************************
/*! \brief    Datum/Zeit-Klasse
 ****************************************************************************/
class CDateTime
{
  //-------------------------------------------------------------------------
  // Interne Daten
  //-------------------------------------------------------------------------
  protected:
	  bool  m_bLeapYear;  /*!< Flag: Schaltjahr? */
    int   m_Day;        /*!< Tag im Monat (1..31) */
    int   m_Month;      /*!< Monat im Jahr (1..12) */
    int   m_Year;       /*!< Jahr (zB.: 1997) */
    int   m_Sec;        /*!< Anzahl Sekunden (0..59) */
    int   m_Min;        /*!< Anzahl Minuten	 (0..59) */
    int   m_Hour;       /*!< Anzahl Stunden	 (0..23) */
	  int		m_DayOfWeek;  /*!< Wochentag (SO=0, MO=1, DI=2...SA=6) */
	  int	  m_DayOfYear;  /*!< Tag im Jahr (1..365, 366) */
  //-------------------------------------------------------------------------
  // �ffentliche Methoden
  //-------------------------------------------------------------------------
  public:
    CDateTime();
	  CDateTime(const CDateTime& Time);
	  CDateTime(const int Year, const int Month, const int Day, const int Hour, const int Min, const int Sec);

    ~CDateTime ();

    CDateTime& operator = (const CDateTime& Time);
#if defined TIMESTAMP_STRUCT
    CDateTime& operator = (const TIMESTAMP_STRUCT Time);
#endif
    bool  operator > (const CDateTime& Time);
    bool  operator < (const CDateTime& Time);
    bool  operator == (const CDateTime& Time);
    bool  operator >= (const CDateTime& Time);
    bool  operator <= (const CDateTime& Time);
    bool  operator != (const CDateTime& Time);

	  CDateTime   GetAktTime();
    int         GetYear() { return m_Year; }            /*!< Gibt die Anzahl Jahre zur�ck */
	  int         GetMonth() { return m_Month; }          /*!< Gibt die Anzahl Monate zur�ck */
	  int         GetDay()  { return m_Day; }             /*!< Gibt die Anzahl Tage zur�ck */
	  int         GetHour() { return m_Hour; }            /*!< Gibt die Anzahl Stunden zur�ck */  
	  int         GetMin() { return m_Min; }              /*!< Gibt die Anzahl Minuten zur�ck */
	  int         GetSec() { return m_Sec; }              /*!< Gibt die Anzahl Sekunden zur�ck */
	  string      GetString (const string Format = "dd.mm.yyyy HH:MM:SS");
	  int         GetDayOfWeek()  { return m_DayOfWeek; } /*!< Gibt den Tag der Woche zur�ck */
	  int         GetDayMonth();
    time_t      GetMSTime();

	  bool        GetLocalTime(time_t time, struct tm* timeInfo);

    time_t      MakeTime(struct tm* tm);
    string      Prepare(const string s, const string Format);

    bool  IsValid();
    bool  IsNull();

    bool  SetFromString(const string TimeStr, const string Format = "dd.mm.yyyy HH:MM:SS");
    bool  SetYear(int iValue, bool bValidate) { m_Year = iValue; return bValidate ? Validate() : true; }    /*!< Setzt die Anzahl Jahre und validiert die Werte \return Werte korrekt validiert? \retval true OK \retval false Fehler beim Validieren */
	  bool  SetMonth(int iValue, bool bValidate) { m_Month = iValue; return bValidate ? Validate() : true; }  /*!< Setzt die Anzahl Monate und validiert die Werte \return Werte korrekt validiert? \retval true OK \retval false Fehler beim Validieren */
	  bool  SetDay(int iValue, bool bValidate)  { m_Day = iValue; return bValidate ? Validate() : true; }     /*!< Setzt die Anzahl Tage und validiert die Werte \return Werte korrekt validiert? \retval true OK \retval false Fehler beim Validieren */
	  bool  SetHour(int iValue, bool bValidate) { m_Hour = iValue; return bValidate ? Validate() : true; }    /*!< Setzt die Anzahl Stunden und validiert die Werte \return Werte korrekt validiert? \retval true OK \retval false Fehler beim Validieren */
	  bool  SetMin(int iValue, bool bValidate) { m_Min = iValue; return bValidate ? Validate() : true; }      /*!< Setzt die Anzahl Minuten und validiert die Werte \return Werte korrekt validiert? \retval true OK \retval false Fehler beim Validieren */
	  bool  SetSec(int iValue, bool bValidate) { m_Sec = iValue; return bValidate ? Validate() : true; }      /*!< Setzt die Anzahl Sekunden und validiert die Werte \return Werte korrekt validiert? \retval true OK \retval false Fehler beim Validieren */
	  bool  Set(const int Year, const int Month, const int Day, const int Hour, const int Min, const int Sec);
	  bool  Set(const time_t iTime);
  //-------------------------------------------------------------------------
  // Interne Methoden
  //-------------------------------------------------------------------------
  protected:
    bool  Validate ();
};

//---------------------------------------------------------------------------
#endif