#ifndef LOGFILE_H
#define LOGFILE_H                 

#ifndef WIN32
  #define _MSL_USING_MW_C_HEADERS 1
#endif  

#ifndef ERR_NO
  #define ERR_NO  0
#endif

#ifndef LOGFILE_DEFAULT_NAME
  #define LOGFILE_DEFAULT_NAME            "Project.log"
#endif

#define LOGFILE_DEFAULT_ADDDATE           false

#define LOGFILE_CLASS_1   0x01
#define LOGFILE_CLASS_2   0x02
#define LOGFILE_CLASS_3   0x04
#define LOGFILE_CLASS_4   0x08
#define LOGFILE_CLASS_5   0x10
#define LOGFILE_CLASS_6   0x20

#include "TextFile.h"

#include <string>
using namespace std;

class CLogfile
{
  protected:
    string    m_sFilename;
    bool      m_bError;
    bool      m_bInit;
    CTextFile m_oTextFile;
  public:
    CLogfile();
    CLogfile(string sFilename, bool bDate = true);
    ~CLogfile();

    string getPreferenceFolder(bool bAllUser, string sApplication = "") { return CTextFile::getPreferenceFolder(bAllUser, sApplication); }
    string getDesktopFolder() { return CTextFile::getDesktopFolder(); }
    string getPMMLogFolder() { return CTextFile::getPMMLogFolder(); }
    string getTempFolder() { return CTextFile::getTempFolder(); }
    string getCurrentUserFolder() { return CTextFile::getCurrentUserFolder(); }

    FILE* openForAppending();
    void  close();
    FILE* getFile() { return m_oTextFile.getFile(); }

    string GetFilename() { return m_sFilename; }
    bool IsError() { return m_bError; }
    void Init(string Filename, bool bDate);

#ifndef __MWERKS__
    // Diese Ausgabe setzt ein bereits ge�ffnetes Konsolenfenster voraus
    // Ausgabe mit cout. Funktioniert z.B. im Indesign Server mit ge�ffnetem Konsolenfenster
    static void consoleOutFormat(const char* sFrm, ...);
    static void consoleOutAppFormat(string sApp, string sVersion, bool bDate, const char* sFrm, ...);
    static void consoleOut(string s);
    static void consoleOutApp(string sApp, string sVersion, bool bDate, string sOut);
    //
#endif

    bool TestOut(bool bError, bool bDate, string s);
    bool TestOutFormat(bool bError, bool bDate, const char* sFrm, ...);
    bool TestOutWithErrClass(int iErrClass, int iErrClassToWrite, bool bError, bool bDate, string s);
    bool TestOutWithErrClassFormat(int iErrClass, int iErrClassToWrite, bool bError, bool bDate, const char* sFrm, ...);

#if defined WIN32 && defined INDESIGN_PROJECT
    // Hier wird ein eigenes Konsolenfenster ge�ffnet und in dieses Fenster ausgegeben
    // L�uft nur unter Windows in einem Indesign-Projekt, bzw. wurde noch nicht
    // f�r andere Umgebungen implementiert.
    void writeToPMMConsole(const char* sFrm, ...);
    void closePMMConsole();
    //
#endif
  protected:
};

#endif